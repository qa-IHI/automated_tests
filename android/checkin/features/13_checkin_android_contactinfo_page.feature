# encoding: UTF-8

@checkin_feature_contact_info_page @qa_ready @13
Feature: Contact Info Page

Background:
  Given I am logged out on the app

@13.1
Scenario: 13.1 Android back button from the Contact info panel
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  And the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks the Android Back button
  Then the Scan and Check-In page should open "eyes"


@13.2
Scenario: 13.2 Enlarge contact business card image
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "wardhaugh" "eyes"
  When the user clicks the X icon
  Then the contact page should contain a business card image for "wardhaugh" "eyes"

# Seems like zooming into an image is broken in Appium with Android.
@wip @13.3
Scenario: 13.3 View a business card in full screen and zoom in
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "wardhaugh" "eyes"
  When the user zooms in on the business card
  Then the business card should zoomed in for "wardhaugh" "eyes"

@13.4
Scenario: 13.4 Click edit on the contact info page
	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"