# encoding: UTF-8

@checkin_feature_contacts @qa_ready @8
Feature: Contacts

Background:
  Given I am logged out on the app

@8.1
Scenario: 8.1 Choose a contact on the Contact List
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the contact "Ronaldo"
   Then the Contact info panel should appear
   And the contact information for the contact should match "Ronaldo" "eyes"
