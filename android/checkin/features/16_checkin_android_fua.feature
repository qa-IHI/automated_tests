# encoding: UTF-8

@checkin_feature_fua @qa_ready @16 @coretests
Feature: Follow Up Actions

Background:
  Given I am logged out on the app

@16.1
Scenario: 16.1 Add Top Gear contacts
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user deletes existing contacts for "Mobile Auto Long Topics, Mobile Automation Event 1, Mobile Automation Event 2"
  Then the Contacts list should be empty "eyes"
  When the user adds the "Top Gear" contacts
  Then the Contacts list should include "Top Gear" contacts "noeyes"

@16.2
Scenario: 16.2 Add NFL contacts
  Given the user is on the Login screen of the Checkin app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "NFL" contacts 
  Then the Contacts list should include "NFL" contacts "eyes"

@16.3
Scenario: 16.3 Press the X button on the Follow Up Actions page
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  And the user searches for the contact with "Last Name" "May"
  Then the contact with "Last Name" "May" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "May" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks the X icon on the Follow-Up Actions page
  Then the Edit Contact page should open for "May" "noeyes"
  	
# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @16.4
Scenario: 16.4 Select and de-select a follow-up action that has no sub follow-up actions
	Given the user is on the Login screen of the Checkin app "eyes"
	And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
	And the user searches for the contact with "Last Name" "Hammond"
	Then the contact with "Last Name" "Hammond" should be returned by the search
	When the user clicks the first contact
	Then the Contact info page should appear
	And the contact information for the contact should match "Hammond" "eyes"
	When the user scrolls to the "Web" field
	And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
	When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user selects the "Jupiter" follow-up actions
  Then the "Jupiter" follow-up actions should be "selected"
  When the user deselects the "Jupiter" follow-up actions
  Then the "Jupiter" follow-up actions should be "deselected"

@16.5
Scenario: 16.5 Select a follow-up action with sub follow-up actions
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Clarkson"
  Then the contact with "Last Name" "Clarkson" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Clarkson" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"


# Currently cannot be automated. Appium cannot tell if a sub follow-up action is selected or deselected.
@wip @16.6
Scenario: 16.6 Select and de-select a sub follow-up action
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  Then the contact with "Last Name" "May" should be returned by the search
  When the user clicks the first contact
  Then the Contact info page should appear
  And the contact information for the contact should match "May" "eyes"
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  When the user selects the "English, Spanish" sub follow-up actions
  Then the "English, Spanish" sub follow-up actions should be "selected"
  When the user clicks "Done"
  Then the user should be on the Topics page
  And the "Languages" topics should be "selected"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  And the "English, Spanish" sub follow-up actions should be "selected"
  When the user clears all sub follow-up actions
  Then all sub follow-up actions should be "deselected"
  When the user clicks "Done"
  Then the user should be on the Topics page
  And the "Languages" topics should be "deselected"

@16.7
Scenario: 16.7 Select a Note follow-up action
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	And the text should match "This is a test note."


# Currently cannot be automated. Appium cannot tell if a sub follow-up action is selected or deselected.
@wip @16.8
Scenario: 16.8 Edit a Note's default text and press done
  Given the user is on the Login screen of the Checkin app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "May"
  When the user scrolls to the "City" field
 	#When the user clicks "Edit Follow-Up Actions"
	And the user clicks "Edit Topics"
	Then the user should be on the Topics page
	When the user clicks "Next"
	Then the user should be on the Edit Follow-Up Actions page
	When the user clicks "A test note"
 	And changes the text from "This is a test note." to "A reasonably priced car"
	Then the user should be on the Edit Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"
	When the user clicks "Done"
	And the user clicks the Home button
	Then the Scan a Card page should open
	When the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
 # Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  Then the "Follow-Up Action(s)" field should contain "A test note"
  When the user clicks "Edit"
  Then the user should be on the Edit Contact page for "May"
  When the user scrolls to the "City" field
 	#When the user clicks "Edit Follow-Up Actions"
	And the user clicks "Edit Topics"
	Then the user should be on the Topics page
	When the user clicks "Next"
	Then the user should be on the Edit Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"


@16.9
Scenario: 16.9 Press the X icon for a note
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Manning"
  Then the contact with "Last Name" "Manning" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Manning" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Manning" "noeyes"
 	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	When the user clicks the X icon in a note
	Then the user should be on the Edit Follow-Up Actions page "eyes"


# Expecting this to fail because we can't tell if a topic/fua is selected/deselected right now. Need Dev assist.
@wip @16.10
Scenario: 16.10 Edit follow-up action Note and use special characters
	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info page should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user scrolls to the "Edit Topics" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "A test note"

	And changes the text from "This is a test note." to "!@#$%&+?/_,.()-:;*="
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	And the "A test note" follow-up actions should be "selected"

	When the user clicks "Done"
	And the user clicks the Home button
	Then the user is on the landing page "noeyes"

	When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"

  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  #Then the contact with "Last Name" "Clarkson" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "Clarkson"
  When the user scrolls to the "City" field
 	#When the user clicks "Edit Follow-Up Actions"
	And the user clicks "Edit Topics"
	Then the user should be on the Topics page "eyes"
	When the user clicks "Next"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	And the text should match "!@#$%&+?/_,.()-:;*="

# Currently cannot tell when a FUA is selected or not.
@wip @16.11
Scenario: 16.11 Add a follow-up action Note while editing a contact
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"

  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Manning"
  Then the contact with "Last Name" "Manning" should be returned by the search
  When the user clicks the first contact
  Then the Contact info page should appear 
  And the contact information for the contact should match "Manning" "eyes"
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Manning" "noeyes"
  When the user changes the "Mobile" from "12345678901" to "9999999999"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
  And changes the text from "Some random comments." to "Charing Cross"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Note" follow-up actions should be "selected"
  When the user clicks "Done"
  Then the Contact page should appear "noeyes"
  #Then the user will go back to the Contact info page
  When the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  #Then the contact with "Last Name" "Hammond" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Comments" field
  Then the "Comments" field should contain "Charing Cross"

@16.12
Scenario: 16.12 Enter more than 500 characters for a follow-up action Note
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"

 	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
 	And changes the text from "Some random comments." to more than 500 characters
  Then the note will only limit the entry to "500" characters
  When the user clicks "Done"
  Then the user should be on the Edit Follow-Up Actions page "eyes"

# Currently cannot tell when a FUA is selected or not.
@wip @16.13
Scenario: 16.13 De-select a follow-up action Note
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
  And changes the text from "Some random comments." to "Formula E Racing"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Note" follow-up actions should be "selected"
  When the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Comments" field
  Then the "Comments" field should contain "Formula E Racing"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Clear"
  Then the "Note" follow-up actions should be "deselected"    
  When the user clicks "Done"
  Then the Contact page should appear "noeyes"
  And the "Comments" field should be empty

# Currently cannot tell when a FUA is selected or not.
@wip @16.14
Scenario: 16.14 Select and edit a Free Text follow-up action
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is logged into the app and the tutorial is displayed "noeyes"
  When the user clears the tutorial overlay
  Then the user is on the landing page "eyes"
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  Then the Free Text pop-up should appear with title "Free Text"
  And the text should match "default free text"
  When the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "Lets drive around Monaco!"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"


# Currently cannot be automated. Appium cannot tell if a sub follow-up action is selected or deselected.
@wip @16.15
Scenario: 16.15 De-select a follow-up action Free Text
  Given the user is on the Login screen of the Checkin app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  #Then the contact with "Last Name" "May" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "May"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "May"
  And the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  When the user clicks "Edit Topics"
  Then the user should be on the Topics page
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Clear"
  Then the "Free Text" follow-up actions should be "deselected"

@16.16
Scenario: 16.16 Press the X icon for a Free Text follow-up action
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Manning"
  Then the contact with "Last Name" "Manning" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Manning" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Manning" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  Then the Free Text pop-up should appear with title "Free Text"
  When the user clicks the X icon in a note
  Then the user should be on the Edit Follow-Up Actions page "eyes"

# Currently cannot be automated. Appium cannot tell if a sub follow-up action is selected or deselected.
@wip @16.17
Scenario: 16.17 Edit follow-up action Free Text and use special characters
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Manning"
  Then the contact with "Last Name" "Manning" should be returned by the search
  When the user clicks the first contact
  Then the Contact info page should appear
  And the contact information for the contact should match "Manning" "eyes"
 
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Manning" "noeyes"
  
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"

  When the user clicks "Free Text"
  And changes the text from "default free text" to "!@#$%&+?/_\"\",.()-:;*="
  And the user clicks "Done"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"

  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Manning"
  #Then the contact with "Last Name" "Manning" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "Manning"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Manning" "noeyes"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the Note pop-up should appear with title "Free Text"
  And the text should match "!@#$%&+?/_\"\",.()-:;*="

@16.18
Scenario: 16.18 Add Top Gear contacts
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user deletes existing contacts for "Mobile Auto Long Topics, Mobile Automation Event 1, Mobile Automation Event 2"
  Then the Contacts list should be empty "eyes"
  When the user adds the "Top Gear" contacts
  Then the Contacts list should include "Top Gear" contacts "noeyes"

@16.19
Scenario: 16.19 Add NFL contacts
  Given the user is on the Login screen of the Checkin app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "NFL" contacts 
  Then the Contacts list should include "NFL" contacts "eyes"

# Currently cannot be automated. Appium cannot tell if a sub follow-up action is selected or deselected.
@wip @16.20
Scenario: 16.20 Add a follow-up action Free Text while editing a contact
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is logged into the app and the tutorial is displayed "noeyes"
  When the user clears the tutorial overlay
  Then the user is on the landing page "eyes"
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  #Then the contact with "Last Name" "Hammond" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Phone" from "01234567890" to "7777777777"
  And the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "Waterloo Station"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  #Then the contact with "Last Name" "Hammond" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Comments" field
  Then the "Follow-Up Action(s)" field should contain "Free Text"

@16.21
Scenario: 16.21 Enter more than 500 characters for a follow-up action Free Text
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to more than 500 characters
  Then the note will only limit the entry to "500" characters
  When the user clicks "Done"
  Then the user should be on the Edit Follow-Up Actions page "eyes"

@16.22
Scenario: 16.22 Add Starfleet contacts
  Given the user is on the Login screen of the Checkin app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "Starfleet" contacts 
  Then the Contacts list should include "Starfleet" contacts "eyes"

@16.23
Scenario: 16.23 Long list of follow-up actions and sub follow-up actions
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Kirk"
  Then the contact with "Last Name" "Kirk" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Kirk" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Kirk" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  And the FUAs page should display "Catalunya, Circuit of the Americas, Daytona, Hungaroring, Laguna Seca, Melbourne, Monaco, Mont Tremblant, Nurburgring, Silverstone, Spa, Suzuka, Texas Motor Speedway, Top Gear Track, Zandvoort" "FUAs" in alpha order
  When the user clicks the X icon on the Follow-Up Actions page
  And the user clicks "Edit Follow-Up Actions"
  And the user clicks "Monaco"
  Then the sub follow-up actions pop up should appear
  And the sub follow-up actions page should display "A, B, Z, F, D, G, V, t, S, m, o, n, q" "sub-FUAs" in alpha order


# Currently cannot be automated. Appium cannot tell if a sub follow-up action/follow-up action is selected or deselected.
@wip @16.24
Scenario: 16.24 Follow-up actions clear button
  Given the user is on the Login screen of the Checkin app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 2" on the Scan a Card page
  And the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  #Then the contact with "Last Name" "Hammond" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "Hammond"
  And the user scrolls to the "City" field
  When the user clicks "Edit Topics"
  Then the user should be on the Topics page
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page
  When the user selects the "Jupiter, Mars, Pluto" follow-up actions
  And the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  When the user selects the "English, Spanish" sub follow-up actions
  And the user clicks "Done"
  Then the "Jupiter, Mars, Pluto, Languages" follow-up actions should be "selected"
  When the user clicks "Clear"
  Then all follow-up actions should be "deselected"


# Currently cannot be automated. Appium cannot tell if a sub follow-up action/follow-up action is selected or deselected.
@wip @16.25
Scenario: 16.25 Verify follow-up actions for a new contact
  Given the user is on the Login screen of the Checkin app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 2" on the Scan a Card page
  And the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  #Then the contact with "Last Name" "Clarkson" should be returned by the search
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "Clarkson"
  When the user scrolls to the "City" field
  #When the user clicks "Edit Follow-Up Actions"
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page  
  And the "Mars, Pluto" follow-up actions should be "selected"


# Currently cannot be automated. Appium cannot tell if a sub follow-up action/follow-up action is selected or deselected.
@wip @16.26
Scenario: 16.26 Add new contact with follow-up actions but no topics
  Given the user is on the Login screen of the Checkin app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user navigates to Contacts
  And the user deletes existing contacts
  Then the Contacts list should be empty
  When the user clicks the Home button
  Then the Scan a Card page should open
  When the user chooses event "Mobile Automation Event 2" on the Scan a Card page
  And the user adds the "F1" contacts
  When the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Rosberg"
  And the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "Rosberg"
  When the user scrolls to the "Comments" field
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "Rosberg"
  When the user scrolls to the "City" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page  
  And the "Mars, Jupiter" follow-up actions should be "selected"
  