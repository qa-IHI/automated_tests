# encoding: UTF-8

require 'appium_lib'
require 'rubygems'
require 'cucumber/ast'
require 'yaml'
require 'sauce_whisk'
require 'json'
require 'selenium-webdriver'
require 'eyes_selenium'


# This Ruby script reads in test parameters from the env.cfg file.
CONFIG_FILE = Dir.pwd + '/features/support/env.cfg'
DEFAULT_BACKGROUND_TIME = 7

if File.file?(CONFIG_FILE) == false
	puts "ERROR: The config file #{CONFIG_FILE} does not exist. Exiting."
	exit 1
else
	config = begin
  		YAML.load(File.open(CONFIG_FILE))
	rescue ArgumentError => e
  		puts "Could not parse YAML: #{e.message}"
	end

	$caps = config["caps"]
	$test = config["test"]
end

# Determine if we're running against a Sauce Labs Appium server or not.
# Option 1: Run a Sauce Labs servers.
if ENV['SERVER'] == "sauce"
	# Get the filename of the zipped app package from the Jenkins environment.
	if ENV['APPFILE'] == nil
		puts "ERROR: The zipped app package is undefined. Exiting."
		exit 1
	else
		$app_file = ENV['APPFILE']
		puts "app_file = #{$app_file}"

		# Extract the app version from the filename.
		$app_version = $app_file.match(/^AtEvent-(.*).zip$/).captures[0]
		puts "app_version = #{$app_version}"
	end

	# Get the Sauce user and Sauce key from the Jenkins environment.
	if ENV['SAUCE_USERNAME'] == nil
		puts "ERROR: The Sauce user is undefined. Exiting."
		exit 1
	else
		$sauce_user = ENV['SAUCE_USERNAME']
		puts "sauce_user = #{$sauce_user}"
	end

	if ENV['SAUCE_ACCESS_KEY'] == nil
		puts "ERROR: The Sauce key is undefined. Exiting."
		exit 1
	else
		$sauce_key = ENV['SAUCE_ACCESS_KEY']
		puts "sauce_key = #{$sauce_key}"
	end

	if ENV['SAUCEJSONURL'] == nil
		puts "ERROR: The Sauce Labs JSON endpoint is undefined. Exiting."
		exit 1
	else
		$sauce_json_url = ENV['SAUCEJSONURL']
		puts "sauce_json_url = #{$sauce_json_url}"
	end

	if ENV['SAUCEMINTIME'] == nil
		puts "ERROR: The Sauce Labs minimum available time is undefined. Exiting."
		exit 1
	else
		$sauce_min_time = ENV['SAUCEMINTIME']
		puts "sauce_min_time = #{$sauce_min_time}"
	end

	if ENV['ANDROIDVERSION'] == nil
		puts "ERROR: The Android Version is undefined. Exiting."
		exit 1
	else
		$android_version = ENV['ANDROIDVERSION'].to_s
		puts "android_version = #{$android_version}"
	end

	if ENV['ANDROIDDEVICE'] == nil
		puts "ERROR: The Android device is undefined. Exiting."
		exit 1
	else
		$android_device = ENV['ANDROIDDEVICE']
		puts "android_device = #{$android_device}"
	end

	if ENV['APPIUMVERSION'] == nil
		puts "ERROR: The Appium version is undefined. Exiting."
		exit 1
	else
		$appium_version = ENV['APPIUMVERSION'].to_s
		puts "appium_version = #{$appium_version}"
	end

	# Continue only if we have at least 180 mins of test time remaining on Sauce Labs.
	#json = `curl -s https://saucelabs.com/rest/v1/users/shawnakumar -u shawnakumar:b90ca1c2-7ca1-4159-8b43-f9a8aac71ed9`
	json = `curl -s #{$sauce_json_url}/#{$sauce_user} -u #{$sauce_user}:#{$sauce_key}`
	result = JSON.parse(json)
	
	if result["minutes"].to_i <= $sauce_min_time.to_i
		puts "ERROR: There are only #{result["minutes"]} minutes left on Sauce Labs. Automated tests will not start."
		exit 1
	else
		puts "Excellent!! There are #{result["minutes"]} minutes left of testing time on Sauce Labs. Proceeding..."
	end

	$appium_url = "http://#{$sauce_user}:#{$sauce_key}@ondemand.saucelabs.com:80/wd/hub"
	$app_location = "sauce-storage:#{$app_file}"
	$unicodeKeyboard = $caps["unicodeKeyboard"]

# Option 2
# Run on a non-Sauce server.
else
	# local server address
	$appium_url = "http://127.0.0.1:4723/wd/hub"

	# remote server address
	#$appium_url = "http://192.168.23.17:4723/wd/hub"

	$appium_version = $caps["appiumVersion"].to_s
	$android_device = $caps["deviceName"]
	$android_version = $caps["platformVersion"].to_s
	$avd = $caps["avd"].to_s
	$unicodeKeyboard = $caps["unicodeKeyboard"]

	$app_location = $test["app_path"]
	$app_version = $test["app_version"].to_s
	$eyes_enabled = $test["eyes_enabled"].to_s
	$eyes_key = $test["eyes_api_key"]
	$qa_reset_username = $test["qa_reset_username"]
end
