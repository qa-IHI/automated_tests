# encoding: UTF-8

@checkin_feature_filter_contacts @qa_ready @11
Feature: Filter Contacts

Background:
  Given I am logged out on the app

@11.1
Scenario: 11.1 Use the Filter button on the Contacts page
  Given the user is on the Login screen of the Checkin app "eyes"
	And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
	When the user clicks the Filter button
	Then the Filter Contacts page should appear "eyes"


@11.2
Scenario: 11.2 Choose a contact after filtering contacts
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Company" named "FC Barcelona"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the contact list will contain "1" contacts
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Messi" "eyes"

@11.3
Scenario: 11.3 Filter contacts based on Company
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Company" named "FC Barcelona"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"
  When the user clicks the Filter button
  And the user filters by the "Company" named "¿¡ÀÈÌÒÙÇÃÑÕ"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the contact list will contain "2" contacts
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ"

@11.4
Scenario: 11.4 Filter contacts based on Job Title
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Job Title" named "Football Player"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"
  When the user clicks the Filter button
  And the user filters by the "Job Title" named "Alien"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the contact list will contain "2" contacts
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ"

# Currently cannot filter on event in the Android app.
@11.5 @wip
Scenario: 11.5 Filter contacts based on Event
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Event" named "Mobile Automation Event 2"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the "ALL" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Ronaldo, )(*&^%$$##@!><?:{"
  And the user clicks the Filter button
  And the user filters by the "Event" named "Mobile Automation Event 1"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the "ALL" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, Ronaldo, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{"

@11.6
Scenario: 11.6 Filter contacts based on Topics
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Topics" named "Android"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"
  When the user clicks the Filter button
  And the user filters by the "Topics" named "Checkin"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"

@11.7
Scenario: 11.7 Filter contacts based on Follow Up Actions
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Follow Up Actions" named "Han Solo"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"
  When the user clicks the Filter button
  And the user filters by the "Follow Up Actions" named "Darth Vader"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the contact list will contain "1" contacts
  And the Contacts list should contain contact(s) "Messi"

# Currently does not work bc we cannot tell if a Topic/FUA is selected or not. Need dev assistance.
@11.8 @wip
Scenario: 11.8 Selected filters should be highlighted green and Filter button will indicate number of filters applied
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Company" named "FC Barcelona"
  #And the user filters by the "Event" named "Mobile Automation Event 2"
  And the user filters by the "Follow Up Actions" named "Jupiter"
  Then "Company, Event, Follow Up Actions" should be highlighted "eyes"
  When the user applies the filter
  Then the Filter button will show "3" filter applied

# Currently does not work bc we cannot tell if a Topic/FUA is selected or not. Need dev assistance.
@11.9 @wip
Scenario: 11.9 Clear filters
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user clicks the Filter button
  And the user filters by the "Job Title" named "Football Player"
  And the user filters by the "Topics" named "Python"
  Then "Job Title, Topics" should be highlighted "eyes"
  When the user applies the filter
  Then the Filter button will show "2" filter applied
  When the user clicks the Filter button
  And the user clears all filters
  Then no filters should be highlighted "eyes"
  When the user applies the filter
  Then the Filter button will show no filters applied
