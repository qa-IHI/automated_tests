# encoding: UTF-8

@checkin_feature_edit_contact_page @qa_ready @14 @coretests
Feature: Edit Contact Page

Background:
  Given I am logged out on the app

@14.1
Scenario: 14.1 Add Top Gear contacts
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user deletes existing contacts for "Mobile Auto Long Topics, Mobile Automation Event 1, Mobile Automation Event 2"
  Then the Contacts list should be empty "eyes"
  When the user adds the "Top Gear" contacts
  Then the Contacts list should include "Top Gear" contacts "noeyes"

@14.2
 Scenario: 14.2 Back button from Edit Contact page
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks "Edit"
  Then the Edit Contact page should open for "random" "noeyes"
  When the user clicks the Android Back button
  Then the Contact info panel should appear


# @14.3
# Scenario: 14.3 View a business card in full screen from the Edit Contact page
# 	Given the user is on the Login screen of the Checkin app "eyes"
#   And the user successfully logs in as "Uzair"
#   Then the user is on the Select an Event page "noeyes"
#   When the user chooses event "Card Scanner Demo" on the Select an Event page
#   Then the Scan and Check-In page should open "noeyes"
#   When the user searches for the contact with "Last Name" "wardhaugh"
#   And the user clicks the first contact
#   Then the contact page should contain a business card image for "wardhaugh" "eyes"
#   When the user clicks "Edit"
#   Then the Edit Contact page should open for "wardhaugh" "eyes"
#   When the user clicks the magnify button
#   Then the enlarged business card should appear for "wardhaugh" "eyes"


# @14.4
# Scenario: 14.4 From the Edit Contact page, view a business card in full screen and zoom in
#   Given the user is on the Login screen of the Checkin app "eyes"
#   And the user successfully logs in as "Uzair"
#   Then the user is on the Select an Event page "noeyes"
#   When the user chooses event "Card Scanner Demo" on the Select an Event page
#   Then the Scan and Check-In page should open "noeyes"
#   When the user navigates to Contacts "eyes"
#   Then the user should be on the Contacts page "eyes"
#   When the user selects the "ALL" tab on the Contacts page "eyes"
#   And the user selects All Events
#   And the user searches for the contact with "Last Name" "wardhaugh"
#   And the user clicks the first contact
#   Then the contact page should contain a business card image for "wardhaugh" "eyes"
#   When the user scrolls to the "Web" field
#   And the user clicks "Edit"
#   Then the Edit Contact page should open for "wardhaugh" "eyes"
#   When the user clicks the magnify button on the Edit Contact page
#   Then the enlarged business card should appear for "wardhaugh" "eyes"
#   When the user zooms in on the business card
#   Then the business card should zoomed in for "wardhaugh" "eyes"

@14.5
Scenario: 14.5 Edit Topics from Edit Contact page
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"
  When the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"

@14.6
Scenario: 14.6 Edit Follow-Up Actions from Edit Contact page
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"

@14.7
Scenario: 14.7 Press Done on the Edit Contact page
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"
  When the user clicks "Done"
  Then the Contact info panel should appear

@14.8
Scenario: 14.8 Press Cancel on the Edit Contact page
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"
  When the user clicks "Cancel"
  Then the Contact info panel should appear

# Currently fails bc there is no pop up warning. Instead we get a grey message box that fades.
@wip @14.9
Scenario: 14.9 Edit Contact without last name
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks "Edit"
  Then the Edit Contact page should open for "clarkson" "eyes"
  When the user changes the "Last Name*" from "Clarkson" to ""
	When the user clicks "Done"
	Then the user will receive a "Please enter last name!" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "clarkson" "eyes"

# Currently fails bc there is no pop up warning. Instead we get a grey message box that fades.
@wip @14.10
Scenario: 14.10 Edit Contact without company
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks "Edit"
  Then the Edit Contact page should open for "clarkson" "eyes"
  When the user changes the "Company*" from "Top Gear" to ""
  And the user clicks "Done"
  Then the user will receive a "Please enter company name!" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "clarkson" "eyes"

# Currently fails bc there is no pop up warning. Instead we get a grey message box that fades.
@wip @14.11
Scenario: 14.11 Edit Contact without email
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Email*" from "hammond@cars.com" to ""
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Please enter contact's email address!" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "Hammond" "noeyes"

# Currently fails bc there is no pop up warning. Instead we get a grey message box that fades.
@wip @14.12
Scenario: 14.12 Edit Contact with invalid email
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event pagei
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Email*" from "hammond@cars.com" to "?????????!!!"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Invalid email address" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "Hammond" "noeyes"

@14.13
Scenario: 14.13 Edit contact and press done
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Company*" from "Top Gear" to "Amazon.com"
	And the user clicks "Done"
  Then the Contact info panel should appear
  And the company should be "Amazon.com"
	
@14.14
Scenario: 14.14 Edit contact and press cancel
 	Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info panel should appear
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Company*" from "Amazon.com" to "Netflix"
	And the user clicks "Cancel"
  Then the Contact info panel should appear
  And the company should be "Amazon.com"

# Currently fails bc there is no pop up warning. Instead we get a grey message box that fades.
@wip @14.15
Scenario: 14.15 Edit contact field with more than 200 characters
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "First Name" from "Jeremy" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "First Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Last Name*" from "Clarkson" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Last Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Company*" from "Amazon.com" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Company Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Job Title" from "Presenter" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Job Title Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Web" from "http://www.website.com" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Web address has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  #When the user scrolls the "Street" field to the top of the window
  When the user changes the "Street" from "12345 Main St." to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Address has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  #When the user scrolls the "City" field to the top of the window
  When the user changes the "City" from "AnyCity" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "City Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  #When the user scrolls the "State" field to the top of the window
  When the user changes the "State" from "Any State" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "State Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  #When the user scrolls to the bottom of the Edit Contact page
  When the user changes the "Zip" from "123 456" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Zip Code has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear
  When the user clicks the Home button
  And the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  #When the user scrolls to the bottom of the Edit Contact page
  When the user changes the "Country" from "United Kingdom" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Edit Topics" field
  And the user clicks "Done"
  Then the user will receive a "Country Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  Then the Contact info page should appear


# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @14.16
Scenario: 14.16 Edit Topics page clear button
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact info page should appear
  And the contact information for the contact should match "Clarkson"
  And the user clicks "Edit"
  Then the user should be on the Edit Contact page for "Clarkson"
  When the user scrolls to the "City" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page
  And the topics for "Clarkson" should be "selected"
  When the user clicks "Clear"
  Then all topics should be deselected

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @14.17
Scenario: 14.17 Edit Topics

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @14.18
Scenario: 14.18 Edit Follow-up Actions clear button

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @14.19
Scenario: 14.19 Edit Follow-up Actions

@14.20
Scenario: 14.20 Add Top Gear contacts
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user deletes existing contacts for "Mobile Auto Long Topics, Mobile Automation Event 1, Mobile Automation Event 2"
  Then the Contacts list should be empty "eyes"
  When the user adds the "Top Gear" contacts
  Then the Contacts list should include "Top Gear" contacts "noeyes"

@14.21
Scenario: 14.21 Add NFL contacts
  Given the user is on the Login screen of the Checkin app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "NFL" contacts 
  Then the Contacts list should include "NFL" contacts "eyes"
   
@14.22
Scenario: 14.22 Edit Note/Free Text
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  And the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Done"
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "I edited this free text"
  And the user clicks "Done"
  And the user clicks "Note"
  And changes the text from "Some random comments." to "I edited this note"
  And the user clicks "Done"
  And the user clicks "Done"
  And the user clicks "Done"
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user closes the contact information panel
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  And the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the text should match "I edited this free text"
  When the user clicks "Note"
  Then the text should match "I edited this note"

@14.23
Scenario: 14.23 Edit existing Note/Free Text
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  And changes the text from "I edited this free text" to "And on that bombshell"
  And the user clicks "Done"
  And the user clicks "Note"
  And changes the text from "I edited this note" to "Some say..."
  And the user clicks "Done"
  And the user clicks "Done"
  And the user clicks "Done"
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user closes the contact information panel
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the text should match "And on that bombshell"
  When the user clicks "Note"
  Then the text should match "Some say..."

@14.24
Scenario: 14.24 Clear existing note
  Given the user is on the Login screen of the Checkin app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user clicks "Edit"
  Then the Edit Contact page should open for "Brady" "noeyes"
  When the user scrolls to the "State" field
  Then the "Comments" field should contain "Some say..."
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
  And the user clears the note "Some say..."
  And the user clicks "Done"
  And the user clicks "Done"
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user closes the contact information panel
  Then the Scan and Check-In page should open "noeyes"
  And the user searches for the contact with "Last Name" "Brady"
  Then the contact with "Last Name" "Brady" should be returned by the search
  When the user clicks the first contact
  Then the Contact info panel should appear
  And the contact information for the contact should match "Brady" "eyes"
  When the user scrolls to the "Web" field
  Then the "Comments" field should be empty
