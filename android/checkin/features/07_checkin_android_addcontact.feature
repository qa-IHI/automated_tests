# encoding: UTF-8

@checkin_feature_adding_contacts @qa_ready @7 @coretests
Feature: Adding Contacts

Background:
  Given I am logged out on the app

@7.1
Scenario: 7.1 Home button from Add Contact page
   Given the user is on the Login screen of the Checkin app "eyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user clicks X to close the Add Contact panel
   Then the Scan and Check-In page should open "eyes"

@7.2
Scenario: 7.2 Back button from Add Contact
   Given the user is on the Login screen of the Checkin app "eyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user clicks the Android Back button
   Then the Scan and Check-In page should open "eyes"

# Currently does not work because message disappears afer a few seconds.
@wip @7.3
Scenario: 7.3 User tries to add contact with no last name
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the company name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Done"
   Then the user will receive a "Please enter last name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.4
Scenario: 7.4 User tries to add contact with no company
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Done"
   Then the user will receive a "Please enter company name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.5
Scenario: 7.5 User tries to add contact with no email
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Zip" field
   And the user clicks "Done"
   Then the user will receive a "Please enter contact's email address" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.6
Scenario: 7.6 User tries to add contact with incorrectly formatted email
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user enters an incorrectly formatted email address
   And the user scrolls to the "Zip" field
   And the user clicks "Done"
   Then the user will receive a "Invalid email address" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.7
Scenario: 7.7 User has not entered last name and clicks Add Topics
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the company name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Add Topics"
   Then the user will receive a "Please enter last name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.8
Scenario: 7.8 User has not entered company and clicks Add Topics
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Add Topics"
   Then the user will receive a "Please enter company name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.9
Scenario: 7.9 User has not entered email and clicks Add Topics
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Zip" field
   And the user clicks "Add Topics"
   Then the user will receive a "Please enter contact's email address" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.10
Scenario: 7.10 User enters incorrectly formatted email and clicks Add Topics
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user enters an incorrectly formatted email address
   And the user scrolls to the "Zip" field
   And the user clicks "Add Topics"
   Then the user will receive a "Invalid email address" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.11
Scenario: 7.11 User has not entered last name and clicks Add Follow-Up Actions
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the company name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Add Follow-Up Actions"
   Then the user will receive a "Please enter last name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.12
Scenario: 7.12 User has not entered company and clicks Add Follow-Up Actions
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Zip" field
   And the user clicks "Add Follow-Up Actions"
   Then the user will receive a "Please enter company name!" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.13
Scenario: 7.13 User has not entered email and clicks Add Follow-Up Actions
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Zip" field
   And the user clicks "Add Follow-Up Actions"
   Then the user will receive a "Please enter contact's email address" message

# Currently does not work because message disappears afer a few seconds.
@wip @7.14
Scenario: 7.14 User enters incorrectly formatted email and clicks Add Follow-Up Actions
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user enters an incorrectly formatted email address
   And the user scrolls to the "Zip" field
   And the user clicks "Add Follow-Up Actions"
   Then the user will receive a "Invalid email address" message

@7.15
Scenario: 7.15 User has entered all required fields and clicks Add Topics
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Add Topics"
   Then the user should be on the Topics page "eyes"

@7.16
Scenario: 7.16 User has entered all required fields and clicks Add Follow-Up Actions
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Add Follow-Up Actions"
   Then the user should be on the Add Follow Up Actions page "eyes"

@7.17
Scenario: 7.17 User has entered all required fields and clicks Done
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the Scan and Check-In page should open "eyes"

@7.18
Scenario: 7.18 User has entered all required fields and clicks Cancel
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Cancel"
   Then the Scan and Check-In page should open "eyes"

@7.19
Scenario: 7.19 User correctly enters all required fields for new contacts and clicks Done
   Given the user is on the Login screen of the Checkin app "eyes"   
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user deletes existing contacts for "Mobile Auto Long Topics, Mobile Automation Event 1, Mobile Automation Event 2"
   Then the Contacts list should be empty "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user manually enters new contact ")(*&^%$$##@!><?:{"
   And the user clicks "Done"
   Then the Scan and Check-In page should open "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the new contact ")(*&^%$$##@!><?:{" should be in the Contacts list "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user manually enters new contact "¿¡ÀÈÌÒÙÇÃÑÕ"
   And the user clicks "Done"
   Then the Scan and Check-In page should open "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the new contact "¿¡ÀÈÌÒÙÇÃÑÕ" should be in the Contacts list "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user manually enters new contact "Ronaldo"
   And the user clicks "Done"
   Then the Scan and Check-In page should open "eyes"
   When the user opens the Select an Event dropdown
   And the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the new contact "Ronaldo" should be in the Contacts list "eyes"

@7.20
Scenario: 7.20 Manually added new contact should be at the top and have a "New" badge until clicked
   Given the user is on the Login screen of the Checkin app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "eyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "eyes"
   When the user clicks the '+' button
   Then the Add Contact panel should open "eyes"
   When the user manually enters new contact "Messi"
   And the user adds a Topic and Follow-up Action for "Messi"
   And the user clicks "Done"
   Then the Scan and Check-In page should open "eyes"
   And the new contact "Messi" should be at the top of the Contact list with a New badge "eyes"
   When the user clicks the contact "Messi"
   Then the Contact info panel should appear
   And the contact information for the contact should match "Messi" "eyes"
   When the user closes the contact information panel
   Then the Scan and Check-In page should open "noeyes"
