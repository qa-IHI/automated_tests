# encoding: UTF-8

@checkin_feature_about_tab @qa_ready @5
Feature: About Tab

Background:
  Given I am logged out on the app

@5.1
Scenario: 5.1 Home button from About
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to the About page
  And the user clicks the Home button
  Then the Scan and Check-In page should open "eyes"

@5.2
Scenario: 5.2 Menu icon from About
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to the About page
  And the user clicks the Menu Icon
  Then the app menu should open and "About" should be highlighted "eyes"

@5.3
Scenario: 5.3 The About tab displays the app version
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to the About page
  Then the correct app version is displayed "eyes"

# Currently doesn't work bc Android requires answering a EULA to use the web browser for the first time.
@wip @5.4
Scenario: 5.4 Click Terms and Conditions link
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to the About page
  And the user clicks the Terms and Conditions link
  And the user chooses the Chrome browser
  Then default web browser should open and display the Terms and Conditions page "eyes"
