# encoding: UTF-8

Given(/^I am logged out on the app$/) do
  begin
    @driver.find_element(:id, "com.ihi.ateventfm:id/loginButton").displayed?
  rescue
    logout
  end
end


# Check if we are on the Login screen
Given(/^the user is on the Login screen of the Checkin app "(.*?)"$/) do |visual_validate|
  	all_elements_visible?(["com.ihi.ateventfm:id/logoText", "com.ihi.ateventfm:id/forgotPasswordView", "com.ihi.ateventfm:id/keepMeLoggedInLabel", "com.ihi.ateventfm:id/loginButton"])

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Initial Login Screen')
  	end
end


# Enter a valid email address.
When(/^the user enters a valid email$/) do
 	enter_valid_email($test["qa_username"])
end


# Log into the app with a valid username/password
When(/^the user enters valid email and password$/) do
  	enter_valid_email($test["qa_username"])
  	enter_valid_password($test["qa_password"])
end


# Click the Log In button
When(/^the user clicks Log In$/) do
  	click_login
end


# Click the Forgot Password? button
When(/^the user clicks Forgot Password$/) do
  	click_forgot_password
end


# Click the Cancel button
When(/^the user clicks Cancel$/) do
  	click_cancel
end


# We should see the elements of the landing screen
Then(/^the user is on the Select an Event page "(.*?)"$/) do |visual_validate|
  	selectanevent_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Select an Event Screen')
  	end
end


Then(/^the Password Reset window should appear "(.*?)"$/) do |visual_validate|
	password_reset_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Password Reset Screen')
  	end
end


# Make sure Keep Me Logged in is enabled by default
When(/^Keep Me Logged in is enabled by default$/) do
  	keep_me_logged_in_checked?
end


# Background the Checkin app for a time and then return to the Checkin
When(/^the user backgrounds the Checkin app and returns$/) do
	#@driver.background_app(DEFAULT_BACKGROUND_TIME)

	click_android_home_button
	sleep 10
	#click_start_atevent
	click_appswitch_button
	select_atevent_process
end

# Kill the Checkin process on the mobile device and then relaunch the app.
When(/^the user kills and relaunches the app$/) do
	click_appswitch_button
	kill_cs_process
	click_android_home_button
	sleep 5
	click_apps_icon
	launch_cs_app
end


# Uncheck the Keep Me Logged in checkbox
When(/^the user unchecks Keep Me Logged in$/) do
 	set_keep_me_logged_in(false)
end


# Wait for the error message
Then(/^the user will receive an* "([^"]*)" message$/) do |message|
 	wait_for_error(message)
end


When(/^the user enters an invalid email format$/) do
 	enter_invalid_email_format
end


# Don't enter a password
When(/^does not enter a password$/) do
 	enter_empty_password
end


# Enter an invalid email address format
When(/^the user enters an invalid email$/) do
 	enter_invalid_email
end


When(/^the user enters an invalid password$/) do
  	enter_invalid_password
end


# Click a button
When(/^the user clicks "([^"]*)"$/) do |name|
 	click_button(name)
end


When(/^the user enters an invalid password reset email$/) do
 	enter_invalid_reset_email
end


When(/^enters a valid email account for reset$/) do
 	enter_valid_reset_email($test["qa_reset_username"])
end


Given(/^the user successfully logs in as "(.*?)"$/) do |user|
 	case user
 	when "Uzair"
    	username = $test["uzair_username"]
    	password = $test["uzair_password"]
  	when "QA"
    	username = $test["qa_username"]
    	password = $test["qa_password"]
  	end

  	enter_valid_email(username)
  	enter_valid_password(password)
  	click_login
end


When(/^the user chooses event "(.*?)" on the Select an Event page$/) do |event|
	$fields_hash["saved_event"] = event
	choose_event_on_selectanevent(event)
end


When(/^the user chooses event "(.*?)" on the Contacts page$/) do |event|
	$fields_hash["saved_event"] = event
	choose_event_on_contacts(event)
end


When(/^the user navigates to other pages from Scan and Check-In$/) do
  	#navigate_to_contacts
  	
  navigate_to_about
  navigate_to_test_connectivity
  navigate_to_reportaproblem
  navigate_to_contactus
end


Then(/^the chosen Event should remain the same$/) do
	saved_event = $fields_hash["saved_event"]
 	displayed_event = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/attendeesFragmenteventName") }
 	raise "Error: The displayed event doesn't match what we chose earlier." unless displayed_event.attribute("text") == saved_event
end


When(/^the user navigates back to the Scan and Check-In page$/) do
  	navigate_to_scanandcheckin
  	#eyes_verify_screen(@app_name, 'Landing Screen')
end


When(/^the user clicks the Menu Icon$/) do
  	click_menu_button
end


Then(/^the app menu should open with "(.*?)" "(.*?)"$/) do |option, visual_validate|
  	#verify_app_menu_highlight(option)

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "App Menu with #{option} Screen")
  	end
end


Then(/^the app menu should open and "([^"]*)" should be highlighted "([^"]*)"$/) do |option, visual_validate|
    verify_app_menu_highlight(option)

    if visual_validate == "eyes"
      eyes_verify_screen(@app_name, "App Menu with #{option} highlighted.")
    end
end


When(/^the user clicks the '\+' button$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").click }
end


When(/^the user clicks the Android Back button$/) do
  	click_back_button
end


Then(/^the Add Contact panel should open "(.*?)"$/) do |visual_validate|
 	add_contact_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Manual Add Contact Screen')
  	end
end


When(/^the user goes to the Contact Us page "(.*?)"$/) do |visual_validate|
 	navigate_to_contactus

 	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Contact Us Screen')
  	end
end


When(/^the user goes to Report a Problem "(.*?)"$/) do |visual_validate|
  	navigate_to_reportaproblem

  	if visual_validate == "eyes"
  		eyes_verify_screen(@app_name, 'Report a Problem Screen')
  	end
end


When(/^the user navigates to Contacts "(.*?)"$/) do |visual_validate|
 	navigate_to_contacts

 	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Contacts Screen')
  	end
end

When(/^the user navigates to Select an Event "(.*?)"$/) do |visual_validate|
	navigate_to_selectanevent

 	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Select an Event Screen')
  	end
end


When(/^the user clicks the Home button$/) do
  	click_cs_home_button
end


When(/^the user clicks the Phone button$/) do
  	click_phone_button
end


Then(/^the phone client should open with phone number "(.*?)"$/) do |number|
	phone_client_page_check(number)
end


When(/^the user goes to the About page$/) do
  	navigate_to_about
  	eyes_verify_screen(@app_name, 'About Screen')
end


Then(/^the Scan and Check-In page should open "([^"]*)"$/) do |visual_validate|
  	scanandcheckin_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Scan and Check-In Screen')
  	end
end


Then(/^the correct app version is displayed "(.*?)"$/) do |visual_validate|
  	verify_app_version

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'About Screen')
  	end
end


When(/^the user clicks the Terms and Conditions link$/) do
  	click_terms_link
end

Then(/^default web browser should open and display the Terms and Conditions page "([^"]*)"$/) do |visual_validate|
  	terms_page_check

  	if visual_validate == "eyes"
 		eyes_verify_screen(@app_name, 'Terms and Conditions Screen')
 	end
end


Then(/^the To field should be "(.*?)"$/) do |address|
	recipient = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailRecipient") }
	raise "Error: The To: field does not have the correct email address." unless recipient.attribute("text") == address
end


Then(/^the default text should be "(.*?)" "(.*?)"$/) do |text, visual_validate|
  	body = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailBody") }
	raise "Error: The default email text is not correct." unless body.attribute("text") == text

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Report a Problem Screen')
  	end
end


When(/^the user changes the To field to personal email "(.*?)"$/) do |email|
  	recipient = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailRecipient") }
  	recipient.clear
  	recipient.type email
  	#click_back_button
end


When(/^the user changes the Body to "(.*?)"$/) do |text|
  	body = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailBody") }
  	body.clear
  	body.type text
  	#click_back_button
end


When(/^the user enters the first name$/) do
	enter_contact_field("First Name *", $test["contact2_fname"])
end


When(/^the user enters the company name$/) do
  	enter_contact_field("Company *", $test["contact2_company"])
end


When(/^the user enters the email$/) do
  	enter_contact_field("Email *", $test["contact2_email"])
end


When(/^the user enters the last name$/) do
    enter_contact_field("Last Name *", $test["contact2_lname"])
end


When(/^the user scrolls to the "(.*?)" field$/) do |field|
  	@driver.scroll_to_exact(field)
end


When(/^the user enters an incorrectly formatted email address$/) do
  enter_new_contact_invalid_email
end


Then(/^the user should be on the Topics page "(.*?)"$/) do |visual_validate|
  	topics_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Topics Screen')
  	end
end


Then(/^the user should be on the Add Follow Up Actions page "(.*?)"$/) do |visual_validate|
	followup_actions_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Follow Up Actions Screen')
  	end
end


Then(/^the user should be on the Contacts page "(.*?)"$/) do |visual_validate|
  	contacts_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Contacts Screen')
  	end
end


When(/^the user sets the geographic location to "(.*?)"$/) do |location|
	case location
	when "NYC"
		latitude = 40.71448
		longitude = -74.00598
		altitude = 75
	else
		raise "Error: Unknown geo location."
	end
	@driver.set_location(latitude: latitude, longitude: longitude, altitude: altitude)
	sleep 10
end


When(/^the user opens the Google Maps app$/) do
	click_android_home_button
	click_apps_icon
	launch_gmaps_app
	sleep 10
	#raise "Testing!!!"
end


When(/^the user opens the Checkin app$/) do
	click_android_home_button
	click_apps_icon
	launch_cs_app
end


When(/^the user closes the GPS error dialog if necessary$/) do
	close_gps_error_dialog
end


When(/^the user selects the "(.*?)" tab on the Contacts page "(.*?)"$/) do |tab_name, visual_validate|
  	click_contacts_tab(tab_name)

  	if visual_validate == "eyes"
    	screen_name = "Contacts Screen - #{tab_name} Tab"
    	eyes_verify_screen(@app_name, screen_name)
  	end
end


When(/^the user selects All Events$/) do
  	select_all_events_on_contacts_page
end


When(/^the user deletes existing contacts for "(.*?)"$/) do |event_list|
  	delete_all_contacts(event_list)
end


When(/^the user manually enters new contact "(.*?)"$/) do |name|
  	add_new_contact(name)
end


Then(/^the new contact "(.*?)" should be in the Contacts list "(.*?)"$/) do |name, visual_validate|
  	#check_new_contact_added(name)
  	
    sleep 5
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contacts Screen with #{name}")
  	end
end


When(/^the user adds a Topic and Follow\-up Action for "(.*?)"$/) do |lname|
  	add_topic_fua(lname)
end


Then(/^the new contact "(.*?)" should be at the top of the Contact list with a New badge "(.*?)"$/) do |lname, visual_validate|
  	#new_contact?(lname)

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "#{lname} Top of Contacts List Screen")
  	end
end


When(/^the user opens all existing contacts$/) do
	open_all_contacts
end


When(/^the user clicks the contact "(.*?)"$/) do |lname|
  sleep 3
  click_contact(lname)
end


Then(/^the Contact info panel should appear$/) do
	contact_info_page_check
end


Then(/^the contact information for the contact should match "(.*?)" "(.*?)"$/) do |lname, visual_validate|
  	#verify_contact_info(lname)

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "#{lname} Contact Info Screen")
  	end
end


Then(/^the Contact list should be in alphabetic order "(.*?)"$/) do |visual_validate|
  	click_contacts_tab("ALL")
  	select_all_events_on_contacts_page
  	verify_contacts_fname_alpha_order

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contacts Screen in Alphabetical Order")
  	end
end


When(/^the user searches for the contact with "(.*?)" "(.*?)"$/) do |key, value|
  	search_for_contact(value)
end


When(/^the user clicks the first contact$/) do
  	click_first_contact
end


Then(/^the contact with "(.*?)" "(.*?)" should not be returned by the search$/) do |key, value|
  	verify_no_contacts
end


Then(/^the user should be on the Edit Contact page for "(.*?)"$/) do |lname|
  	edit_contact_page_check(lname)
end


When(/^the user changes the "(.*?)" from "(.*?)" to "(.*?)"$/) do |key, old_value, new_value|
  	edit_field(key, old_value, new_value)
end


Then(/^the contact information for "(.*?)" will have "(.*?)" "(.*?)"$/) do |lname, key, value|
  	contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    email = $test["#{contact}_email"]

    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fullname}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{value}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{email}\")").displayed? }
end


Then(/^the Sort Contacts page should appear "(.*?)"$/) do |visual_validate|
  	sort_contacts_page_check
  	
  	if visual_validate
    	eyes_verify_screen(@app_name, 'Sort Contacts Screen')
  	end
end


When(/^the user selects to sort the contacts into "(.*?)" "(.*?)" order$/) do |order, criteria|
  	sort_contacts(order, criteria)
end

When(/^the user sorts the contacts by "(.*?)" into "(.*?)" order$/) do |column, order|
  sort_contacts(column, order)
end


Then(/^the Contacts list should be in "(.*?)" "(.*?)" order by last name "(.*?)"$/) do |order, criteria, visual_validate|
  	verify_contacts_sorted(order, criteria)
  	
  	if visual_validate
    	eyes_verify_screen(@app_name, "Sort Contacts #{order} Alphabetical Order Screen")
  	end
end


When(/^the user clicks the "(.*?)" sort option$/) do |option|
	case option
	when "Alphabetically (Ascending)"
		@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterAsc").click }
	when "Alphabetically (Descending)"
		@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterDesc").click }
	when "By Date (Ascending)"
		@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterAsc").click }
	when "By Date (Descending)"
		@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterDesc").click }
	end
end


Then(/^only the "(.*?)" option should be selected "(.*?)"$/) do |option, visual_validate|
 	alpha_ascend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterAsc") }
  	alpha_descend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterDesc") }
  	date_ascend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterAsc") }
  	date_descend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterDesc") }

  	case option
  	when "Alphabetically (Ascending)"
    	raise "Error: Only Alphabetic Ascending option should be selected." unless alpha_ascend_element.attribute("checked") == true && alpha_descend_element.attribute("checked") == false && date_ascend_element.attribute("checked") == false && date_descend_element.attribute("checked") == false
  	when "Alphabetically (Descending)"
    	raise "Error: Only Alphabetic Descending option should be selected." unless alpha_ascend_element.attribute("checked") == false && alpha_descend_element.attribute("checked") == true && date_ascend_element.attribute("checked") == false && date_descend_element.attribute("checked") == false
  	when "By Date (Ascending)"

  	when "By Date (Descending)"
  		
  	end

  	if visual_validate
    	eyes_verify_screen(@app_name, "#{order} Sort Option Screen")
  	end
end


When(/^the user clears the Sort options$/) do
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/sortClearButton").click }
end


Then(/^no Sort options should be selected "(.*?)"$/) do |visual_validate|
  	alpha_ascend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterAsc") }
  	alpha_descend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterDesc") }
  	date_ascend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterAsc") }
  	date_descend_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterDesc") }

  	if alpha_ascend_element.attribute("checked") == true || alpha_descend_element.attribute("checked") == true || date_ascend_element.attribute("checked") == true || date_descend_element.attribute("checked") == true
    	raise "Error: No sorting option should be selected."
  	end

  	if visual_validate
    	eyes_verify_screen(@app_name, 'No Sort Options Screen')
  	end
end


Then(/^the Filter Contacts page should appear "(.*?)"$/) do |visual_validate|
  	filter_contacts_page_check
  	
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Filter Contacts Screen')
  	end 
end


When(/^the user filters by the "(.*?)" named "(.*?)"$/) do |filter, value|
  	filter_contacts(filter, value)
end


When(/^the user applies the filter$/) do
  	apply_filter
end


Then(/^the Filter button will show "(.*?)" filter applied$/) do |number|
  	verify_filtered_number(number)
end


Then(/^the "(.*?)" tab will indicate the correct number of contacts$/) do |tabname|
	verify_tab_count(tabname)
end

Then(/^the contact list will contain "(.*?)" contacts$/) do |number|
  verify_contact_list_count(number)
end


Then(/^the Contacts list should contain contact\(s\) "(.*?)"$/) do |contact_list|
	verify_contact_list(contact_list)
end


Then(/^"(.*?)" should be highlighted "(.*?)"$/) do |list, visual_validate|
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, '#{list} Filters Highlighted Screen')
  	end 
end


When(/^the user clears all filters$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/filterClearButton").click }
end


Then(/^no filters should be highlighted "(.*?)"$/) do |visual_validate|
	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'No Filters Highlighted Screen')
  	end 
end


Then(/^the Filter button will show no filters applied$/) do
  	filter_text = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactsPageFilterButton") }
  	raise "Error: Expected 0 Filters applied." unless filter_text.attribute("text") == "Filter"
end


When(/^the user clicks the Filter button$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/attendeeFilterButton").click }
end


Then(/^the Contacts list should be empty "(.*?)"$/) do |visual_validate|
  	verify_all_contacts_deleted

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Empty Contacts Screen')
  	end
end


When(/^the user adds the "(.*?)" contacts$/) do |group|
  	if group == "Top Gear"
    	add_contacts ["Clarkson", "Hammond", "May"]
  	elsif group == "F1"
    	add_contacts ["Hamilton", "Vettel", "Rosberg"]
  	elsif group == "NFL"
    	add_contacts ["Brady", "Manning"]
    elsif group == "Starfleet"
    	add_contacts ["Kirk", "McCoy"]
  	elsif group == "Other"
    	add_contacts ["Brown", "Baggins", "Abrams"]
  	end
end


Then(/^the Contacts list should include "(.*?)" contacts "(.*?)"$/) do |group, visual_validate|
  	if group == "Top Gear"
    	verify_contacts_contained ["Clarkson", "Hammond", "May"]
  	elsif group == "F1"
    	verify_contacts_contained ["Hamilton", "Vettel", "Rosberg"]
  	elsif group == "NFL"
    	verify_contacts_contained ["Brady", "Manning"]
    elsif group == "Starfleet"
    	verify_contacts_contained ["Kirk", "McCoy"]
  	elsif group == "Other"
    	verify_contacts_contained ["Brown", "Baggins", "Abrams"]
  	end

  	if visual_validate == "eyes"
      	eyes_verify_screen(@app_name, "Contacts with #{group} Screen")
    end
end


When(/^the user deletes the first "(.*?)" contacts$/) do |number|
  	delete_contacts(number)
end


Then(/^the first "(.*?)" contacts will not be on the Contacts List$/) do |number|
 	verify_contacts_deleted(number)
end


When(/^the user logs out of the Checkin app$/) do
  	logout
end


Then(/^the contact with "(.*?)" "(.*?)" should be returned by the search$/) do |key, value|
  	verify_search_contact_returned(value)
end


Then(/^the contact page should contain a business card image for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
  	verify_contact_with_card_image

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contact #{attendee} with Business Card Screen")
  	end
end


When(/^the user clicks the magnify button$/) do
  	click_magnify_button
end


When(/^the user clicks the magnify button on the Edit Contact page$/) do
  	click_edit_contact_magnify_button
end


Then(/^the enlarged business card should appear for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
  	verify_magnified_card

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Enlarged #{attendee} Business Card Screen")
  	end
end


When(/^the user zooms in on the business card$/) do
  finger1 = Appium::TouchAction.new
  finger2 = Appium::TouchAction.new
  multi_fingers = Appium::MultiTouch.new

  finger1.long_press(x: 1280, y: 826).move_to(x: 0, y: -500).wait(500).release()
  finger2.long_press(x: 1280, y: 926).move_to(x: 0, y: 500).wait(500).release()

  multi_fingers.add(finger1)
  multi_fingers.add(finger2)
  multi_fingers.perform()
end


Then(/^the business card should zoomed in for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
	verify_magnified_card

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Zoomed #{attendee} Business Card Screen")
  	end
end


When(/^the user clicks the X icon$/) do
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/zoomCloseBtn").click }
end


Then(/^the Edit Contact page should open for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
	edit_contact_page_check2

	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "#{attendee} Edit Contact Screen")
	end
end


Then(/^the user should be on the Edit Follow-Up Actions page "(.*?)"$/) do |visual_validate|
  	edit_followup_actions_page_check

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, 'Edit Follow-Up Actions Screen')
  	end
end


Then(/^the "(.*?)" field should contain "(.*?)"$/) do |key, value|
  	verify_fieldvalue(key, value)
end


When(/^changes the text from "(.*?)" to "(.*?)"$/) do |old_text, new_text|
  	replace_text(old_text, new_text)
end


Then(/^the text should match "(.*?)"$/) do |text|
  	match_text(text)
end


When(/^the user clears the note "(.*?)"$/) do |text|
  	clear_note(text)
end


Then(/^the "(.*?)" field should be empty$/) do |key|
  	verify_empty_fieldvalue(key)
end


When(/^the user clicks the X icon on the Topics page$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicCloseBtn").click }
end


When(/^the user clicks the X icon on the Follow-Up Actions page$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/followupActionCloseBtn").click }
end

When(/^the user clicks the X icon in a note$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subValueCloseButton").click }
end


Then(/^the user should go back to the Edit Contact page$/) do
  	back_to_edit_contact_page_check
end


When(/^the user selects the "([^"]*)" topics$/) do |topic|
	@driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{topic}\")").click }
end


When(/^the user selects the "([^"]*)" follow-up actions$/) do |fua|
	@driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fua}\")").click }
end


Then(/^the sub\-topics should open with "(.*?)" sub\-topics$/) do |list|
  	subtopics_page_check(list)
end


Then(/^the Topics page should display "(.*?)" "(.*?)" in alpha order$/) do |topics_list, category|
  	verify_topics_list_alpha_order(topics_list, category)
end


When(/^the user scrolls to the bottom of the list$/) do
	action = Appium::TouchAction.new
    action.swipe(start_x: 600, start_y: 1755, end_x: 600, end_y: 573, duration: 1000)
    action.perform
end


Then(/^the sub-topics pop up should appear "(.*?)"$/) do |visual_validate|
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subTopicCloseBtn").displayed? }
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subDoneButton").displayed? }
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subClearButton").displayed? }

  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Sub-topics Screen")
  	end
end


Then(/^the sub\-topics page should display "(.*?)" "(.*?)" in alpha order$/) do |subtopics_list, category|
  	verify_topics_list_alpha_order(subtopics_list, category)
end



Then(/^the "(.*?)" topics should be "(.*?)"$/) do |list, state|
  	verify_topics_state(list, state)
end


Then(/^the "(.*?)" follow\-up actions should be "(.*?)"$/) do |list, state|
  	verify_topics_state(list, state)
end


Then(/^the sub follow-up actions should open with "(.*?)"$/) do |list|
	fuas_page_check(list)
end


Then(/^the Note pop\-up should appear with title "(.*?)" "(.*?)"$/) do |title, visual_validate|
  	verify_note_popup(title)
  	
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Note Screen - A test note")
  	end
end


When(/^changes the text from "(.*?)" to more than 500 characters$/) do |old_text|
  new_text = ""
  base = "abcdefghijk"

  # Enter 550 chars
  for i in 1..50 do
    new_text = new_text + base
  end

  replace_text(old_text, new_text)
  #click_button("Done")
end


Then(/^the note will only limit the entry to "(.*?)" characters$/) do |number|
	expected_count = "#{number}/500"
	char_count = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/counterText") }
	raise "Error: Expected character count to be exactly 500 chars." unless char_count.attribute("text") == expected_count
end


Then(/^the Free Text pop-up should appear with title "(.*?)"$/) do |title|
  	verify_note_popup(title)
end


Then(/^the FUAs page should display "(.*?)" "(.*?)" in alpha order$/) do |fua_list, category|
  	verify_fuas_list_alpha_order(fua_list, category)
end


Then(/^the sub follow\-up actions page should display "(.*?)" "(.*?)" in alpha order$/) do |subfua_list, category|
  	verify_fuas_list_alpha_order(subfua_list, category)
end


Then(/^the sub follow\-up actions pop up should appear$/) do
	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subTopicPageTitle").displayed? }
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subTopicCloseBtn").displayed? }
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subDoneButton").displayed? }
  	@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subClearButton").displayed? }
end


Then(/^the event dropdown should have "(.*?)" selected "(.*?)"$/) do |event, visual_validate|
  	selected_event = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/selectedEventItemTitleView") }
  	raise "Error: The Contacts event does not match the selected event." unless selected_event.attribute("text") == event
  	
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contacts Screen - #{event} Event Selected")
  	end
end


Then(/^the user should be on the "(.*?)" tab "(.*?)"$/) do |choice, visual_validate|
  	verify_contact_tab_choice(choice)
  
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contacts Screen - #{choice} Tab")
  	end
end


Then(/^the no contacts image should be displayed on the "(.*?)" tab "(.*?)"$/) do |tabname, visual_validate|
  	verify_empty_contacts_tab(tabname)
  	
  	if visual_validate == "eyes"
    	eyes_verify_screen(@app_name, "Contacts Screen - #{tabname} Tab No Contacts Image")
  	end
end


When(/^the user selects the "(.*?)" event on the Contact page$/) do |event|
  	select_event_contacts_page(event)
end


Then(/^the default map client should open with a map of the atEvent office "([^"]*)"$/) do |visual_validate|
  sleep 20

  if visual_validate == "eyes"
      eyes_verify_screen(@app_name, "Contacts US Screen - Office Map")
  end
end


Then(/^the "([^"]*)" tab should contain "([^"]*)" contacts$/) do |tabname, number|
  case tabname
  when "ALL"
    target = "ALL (#{number})"
    text_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabText") }
    raise "Error. The contact count for the #{tabname} tab is incorrect." unless text_element.attribute("text") == target
  when "TODAY"
    target = "TODAY (#{number})"
    text_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/todayTabBackground") }
    raise "Error. The contact count for the #{tabname} tab is incorrect." unless text_element.attribute("text") == target
  when "PENDING"
    target = "PENDING (#{number})"
    text_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/pendingTabBackground") }
    raise "Error. The contact count for the #{tabname} tab is incorrect." unless text_element.attribute("text") == target
  else
    raise "Error. Unknown #{tabname} tab."
  end
end


When(/^the user chooses the Chrome browser$/) do
  @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Chrome")').click }
end


When(/^the user clicks X to close the Add Contact panel$/) do
  @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/addNewAttendeeCrossButton").click }
end


When(/^the user opens the Select an Event dropdown$/) do
  open_select_event_dropdown
end


When(/^the user closes the Select an Event dropdown$/) do
  close_select_event_dropdown
end


When(/^the user closes the contact information panel$/) do
  @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/attendeeDetailCrossButton").click }
end

# Check that the named column is in ascending or descending order.
Then(/^the contacts "([^"]*)" should be in "([^"]*)" order$/) do |column, order|
  verify_sorted_column(column, order)
end

Then(/^the company should be "([^"]*)"$/) do |company|
  text = @driver.find_element(:id, "com.ihi.ateventfm:id/attendeeDetailCompany")
  raise "Error: The company did not match the expected #{company}" unless text.attribute("text") == company
end