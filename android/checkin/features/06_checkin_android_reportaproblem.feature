# encoding: UTF-8

@checkin_feature_report_a_problem @6
Feature: Report a Problem

Background:
  Given I am logged out on the app

@6.1
Scenario: 6.1 User selects Report a Problem
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to Report a Problem "eyes"
  Then the To field should be "support@at-event.com"
  And the default text should be "Describe your problem here" "eyes"

@6.2
Scenario: 6.2 User sends Report a Problem email
  Given the user is on the Login screen of the Checkin app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "eyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "eyes"
  When the user goes to Report a Problem "eyes"
  And the user changes the To field to personal email "kris.huynh@gmail.com"
  And the user changes the Body to "QA testing -- please ignore."
  And the user clicks "Send"
  Then the Scan and Check-In page should open "noeyes"