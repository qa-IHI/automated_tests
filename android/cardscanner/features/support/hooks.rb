# encoding: UTF-8

# Setup and Teardown sections
###############################################################################################################
# Setup section that executes before each test scenario.
# Set the Appium test capabilities.
# Start the Appium driver.
Before do |scenario|
    @app_name = $test["app_name"]# + "-" + $test["app_version"]
    @scenario_name = "Android Card Scanner: " + scenario.name

    # Use an Appium lib object
    def desired_capabilities
    {  caps:
        {
            #appiumVersion:      $appium_version,
            platformName:       $caps["platformName"],
            platformVersion:    $android_version,
            deviceName:         $android_device,
            #orientation:        $caps["orientation"],
            sendKeyStrategy:    $caps["sendKeyStrategy"],
            app:                $app_location,
            unicodeKeyboard:    $unicodeKeyboard,
            #appActivity:        ".activity.MainActivity", 
            #appWaitActivity:    ".activity.Splash",
            #appWaitActivity:    ".activity.MainActivity",
            appWaitActivity:    ".activity.*",
            #name:               @scenario_name,
            automationName:     "UiAutomator2",
            
            #noReset:            false, 
            #fullReset:          true,
            noReset:            true, 
            fullReset:          false,
            newCommandTimeout:  90,
        },
        appium_lib:
        {
            server_url:         $appium_url
        }
    }
    end

    raise "Error: Could not create a Appium Driver object." unless @driver = Appium::Driver.new(desired_capabilities)
	@driver.start_driver
    $job_id = @driver.session_id

    # Set up Applitools Eyes object for visual validation.
    if $eyes_enabled == "true"
        @eyes = Applitools::Selenium::Eyes.new
        @eyes.api_key = $eyes_key
    
        # Enable eyes logging to STDOUT
        #@eyes.log_handler = Logger.new(STDOUT)

        # Aggregate all test scenario validation points into it's own batch on the Applitools Eyes dashboard.
        @eyes_batch = Applitools::BatchInfo.new(@scenario_name)
    end

    @deleted_contacts = Array.new(10)

    # Create a new hash map for the names of fields in the app. We want to keep track of the field names in case they change during a session.
    $fields_hash = Hash.new
    $fields_hash["cleared_overlay"] = false

    @deleted_contacts = Array.new(10)
end


# Teardown section that executes after each test scenario.
# Report pass or fail to Sauce Labs.
# Stop the Appium driver.
# Clear the hash.
After do |scenario|
    if ENV['SERVER'] == "sauce"
        if ENV['SAUCE_USERNAME'] && !ENV['SAUCE_USERNAME'].empty? && ENV['SAUCE_ACCESS_KEY'] && !ENV['SAUCE_ACCESS_KEY'].empty?
            passed = !(scenario.failed?)
            SauceWhisk::Jobs.change_status($job_id, passed)
        end
    end

    @driver.driver_quit
    #@driver.quit

    if $eyes_enabled == "true"
        @eyes.abort_if_not_closed
    end

    $fields_hash.clear
end

##################################################################################################################################

def all_elements_visible?(list)
    for element in list do
        @driver.wait { @driver.find_element(:id, element).displayed? }
        #@driver.find_element(:id, element).displayed?
    end
end


def eyes_verify_screen(app_name, screen_name)
    if $eyes_enabled == "true"
        @eyes.batch = @eyes_batch
        @eyes.baseline_name = screen_name

        @eyes.test(app_name: app_name, test_name: screen_name, driver: @driver) do |driver|
            @eyes.check_window(screen_name)
        end
    end
end


def enter_valid_email(email)
    email_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPageUsernameEditText")
    email_element.clear
    email_element.type email
    #click_back_button
end


def enter_valid_password(password)
    pw_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPagePasswordEditText")
    pw_element.type password
    #click_back_button
end

# Android Keycode constants can be found here: https://developer.android.com/reference/android/view/KeyEvent.html
def click_back_button
    @driver.press_keycode 4
end


def click_android_home_button
    @driver.press_keycode 3
end


def click_cs_home_button
    @driver.wait { @driver.find_element(:accessibility_id, "Home").click }
end
   

def click_appswitch_button
    @driver.press_keycode 187
end


def click_apps_icon
    @driver.wait { @driver.find_element(:accessibility_id, "Apps list").click }
end


def kill_cs_process
    @driver.wait { @driver.find_element(:accessibility_id, "Dismiss atEvent 3.0.").click }
end


def launch_cs_app
    @driver.wait { @driver.find_element(:accessibility_id, "atEvent 3.0").click }
end


def launch_settings_app
    @driver.wait { @driver.find_element(:accessibility_id, "Settings").click }
end


def launch_gmaps_app
    @driver.wait { @driver.find_element(:accessibility_id, "Maps").click }
end
   

def select_atevent_process
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("atEvent 3.0")').click }
end


def click_login
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/loginButton").click }
end


def click_forgot_password
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/forgotPasswordView").click }
end


def click_cancel
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/forgotCancelButton").click }
end


def selectanevent_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("atEvent")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/eventSelectionView").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/eventSearchEditText").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Select an Event")').displayed? }
end

# Check for elements on the Scan and Checkin page
def scanandcheckin_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("atEvent")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/businessCardOption").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/BarCodeOption").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/manualAddOption").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nfcOption").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/checkinTab").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/homeScreenHelpBtn").displayed? }
end


# Check for elements on the Password Reset screen
def password_reset_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Accelerating Sales Velocity")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/forgotPasswordEmailEditText").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/forgotCancelButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/forgotSendButton").displayed? }
end


def keep_me_logged_in_checked?
    checkbox = @driver.find_element(:id, "com.ihi.ateventfm:id/keepMeLoggedInView")
    raise "Error: Expected Keep me logged in to be checked by default." unless checkbox.attribute("checked") == "true"
end


def set_keep_me_logged_in(choice)
    checkbox = @driver.find_element(:id, "com.ihi.ateventfm:id/keepMeLoggedInView")
    if choice == true
        checkbox.click unless checkbox.attribute("checked") == "true"
    elsif choice == false
        checkbox.click unless checkbox.attribute("checked") == "false"
    end
end


def wait_for_error(message)
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{message}\")").displayed? }
end


def enter_invalid_email_format
    email_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPageUsernameEditText")
    email_element.clear
    email_element.type "$$$$$$$$$$$$$"
    #click_back_button
end


def enter_empty_password
    pw_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPagePasswordEditText")
    pw_element.type ""
    #click_back_button
end


def enter_invalid_password
    pw_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPagePasswordEditText")
    pw_element.type "futbolllll"
    #click_back_button
end


def enter_invalid_email
    email_element = @driver.find_element(:id, "com.ihi.ateventfm:id/loginPageUsernameEditText")
    email_element.clear
    email_element.type "lio@messi.com"
    #click_back_button
end


def click_button(name)
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{name}\")").click }
end


def enter_invalid_reset_email
    email_element = @driver.find_element(:id, "com.ihi.ateventfm:id/forgotPasswordEmailEditText")
    email_element.clear
    email_element.type "lio@messi.com"
    #click_back_button
end


def enter_valid_reset_email(email)
    email_element = @driver.find_element(:id, "com.ihi.ateventfm:id/forgotPasswordEmailEditText")
    email_element.clear
    email_element.type $qa_reset_username
    #click_back_button
end


def choose_event_on_selectanevent(event)
    # First, click the dropdown menu
    # menu = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/eventsSpinner") }
    # menu.click

    # # Then choose the event
    # choice_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{event}\")") }
    # choice_element.click


    event_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/eventSelectionListView") }
    event_containers = event_list.find_elements(:class, "android.widget.LinearLayout")
    event_containers.each do |container|
        name = container.find_element(:id, "com.ihi.ateventfm:id/eventItemTitleView")
        if name.attribute("text") == event
            container.click
            break
        end
    end
end


def choose_event_on_contacts(event)
    # First, click the dropdown menu
    menu = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactEventSpinner") }
    menu.click

    # Then choose the event
    choice_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{event}\")") }
    choice_element.click
end


def click_menu_button
    menu = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton") }
    menu.click
end


def contacts_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Contacts")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Select an event or occasion")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactsPageSearchBox").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactsPageFilterButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactsPageSortButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/todayTabBackground").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabBackground").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/pendingTabBackground").displayed? }
end


def contactus_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Contact Us")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactUsPhoneButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactUsEmailButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/getDirectionsButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/viewMapsButton").displayed? }
end


def nearby_contacts_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Nearby Contacts")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/seekBar1").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nearbyContactPageListView").displayed? }
end


def test_connectivity_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Test Connectivity")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/animView").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/testButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/exitButton").displayed? }
end


def navigate_to_contacts
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption3") }
    target.click
    contacts_page_check
end


def navigate_to_nearby_contacts
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption5") }
    target.click
    nearby_contacts_page_check
end


def navigate_to_test_connectivity
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption7") }
    target.click
    test_connectivity_page_check
end


def navigate_to_selectanevent
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption1") }
    target.click
    selectanevent_page_check
end


def navigate_to_reportaproblem
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption8") }
    target.click
    reportaproblem_page_check
end


def reportaproblem_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Contact Us")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("To:")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailRecipient").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Describe your problem here:")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportEmailBody").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportDoneButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/reportCancelButton").displayed? }
end


def about_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Contact Us")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/aboutPageVersionView").displayed? }
    verify_app_version
end


def verify_app_version
    version = "Version #{$app_version}"
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{version}\")").displayed? }
end


def add_contact_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Add Contact")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("First Name *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Last Name *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Email *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Company *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Office Address")').displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Phone")').displayed? }
    #@@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Fax")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Mobile")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Zip Code")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("City")').displayed? }
end


def contact_info_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Contact")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("First Name")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Last Name")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Company")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Mobile")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Email")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Office Address")').displayed? }
end


def phone_client_page_check(number)
    @driver.wait { @driver.find_element(:id, "com.android.dialer:id/dialpad_view").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "dial").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "*").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "#").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{number}\")").displayed? }
end


def topics_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Topics")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicPageListView").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
end


def followup_actions_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Follow Up Actions")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/followupActionCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/actionPageListView").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
end


def navigate_to_contactus
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption9") }
    target.click
    contactus_page_check
end


def navigate_to_selectanevent
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption1") }
    target.click
    selectanevent_page_check
end


def navigate_to_about
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption6") }
    target.click
    about_page_check
end


def navigate_to_scanandcheckin
    click_menu_button
    target = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption2") }
    target.click
    scanandcheckin_page_check
end


def verify_app_menu_highlight(option)
    case option
    when "Select an Event"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption1") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Select an Event Screen with Menu Highlight')
    when "Scan & Check-In"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption2") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Scan & Check-In Screen with Menu Highlight')
    when "Contacts"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption3") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Contacts Screen with Menu Highlight')
    when "Nearby Contacts"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption4") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Attendee Alerts Screen with Menu Highlight')
    when "Nearby Contacts"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption5") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Nearby Contacts Screen with Menu Highlight')
    when "About"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption6") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'About Screen with Menu Highlight')
    when "Test Connectivity"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption7") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Test Connectivity Screen with Menu Highlight')
    when "Report a Problem"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption8") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Report a Problem Screen with Menu Highlight')
    when "Contact Us"
        highlight = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption9") }
        raise "Error: Expected the menu item #{option} to be highlighted." unless highlight.attribute("selected") == "true"
        eyes_verify_screen(@app_name, 'Contact Us Screen with Menu Highlight')
    end
end


def click_phone_button
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactUsPhoneButton").click }
end


def click_terms_link
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("http://at-event.com/terms-of-use")').click }
end


def terms_page_check
    @driver.wait { @driver.find_element(:id, "com.android.chrome:id/url_bar").displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("http://at-event.com/terms-of-use")').displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "Terms & Conditions").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.android.browser:id/webview_wrapper").displayed? }
end


def enter_contact_field(field, value)
    data_rows = @driver.find_elements(:class, "android.widget.LinearLayout")
    data_rows.each do |row|
        text_elements = row.find_elements(:class, "android.widget.TextView")
        if text_elements.size > 0
            if text_elements[0].attribute("text") == field
                entry_fields = row.find_elements(:class, "android.widget.FrameLayout")
                if entry_fields.size > 0
                    field_element = entry_fields[0].find_element(:class, "android.widget.EditText")
                    field_element.clear
                    field_element.type value
                end
                break
            end
        end
    end
end


def enter_new_contact_invalid_email
    email_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Email*")') }
    email_element.clear
    email_element.type "$$$$$$$$$$$$$"
    #click_back_button
end


def close_gps_error_dialog
    begin
        gps_error = @driver.find_element(:uiautomator, 'new UiSelector().text("GPS Settings")')

        if gps_error
            @driver.find_element(:uiautomator, 'new UiSelector().text("Cancel")').click
        end
    rescue
        return
    end
end


def click_contacts_tab(tab_name)
    case tab_name
    when "TODAY"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/todayTabBackground").click }
    when "ALL"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabBackground").click }
    when "PENDING"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/pendingTabBackground").click }
    else
        raise "Error: Could not click on the #{tab_name} tab on the Contacts Page."
    end
end


def select_all_events_on_contacts_page
    # First, click the dropdown menu
    menu = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactEventSpinner") }
    menu.click

    # Then choose the event
    choice_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("All events and occasions")') }
    choice_element.click
end


def click_contact_delete
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactItemDeleteImage").click }
end


def empty_contact_list?
    begin
        empty_list = @driver.find_element(:id, "com.ihi.ateventfm:id/noContactImageView").displayed?
        true if empty_list
    rescue
        false
    end
end


def delete_all_contacts    
    while !empty_contact_list? do     
        action = Appium::TouchAction.new
        action.long_press(x: 43, y: 1374)
        action.perform
        click_contact_delete
        wait_for_error("Delete Contact")
        click_button("Delete")
        sleep 3
    end
end


def add_new_contact(name)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(name)

    enter_contact_field("First Name *", $test["#{contact}_fname"])
    enter_contact_field("Last Name *", $test["#{contact}_lname"])
    enter_contact_field("Email *", $test["#{contact}_email"])
    enter_contact_field("Company *", $test["#{contact}_company"])
    enter_contact_field("Office Address", $test["contact_street"])
    enter_contact_field("Mobile", $test["contact_mobile"])
    enter_contact_field("Zip Code", $test["contact_zip"])
    enter_contact_field("City", $test["contact_city"])
    enter_contact_field("State", $test["contact_state"])
    @driver.scroll_to("Website")
    enter_contact_field("Country", $test["contact_country"])
    enter_contact_field("Phone", $test["contact_phone"])
    enter_contact_field("Title", $test["#{contact}_job_title"])
    enter_contact_field("Comments", $test["contact_comments"])
    enter_contact_field("Website", $test["contact_web"])
    @driver.scroll_to("Done")
end


def determine_contact(criteria)
    case criteria
    when $test["contact0_fname"], $test["contact0_lname"], $test["contact0_company"], $test["contact0_email"] # special chars
        contact = "contact0"
    when $test["contact1_fname"], $test["contact1_lname"], $test["contact1_company"], $test["contact1_email"] # Messi
        contact = "contact1"
    when $test["contact2_fname"], $test["contact2_lname"], $test["contact2_company"], $test["contact2_email"] # Ronaldo
        contact = "contact2"
    when $test["contact3_fname"], $test["contact3_lname"], $test["contact3_company"], $test["contact3_email"] # international chars
        contact = "contact3"
    when $test["contact4_fname"], $test["contact4_lname"], $test["contact4_company"], $test["contact4_email"] # Clarkson
        contact = "contact4"
    when $test["contact5_fname"], $test["contact5_lname"], $test["contact5_company"], $test["contact5_email"] # Hammond
        contact = "contact5"
    when $test["contact6_fname"], $test["contact6_lname"], $test["contact6_company"], $test["contact6_email"] # May
        contact = "contact6"
    when $test["contact7_fname"], $test["contact7_lname"], $test["contact7_company"], $test["contact7_email"] # Hawking
        contact = "contact7"
    when $test["contact8_fname"], $test["contact8_lname"], $test["contact8_company"], $test["contact8_email"] # Brown
        contact = "contact8"
    when $test["contact9_fname"], $test["contact9_lname"], $test["contact9_company"], $test["contact9_email"] # Abrams
        contact = "contact9"
    when $test["contact10_fname"], $test["contact10_lname"], $test["contact10_company"], $test["contact10_email"] # Baggins
        contact = "contact10"
    when $test["contact11_fname"], $test["contact11_lname"], $test["contact11_company"], $test["contact11_email"] # Hamilton
        contact = "contact11"
    when $test["contact12_fname"], $test["contact12_lname"], $test["contact12_company"], $test["contact12_email"] # Vettel
        contact = "contact12"
    when $test["contact13_fname"], $test["contact13_lname"], $test["contact13_company"], $test["contact13_email"] # Rosberg
        contact = "contact13"
    when $test["contact14_fname"], $test["contact14_lname"], $test["contact14_company"], $test["contact14_email"] # Manning
        contact = "contact14"
    when $test["contact15_fname"], $test["contact15_lname"], $test["contact15_company"], $test["contact15_email"] # Brady
        contact = "contact15"
    when $test["contact16_fname"], $test["contact16_lname"], $test["contact16_company"], $test["contact16_email"] # Kirk
        contact = "contact16"
    when $test["contact17_fname"], $test["contact17_lname"], $test["contact17_company"], $test["contact17_email"] # McCoy
        contact = "contact17"
    end

    return contact, $test["#{contact}_fname"] + " " + $test["#{contact}_lname"], $test["#{contact}_company"], $test["#{contact}_topic1"], $test["#{contact}_topic2"], $test["#{contact}_fua1"], $test["#{contact}_fua2"]
end


def check_new_contact_added(name)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(name)
    click_contacts_tab("ALL")
    found = false
    contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
    contacts = contact_list.find_elements(:class, "android.widget.LinearLayout")
    contacts.each do |person|
        text_name = person.find_element(:id, "com.ihi.ateventfm:id/contactItemTitleView")
        text_company = person.find_element(:id, "com.ihi.ateventfm:id/contactItemDescriptionView")

        if text_name.attribute("text") == fullname && text_company.attribute("text") == company
            found = true
        end
    end

    raise "Error: Expected the contact to be on the Contact List." unless found
end


def add_topic_fua(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    topics_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Add Topics")') }
    topics_element.click

    if topic1 =~ /\w+/
        topic1_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{topic1}\")") }
        topic1_element.click
    end

    if topic2 =~ /\w+/
        topic2_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{topic2}\")") }
        topic2_element.click
    end

    done_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")') }
    done_element.click
    fua_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Add Follow-Up Actions")') }
    fua_element.click

    if fua1 =~ /\w+/
        fua1_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fua1}\")") }
        fua1_element.click
    end

    if fua2 =~ /\w+/
        fua2_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fua2}\")") }
        fua2_element.click
    end

    done_element = @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")') }
    done_element.click
end


def new_contact?(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    if !empty_contact_list?     
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contacts = contact_list.find_elements(:class, "android.widget.RelativeLayout")
        first_contact = contacts[0]

        name = first_contact.find_element(:id, "com.ihi.ateventfm:id/contactItemTitleView")
        raise "Error: Contact at top of list does not match #{lname}." unless name.attribute("text") == fullname
        raise "Error: New contact does not have a New Badge." unless first_contact.find_element(:id, "com.ihi.ateventfm:id/contactItemNewImage")
    else
        raise "Error: The contact list is unexpectedly empty."
    end
end


# Open each contact in the Contacts page to clear out the New badge.
def open_all_contacts
    if !empty_contact_list?     
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contacts = contact_list.find_elements(:class, "android.widget.LinearLayout")
        contacts.each do |person|
            #person.click
            button = Appium::TouchAction.new
            button.press(X: 800, y: 1360).wait(1000).move_to(x: 800, y: 1360).release.perform
            click_back_button
        end
    end
end


def click_contact(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)
    contact_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fullname}\")") }
    if contact_element
        contact_element.click
    else
        raise "Error: Could not click on the #{fullname} contact."
    end
end


def verify_contact_info(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)
    
    name_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailNameView") }
    raise "Error: Could not find the expected contact name." unless name_element.attribute("text") == fullname && name_element.attribute("enabled") == "true"
    company_element = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailCompanyView") }
    raise "Error: Could not find the expected company name." unless company_element.attribute("text") == company && company_element.attribute("enabled") == "true"

    title = $test["#{contact}_job_title"]
    phone = $test["contact_phone"]
    mobile = $test["contact_mobile"]
    email = $test["#{contact}_email"]

    #@driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#title}\")").displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{phone}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{mobile}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{email}\")").displayed? }
end


def verify_contacts_fname_alpha_order
    if !empty_contact_list?     
        displayed_contacts = Array.new
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contacts = contact_list.find_elements(:class, "android.widget.LinearLayout")
        contacts.each do |person|
            name = person.find_element(:id, "com.ihi.ateventfm:id/contactItemTitleView").attribute("text")
            displayed_contacts.push(name)
        end

        sorted_contacts = Array.new
        sorted_contacts = displayed_contacts
        sorted_contacts.sort
        raise "Error: It doesn't look like the Contacts list is in alphabetic order." unless displayed_contacts = sorted_contacts
    end
end


def search_for_contact(value)
    search_element = @driver.find_element(:id, "com.ihi.ateventfm:id/contactsPageSearchBox")
    search_element.type value
end


def click_first_contact
    if !empty_contact_list?     
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        first_contact = contact_list.find_elements(:class, "android.widget.LinearLayout")[0]
        first_contact.click
    else
        raise "Error: The contact list is unexpectedly empty."
    end
end


def verify_no_contacts
    if !empty_contact_list? 
        raise "Error: Expected contact list to be empty."
    end
end


def edit_contact_page_check(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Edit Contact")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }

    fname = $test["#{contact}_fname"]
    lname = $test["#{contact}_lname"]
    #company = $test["#{contact}_company"]
    email = $test["#{contact}_email"]

    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fname}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{lname}\")").displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{company}\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{email}\")").displayed? }
end


def edit_contact_page_check2
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Edit Contact")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/editContactImageView").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("First Name *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Last Name *")').displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Job Title")').displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Company *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Company *")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Mobile")').displayed? }
    #@driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Fax")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Email *")').displayed? }
end


def sort_contacts_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Sort")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/sortCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterAsc").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterDesc").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterAsc").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterDesc").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
end


def filter_contacts_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Apply Filter")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/filterCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/companyFilter").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/jobFilter").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicsFilter").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/followupactionsFilter").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Apply")').displayed? }
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
end


def edit_field(key, old_value, new_value)
    text_fields = @driver.find_elements(:class, "android.widget.EditText")
    target = nil
    text_fields.each do |field|
        if field.attribute("text") == old_value
            target = field
            break
        end
    end

    if target
        target.click
        target.clear
        target.type new_value
    else
        raise "Error: Could not find the field #{old_value}."
    end
end


def sort_contacts(order, criteria)
    case criteria
    when "alphabetic"
        case order
        when "ascending"
            @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterAsc").click }
        when "descending"
            @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/nameFilterDesc").click }
        end
    when "date"
        case order
        when "ascending"
            @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterAsc").click }
        when "descending"
            @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/dateFilterDesc").click }
        end
    end

    @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').click
end


def verify_contacts_sorted(order, criteria)
    case criteria
    when "alphabetical"
        list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contacts = list.find_elements(:id, "com.ihi.ateventfm:id/contactItemTitleView")
        original_display = Array.new
        contacts.each do |person|
            lastname = person.attribute("text").split(' ')[1]
            original_display.push(lastname)
        end

        sorted_display = Array.new

        case order
        when "ascending"
            sorted_display = original_display.sort
        when "descending"
            sorted_display = original_display.sort.reverse
        end

        raise "Error: The contact list has not been sorted in #{order} #{criteria} order." unless original_display == sorted_display

    when "date"
        list_size = @driver.find_element(:id, "com.ihi.ateventfm:id/allTabText").attribute("text")[/\d+/].to_i

        if list_size > 5
            list_size = 5
        end
  
        original_dates = Array.new

        list_size.times do |n|
            list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
            person = list.find_elements(:id, "com.ihi.ateventfm:id/contactItemTitleView")[n]
            person.click
            scandate_element = @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailScanDateTimeView")
            scandate = scandate_element.attribute("text")
            date = Time.strptime(scandate, "%m/%d/%Y | %I:%M %p")
            original_dates.push(date)
            click_back_button
        end

        sorted_dates = Array.new

        case order
        when "ascending"
            sorted_dates = original_dates.sort
        when "descending"
            sorted_dates = original_dates.sort.reverse
        end

        raise "Error: The contact list has not been sorted in #{order} #{criteria} order." unless original_dates == sorted_dates
    end
end


def filter_contacts(filter, value)
    case filter
    when "Company"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/companyFilter").click }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Company")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
    when "Job Title"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/jobFilter").click }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Job Title")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
    when "Topics"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicsFilter").click }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Topics")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
    when "Follow Up Actions"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/followupactionsFilter").click }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Follow Up Actions")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Done")').displayed? }
        @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Clear")').displayed? }
    end

    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{value}\")").click }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subDoneButton").click }
end


def apply_filter
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/filterContactButton").click }
end


def verify_filtered_number(number)
    text = "Filter (#{number})"
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{text}\")").displayed? }
end


def verify_tab_count(tabname)
    tab_text = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabText").attribute("text") }
    tab_count = tab_text.scan(/\d+/).first

    contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
    contacts = contact_list.find_elements(:class, "android.widget.LinearLayout")
    
    raise "Error: The tab count does not match the number of actual contacts in the list." unless tab_count.to_i == contacts.length.to_i
end


def verify_contact_list(contact_list)
    names_array = Array.new
    contact_list.split(", ").each do |person|
        contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
        names_array.push(fullname)
    end

    displayed = @driver.find_elements(:id, "com.ihi.ateventfm:id/contactItemTitleView")
    original_display = Array.new
    displayed.each do |person|
        original_display.push(person.attribute("text"))
    end

    names_array.sort
    original_display.sort

    raise "Error: The displayed contact list does not match the expected list." unless (original_display - names_array).empty?
end


def verify_all_contacts_deleted
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/noContactImageView").displayed? }
end


def add_contacts(list)
    list.each do |person|
        contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
        click_add_contact_button
        add_new_contact(person)
        add_topic_fua(person)
        click_button("Done")
        close_gps_error_dialog
        click_cs_home_button
    end
end


def click_add_contact_button
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/manualAddOption").click }
end


def verify_contacts_contained(list)
    list.each do |person|
        contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
        @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{fullname}\")").displayed? }
    end 
end


def delete_contacts(number)
    for i in 0..(number.to_i - 1)
        if !empty_contact_list?
            contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
            first_contact = contact_list.find_elements(:class, "android.widget.LinearLayout")[0].find_element(:id, "com.ihi.ateventfm:id/contactItemTitleView")
            fullname = first_contact.attribute("text")
            
            action = Appium::TouchAction.new
            action.long_press(x: 43, y: 1374)
            action.perform
            click_contact_delete
            wait_for_error("Delete Contact")
            click_button("Delete")
            sleep 3

            @deleted_contacts.push(fullname)
        end
    end
end


def verify_contacts_deleted(number)
    if !empty_contact_list?
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contacts = contact_list.find_elements(:id, "com.ihi.ateventfm:id/contactItemTitleView")
        
        names_array = Array.new
        contacts.each do |person|
            names_array.push(person.attribute("text"))
        end

        for i in 0..(number.to_i - 1)
            fullname = @deleted_contacts.pop
            @deleted_contacts.push(fullname)

            if names_array.include?(fullname)
                raise "Error: Expected contact #{fullname} to have been deleted."
            end
        end
    end
end


def logout
    click_menu_button
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slidingOption10").click }
end


def verify_search_contact_returned(criteria)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(criteria)

    if !empty_contact_list?
        contact_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactPageListView") }
        contact = contact_list.find_elements(:class, "android.widget.LinearLayout")[0].find_element(:id, "com.ihi.ateventfm:id/contactItemTitleView")
        displayname = contact.attribute("text")
            
        if displayname != fullname
            raise "Error: Could not search for the given contact #{criteria}."
        end
    else
        raise "Error: The contact list is unexpectedly empty."
    end
end


def verify_contact_with_card_image
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Contact\")").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailCameraView").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailZoomView").displayed? }
end


def click_magnify_button
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactDetailZoomView").click }
end


def click_edit_contact_magnify_button
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/editContactZoomView").click }
end


def verify_magnified_card
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/zoomCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/zoomView").displayed? }
end


def edit_followup_actions_page_check
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Follow Up Actions\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Done\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Clear\")").displayed? }
    
end


def verify_fieldvalue(key, value)
    case key
    when "fullname"
        resource = "com.ihi.ateventfm:id/contactDetailNameView"
    when "Job Title"
        resource = "com.ihi.ateventfm:id/contactDetailDesignationView"
    when "Company" 
        resource = "com.ihi.ateventfm:id/contactDetailCompanyView"
    when "Email" 
        resource = "com.ihi.ateventfm:id/contactDetailEmailView"
    when "Comments"
        resource = "com.ihi.ateventfm:id/editContactCommentsView"
    else
        raise "Error: Could not find the field #{key} to verify."
    end

    field = @driver.wait { @driver.find_element(:id, resource) }
    raise "Error: The field did not have the expect value #{value}." unless field.attribute("text") == value
end


def replace_text(old_text, new_text)
    textfield = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/addNoteEditText") }
    textfield.clear
    textfield.type new_text
    #@driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subValueDoneButton").click }
end


def match_text(text)
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{text}\")").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subValueDoneButton").click }
end


def clear_note(text)
    textfield = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/addNoteEditText") }
    textfield.clear
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subValueDoneButton").click }
end


def verify_empty_fieldvalue(key)
    case key
    when "Comments"
        resource = "com.ihi.ateventfm:id/contactDetailCommentView"
    else
        raise "Error: Could not find the field #{key} to verify."
    end

    field = @driver.wait { @driver.find_element(:id, resource) }
    raise "Error: Expected the #{key} field to be empty." unless field.attribute("text") == ""
end


def back_to_edit_contact_page_check
    @driver.wait { @driver.find_element(:uiautomator, 'new UiSelector().text("Edit Contact")').displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/slideButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topBarCameraButton").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Edit Topics\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Edit Follow-Up Actions\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Done\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Cancel\")").displayed? }
end


def subtopics_page_check(list)
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subTopicCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Done\")").displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"Clear\")").displayed? }

    list.split(", ").each do |subtopic|
        @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{subtopic}\")").displayed? }
    end
end


def verify_topics_list_alpha_order(list, category)
    list_array = Array.new
    list.split(", ").each do |topic|
        list_array.push(topic)
    end

    list_array.sort
    visible_topics_array = Array.new

    if category == "topics"
        topics_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/topicPageListView") }
    elsif category == "sub-topics"
        topics_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subListView") }
    end

    topics = topics_list.find_elements(:id, "com.ihi.ateventfm:id/topicItemTextView")
    topics.each do |topic|
        if topic.displayed?
            visible_topics_array.push(topic.attribute("text"))
        end
    end

    raise "Error: Topics are incorrect." unless visible_topics_array == list_array
end


def verify_fuas_list_alpha_order(list, category)
    list_array = Array.new
    list.split(", ").each do |topic|
        list_array.push(topic)
    end

    list_array.sort
    visible_topics_array = Array.new

    if category == "FUAs"
        fuas_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/actionPageListView") }
    elsif category == "sub-FUAs"
        fuas_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subListView") }
    end

    fuas = fuas_list.find_elements(:id, "com.ihi.ateventfm:id/topicItemTextView")
    fuas.each do |fua|
        if fua.displayed?
            visible_topics_array.push(fua.attribute("text"))
        end
    end
        
    raise "Error: FUAs are incorrect." unless visible_topics_array == list_array
end


def verify_subfuas_list(subfua_list)
    list_array = Array.new
    subfua_list.split(", ").each do |item|
        list_array.push(item)
    end

    visible_subfuas_array = Array.new

    for i in 1..2
        subfuas_list = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subListView") }
        subfuas = subfuas_list.find_elements(:id, "com.ihi.ateventfm:id/topicItemTextView")
        subfuas.each do |subfua|
            if subfua.displayed?
                visible_subfuas_array.push(subfua.attribute("text"))
            end
        end
        scroll_to_list_bottom
    end
    raise "Error: SubFUAs are incorrect." unless (list_array - visible_subfuas_array).empty?   
end


def scroll_to_list_bottom
    action = Appium::TouchAction.new
    action.swipe(start_x: 600, start_y: 1755, end_x: 600, end_y: 573, duration: 1000)
    action.perform
end

def verify_topics_state(list, state)
    list.split(", ").each do |topic|

        # topic_element = @driver.wait { @driver.find_element(:id, topic) }
        # if state == "selected"
        #     raise "Error: The #{topic_element.name} topic is not #{state} as expected." unless topic_element.value == 1
        # elsif state == "deselected"
        #     raise "Error: The #{topic_element.name} topic is not #{state} as expected." unless topic_element.value != 1
        # end

        topics_array = Array.new
        if state == "selected"
            topic_view = @driver.find_elements(:class, "UIATableView")[1]
            topic_cells = topic_view.find_elements(:class, "UIATableCell")
            topic_cells.each do |cell|
                if cell.label == "Topic Selected"
                    cell_text = cell.find_element(:class, "UIAStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} topic is not selected as expected." unless topics_array.include? topic
        elsif state == "deselected"
            topic_view = @driver.find_elements(:class, "UIATableView")[1]
            topic_cells = topic_view.find_elements(:class, "UIATableCell")
            topic_cells.each do |cell|
                if cell.label == "Topic Not Selected"
                    cell_text = cell.find_element(:class, "UIAStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} topic is not deselected as expected." unless topics_array.include? topic
        end
    end
end


def fuas_page_check(list)
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subTopicCloseBtn").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subDoneButton").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subClearButton").displayed? }

    list.split(", ").each do |item|
        #@driver.wait { @driver.find_element(:id, subtopic).displayed? }
        @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{item}\")").displayed? }
    end
end


def verify_note_popup(title)
    #@driver.wait { @driver.find_element(:id, title).displayed? }
    @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{title}\")").displayed? }
    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/subValueDoneButton").displayed? }
end


def verify_contact_tab_choice(choice)
    # ui_buttons_list = @driver.find_elements(:class, "UIAButton")
    
    # text_display = "^#{choice} Tab"
    # ui_buttons_list.each do |button|
    #     if button.name =~ /#{text_display}/
    #         return
    #     end
    # end
    # raise "Error: Could not find the #{choice} tab displayed."

    if choice == "ALL"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabText").displayed? }
    elsif choice == "TODAY"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/todayTabText").displayed? }
    elsif choice == "PENDING"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/pendingTabText").displayed? }
    end 
end


def verify_empty_contacts_tab(tabname)
    if tabname == "TODAY"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/todayTabText").displayed? }
    elsif tabname == "ALL"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/allTabText").displayed? }
    elsif tabname == "PENDING"
        @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/pendingTabText").displayed? }
    end

    @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/noContactImageView").displayed? }
end


def select_event_contacts_page(event)
    menu = @driver.wait { @driver.find_element(:id, "com.ihi.ateventfm:id/contactEventSpinner") }
    menu.click

    # Then choose the event
    choice_element = @driver.wait { @driver.find_element(:uiautomator, "new UiSelector().text(\"#{event}\")") }
    choice_element.click
end