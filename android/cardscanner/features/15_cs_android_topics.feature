# encoding: UTF-8

@cs_feature_topics @qa_ready @15 @coretests
Feature: Topics

Background:
  Given I am logged out on the app

@15.1
Scenario: 15.1 Add Top Gear contacts
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    Then the Scan and Check-In page should open "noeyes"
    When the user navigates to Contacts "eyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    And the user deletes existing contacts
    Then the Contacts list should be empty "eyes"
    When the user navigates to Select an Event "noeyes"
    And the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan and Check-In page should open "noeyes"
    When the user adds the "Top Gear" contacts
    And the user navigates to Contacts "eyes"
    And the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    Then the Contacts list should include "Top Gear" contacts "eyes"
    And the "ALL" tab will indicate the correct number of contacts

@15.2
Scenario: 15.2 Add NFL contacts
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    Then the Scan and Check-In page should open "noeyes"
    When the user adds the "NFL" contacts
    And the user navigates to Contacts "eyes"
    And the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    Then the Contacts list should include "NFL" contacts "eyes"
    And the "ALL" tab will indicate the correct number of contacts

@15.3
Scenario: 15.3 Press the X button on the Topics page
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    And the user navigates to Contacts "noeyes"
    And the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Clarkson"
    Then the contact with "Last Name" "Clarkson" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "Clarkson" "eyes"
    When the user scrolls to the "Web" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Clarkson" "noeyes"
    When the user scrolls to the "Edit Topics" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user clicks the X icon on the Topics page
    Then the user should go back to the Edit Contact page

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @15.4
Scenario: 15.4 Select and de-select a topic that has no sub-topics
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    And the user navigates to Contacts "noeyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    Then the contact with "Last Name" "May" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "May" "eyes"
    When the user scrolls to the "Web" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "May" "noeyes"
    When the user scrolls to the "Edit Topics" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Android" topics
    Then the "Android" topics should be "selected"
    When the user deselects the "Android" topics
    Then the "Android" topics should be "deselected"

@15.5
Scenario: 15.5 Select a topic that has sub-topics
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    And the user navigates to Contacts "noeyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Brady"
    Then the contact with "Last Name" "Brady" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "Brady" "eyes"
    When the user scrolls to the "Web" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Brady" "noeyes"
    When the user scrolls to the "Edit Topics" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Goalkeeping" topics
    Then the sub-topics should open with "Long Kick, Punch, Throw In" sub-topics

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @15.6
Scenario: 15.6 Select and de-select a sub-topic
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    And the user navigates to Contacts "noeyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Brady"
    Then the contact with "Last Name" "Brady" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "Brady" "eyes"
    When the user scrolls to the "Web" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Brady" "noeyes"
    When the user scrolls to the "Edit Topics" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page
    When the user selects the "Corner Kicks" topics
    Then the sub-topics should open with "Center, Left, Right" sub-topics
    When the user selects the "Left" topics
    Then the "Left" topics should be "selected"
    When the user deselects the "Left" topics
    Then the "Left" topics should be "deselected"

@15.7
Scenario: 15.7 Add Starfleet contacts
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
    Then the Scan and Check-In page should open "noeyes"
    When the user adds the "Starfleet" contacts
    And the user navigates to Contacts "eyes"
    And the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    Then the Contacts list should include "Starfleet" contacts "eyes"

@15.8
Scenario: 15.8 Long list of topics and sub-topics
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
    And the user navigates to Contacts "eyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Kirk"
    Then the contact with "Last Name" "Kirk" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "Kirk" "eyes"
    When the user scrolls to the "Edit" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Kirk" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    And the Topics page should display "Alfa Romeo, Aston Martin, Benetton, Brabham, Caterham, Ferrari, Force India, Honda" "topics" in alpha order
    When the user scrolls to the bottom of the list
    And the user waits "10" seconds
    Then the Topics page should display "Lancia, Lotus, Marussia, McLaren, Mercedes, Porsche, Red Bull Racing, Sauber" "topics" in alpha order
    When the user scrolls to the bottom of the list
    Then the Topics page should display "Marussia, McLaren, Mercedes, Porsche, Red Bull Racing, Sauber, Toro Rosso, Williams" "topics" in alpha order
    When the user clicks "Williams"
    Then the sub-topics pop up should appear "eyes"
    And the sub-topics page should display "A, B, C, D, E, F, G, H" "sub-topics" in alpha order
    When the user scrolls to the bottom of the list
    And the user waits "10" seconds
    Then the sub-topics page should display "J, M, N, O, P, S, T, U" "sub-topics" in alpha order
    When the user scrolls to the bottom of the list
    Then the sub-topics page should display "N, O, P, S, T, U, X, Z" "sub-topics" in alpha order

# Currently cannot be automated because Appium cannot determine whether a Topic/FUA is in a selected or deselected state.
@wip @15.9
  Scenario: 15.9 Topics clear button
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    And the user navigates to Contacts "eyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    Then the contact with "Last Name" "May" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "May" "eyes"
    When the user scrolls to the "Comments" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "May" "noeyes"
    When the user scrolls to the "State" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Android" topics
    Then the "Android" topics should be "selected"
    When the user selects the "Checkin" topics
    Then the "Checkin" topics should be "selected"
    When the user selects the "iOS" topics
    Then the "iOS" topics should be "selected"
    When the user clicks "Clear"
    Then all topics should be de-selected

# Currently cannot be automated. Appium cannot tell if a topic is selected or deselected.
@wip @15.10
Scenario: 15.10 Add new contact with topics but no follow up actions
    Given the user is on the Login screen of the Card Scanner app "eyes"
    And the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    And the user navigates to Contacts "eyes"
    Then the user should be on the Contacts page "noeyes"
    When the user selects the "ALL" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user deletes existing contacts
    Then the Contacts list should be empty "eyes"
    When the user navigates to Select an Event "noeyes"
    And the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan and Check-In page should open "noeyes"
    When the user adds the "F1" contacts
    And the user navigates to Contacts "eyes"
    And the user selects the "ALL" tab on the Contacts page "noeyes"
    And the user selects All Events
    Then the Contacts list should include "F1" contacts "eyes"
    And the "ALL" tab will indicate the correct number of contacts
    When the user searches for the contact with "Last Name" "Hamilton"
    Then the contact with "Last Name" "Hamilton" should be returned by the search
    When the user clicks the first contact
    Then the Contact info page should appear
    And the contact information for the contact should match "Hamilton" "eyes"
    When the user scrolls to the "Comments" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Hamilton" "noeyes"
    When the user scrolls to the "State" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    And the "Python, Ruby" topics should be "selected"
