# encoding: UTF-8

@cs_feature_contact_info_page @qa_ready @13
Feature: Contact Info Page

Background:
  Given I am logged out on the app

@13.1
Scenario: 13.1 Back button from Contact Info page
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user clicks the first contact
  Then the Contact info page should appear
  When the user clicks the Android Back button
  Then the user should be on the Contacts page "eyes"

@13.2
Scenario: 13.2 Enlarge contact business card image
 	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "wardhaugh" "eyes"

@13.3
Scenario: 13.3 View a business card in full screen and zoom in
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "wing"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wing" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "wing" "eyes"
  When the user zooms in on the business card
  Then the business card should zoomed in for "wing" "eyes"

@13.4
Scenario: 13.4 View a business card in full screen and press the X icon
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "wardhaugh" "eyes"
  When the user clicks the X icon
  Then the contact page should contain a business card image for "wardhaugh" "eyes"

@13.5
Scenario: 13.5 Click edit on the contact info page
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "wardhaugh"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "wardhaugh" "eyes"
  When the user scrolls to the "Web" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "wardhaugh" "eyes"
