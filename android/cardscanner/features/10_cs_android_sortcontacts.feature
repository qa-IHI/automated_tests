# encoding: UTF-8

@cs_feature_sort_contacts @qa_ready @10
Feature: Sort Contacts

Background:
  Given I am logged out on the app

@10.1
Scenario: 10.1 Use the Sort button on the Contacts page
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user clicks "Sort"
   Then the Sort Contacts page should appear "eyes"

@10.2
Scenario: 10.2 Choose a contact after sorting contacts
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user selects to sort the contacts into "ascending" "alphabetical" order
   And the user clicks "Sort"
   And the user selects to sort the contacts into "descending" "alphabetical" order
   And the user clicks the contact "Messi"
   Then the Contact info page should appear
	And the contact information for the contact should match "Messi" "eyes"


@10.3
Scenario: 10.3 Sort the Contacts list in ascending alphabetical order (Last Name)
	Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user selects to sort the contacts into "descending" "alphabetic" order
   And the user clicks "Sort"
   And the user selects to sort the contacts into "ascending" "alphabetic" order
   Then the Contacts list should be in "ascending" "alphabetical" order by last name "eyes"


@10.4
Scenario: 10.4 Sort the Contacts list in descending alphabetical order (Last Name)
	Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user selects to sort the contacts into "ascending" "alphabetic" order
   And the user clicks "Sort"
   And the user selects to sort the contacts into "descending" "alphabetic" order
   Then the Contacts list should be in "descending" "alphabetical" order by last name "eyes"


@10.5
Scenario: 10.5 Sort the Contacts list in ascending order by scan date
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user selects to sort the contacts into "descending" "date" order
   And the user clicks "Sort"
   And the user selects to sort the contacts into "ascending" "date" order
   Then the Contacts list should be in "ascending" "date" order "eyes"


@10.6
Scenario: 10.6 Sort the Contacts list in descending order by scan date
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user selects to sort the contacts into "ascending" "date" order
   And the user clicks "Sort"
   And the user selects to sort the contacts into "descending" "date" order
   Then the Contacts list should be in "descending" "date" order "eyes"



# Currently fails because we cannot tell if an option is selected or deselected. Open bug.
@wip @10.7
Scenario: 10.7 Clear the sort contacts options
	Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks "Sort"
   And the user clicks the "Alphabetically (Ascending)" sort option
   Then only the "Alphabetically (Ascending)" option should be selected "eyes"
   When the user clears the Sort options
   Then no Sort options should be selected "eyes"
   When the user clicks the "Alphabetically (Descending)" sort option
   Then only the "Alphabetically (Descending)" option should be selected "eyes"
   When the user clears the Sort options
   Then no Sort options should be selected "eyes"
   	