# encoding: UTF-8

@cs_feature_contact_us @qa_ready @4
Feature: Contact Us

Background:
  Given I am logged out on the app

@4.1
Scenario: 4.1 Home button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Home button
  Then the Scan and Check-In page should open "noeyes"

@4.2
Scenario: 4.2 Menu icon from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Menu Icon
  Then the app menu should open and "Contact Us" should be highlighted "eyes"


# Currently not supported. This test case requires setting up a actual email account first on the Android emulator in order to send emails.
@wip @4.3
Scenario: 4.3 Mail button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Email button
  Then the default email client should open with recipient address set to info@at-event.com "eyes"


# Currently, the Android emulator doesn't support GPS.
# We should inject latitude 37.3318 and longitude -122.0312 in the the Android emulator if we can. This is the location of Apple HQ in Cupertino.
@wip @4.4
Scenario: 4.4 Get Directions button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  When the user goes to the Contact Us page "eyes"
  And the user clicks "Get Directions"
  Then the default map client should open with directions from the current location to the atEvent office "eyes"


# Currently, the Android emulator doesn't support GPS.
@wip @4.5
Scenario: 4.5 View Map button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  When the user goes to the Contact Us page "eyes"
  And the user clicks "View Map"
  Then the default map client should open with a map of the atEvent office "eyes"
  
@4.6 
Scenario: Phone call button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Phone button
  Then the phone client should open with phone number "(925) 394-4440"
