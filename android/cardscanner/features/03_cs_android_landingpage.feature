# encoding: UTF-8

@cs_feature_landing_page @qa_ready @3 @coretests
Feature: Landing Page

Background:
  Given I am logged out on the app

@3.1
Scenario: 3.1 Event name
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to other pages from Scan and Check-In
   And the user navigates back to the Scan and Check-In page
   Then the Scan and Check-In page should open "noeyes"
   And the chosen Event should remain the same

@wip @3.2
Scenario: 3.2 Tutorial
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user successfully logs in as "QA"
   Then the user is logged into the app and the tutorial is displayed "eyes"
   When the user clears the tutorial overlay
   Then the user is on the landing page "eyes"
   When the user clicks the tutorial question mark on the Scan a Card page
   Then the tutorial overlay should be opened "eyes"
   When the user clears the tutorial overlay
   Then the user is on the landing page "noeyes"

@3.3
Scenario: 3.3 Menu icon from Scan and Check-In
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the Menu Icon
   Then the app menu should open and "Scan and Check-In" should be highlighted "eyes"

@3.4
Scenario: 3.4 Add contact manually button '+' from Scan a Card
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   