# encoding: UTF-8

@cs_feature_search_contacts @qa_ready @9
Feature: Search Contacts

Background:
  Given I am logged out on the app

@9.1
Scenario: 9.1 Search for an existing contact by last name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" "Messi"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for the contact should match "Messi" "eyes"

@9.2
Scenario: 9.2 Search for an existing contact by first name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "First Name" "cri"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for the contact should match "Ronaldo" "eyes"

@9.3
Scenario: 9.3 Search for an existing contact by company name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company Name" "barc"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for the contact should match "Messi" "eyes"

@9.4
Scenario: 9.4 Search for an existing contact by last name with special characters and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" ")(*&^%$$##@!><?:{"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for the contact should match ")(*&^%$$##@!><?:{" "eyes"

@9.5
Scenario: 9.5 Search for a non-existant contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" "Pirlo"
   Then the contact with "Last Name" "Pirlo" should not be returned by the search

@9.6
Scenario: 9.6 Edit a contact and immediately search for the contact
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan and Check-In page should open "noeyes"
   When the user navigates to Contacts "eyes"
   Then the user should be on the Contacts page "eyes"
   When the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company Name" "Real Madrid"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for the contact should match "Ronaldo" "eyes"
   When the user scrolls to the "Website" field
   And the user clicks "Edit"
   Then the user should be on the Edit Contact page for "Ronaldo"
   When the user scrolls to the "Website" field
   And the user changes the "Company *" from "Real Madrid" to "Manchester United"
   And the user clicks "Done"
   Then the Contact info page should appear
   When the user navigates back to the Scan and Check-In page
   And the user navigates to Contacts "eyes"
   And the user selects the "ALL" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company Name" "Manchester United"
   And the user clicks the first contact
   Then the Contact info page should appear
   And the contact information for "Ronaldo" will have "Company Name" "Manchester United"

   