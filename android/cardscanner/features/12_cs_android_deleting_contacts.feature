# encoding: UTF-8

@cs_feature_deleting_a_contact @qa_ready @12 @coretests
Feature: Deleting a Contact

Background:
  Given I am logged out on the app

@12.1
Scenario: 12.1 Add Top Gear contacts
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  Then the Contacts list should be empty "eyes"
  When the user clicks the Home button
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "Top Gear" contacts
  And the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"
  And the "ALL" tab will indicate the correct number of contacts

@12.2
Scenario: 12.2 Add Other contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "Other" contacts
  And the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Other" contacts "eyes"
  And the "ALL" tab will indicate the correct number of contacts

@12.3
Scenario: 12.3 Delete a contact
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user deletes the first "1" contacts
  Then the first "1" contacts will not be on the Contacts List
  And the "ALL" tab will indicate the correct number of contacts
  When the user logs out of the Card Scanner app
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the first "1" contacts will not be on the Contacts List
  And the "ALL" tab will indicate the correct number of contacts

@12.4
Scenario: 12.4 Delete 2 contacts and then log out and log back in
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user deletes the first "2" contacts
  Then the first "2" contacts will not be on the Contacts List
  And the "ALL" tab will indicate the correct number of contacts
  When the user logs out of the Card Scanner app
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the first "2" contacts will not be on the Contacts List
  And the "ALL" tab will indicate the correct number of contacts

@12.5
Scenario: 12.5 Search for a contact and then delete the contact
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  Then the contact with "Last Name" "Hammond" should be returned by the search
  When the user deletes the first "1" contacts
  Then the first "1" contacts will not be on the Contacts List
  When the user logs out of the Card Scanner app
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the first "1" contacts will not be on the Contacts List
  And the "ALL" tab will indicate the correct number of contacts


@12.6
Scenario: 12.6 Filter a contact and then delete the contact
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "eyes"
  When the user selects the "ALL" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks the Filter button
  And the user filters by the "Company" named "Top Gear"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "Clarkson"
  When the user deletes the first "1" contacts
  Then the first "1" contacts will not be on the Contacts List
  When the user logs out of the Card Scanner app
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the first "1" contacts will not be on the Contacts List
  #And the "ALL" tab will indicate the correct number of contacts

@12.7
Scenario: 12.7 Add a new contact and then delete the contact
	Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user clicks the '+' button
  Then the Add Contact page should open "eyes"
  When the user manually enters new contact "Hawking"
  And the user clicks "Done"
  And the user closes the GPS error dialog if necessary
  Then the user should be on the Contacts page "eyes"
  And the new contact "Hawking" should be in the Contacts list "eyes"
  When the user deletes the first "1" contacts
  Then the first "1" contacts will not be on the Contacts List
  When the user logs out of the Card Scanner app
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the first "1" contacts will not be on the Contacts List
  #And the "ALL" tab will indicate the correct number of contacts


