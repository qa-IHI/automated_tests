# encoding: UTF-8

@cs_feature_contact_tabs @qa_ready @17 @coretests
Feature: Contact Tabs

Background:
  Given I am logged out on the app
  
@17.1
Scenario: 17.1 Delete All contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  Then the Contacts list should be empty "eyes"

@17.2
Scenario: 17.2 Contacts List event matches selected event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  And the event dropdown should have "Mobile Auto Long Topics" selected "noeyes"

@17.3
Scenario: 17.3 TODAY tab
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  Then the user should be on the "ALL" tab "eyes"
  When the user selects the "TODAY" tab on the Contacts page "noeyes"
  Then the user should be on the "TODAY" tab "eyes"

@17.4
Scenario: 17.4 No contacts in TODAY tab
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "TODAY" tab on the Contacts page "noeyes"
  Then the user should be on the "TODAY" tab "noeyes"
  And the no contacts image should be displayed on the "TODAY" tab "eyes"

@17.5
Scenario: 17.5 No contacts in ALL tab with All events selected
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  Then the user should be on the "ALL" tab "noeyes"
  When the user selects All Events
  Then the no contacts image should be displayed on the "ALL" tab "eyes"

@17.6
Scenario: 17.6 No contacts in ALL tab with a specific event selected
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "Mobile Automation Event 1" event on the Contact page
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  Then the user should be on the "ALL" tab "noeyes"
  And the no contacts image should be displayed on the "ALL" tab "eyes"
  	
@17.7
Scenario: 17.7 No contacts in PENDING tab
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "PENDING" tab on the Contacts page "noeyes"
  Then the user should be on the "PENDING" tab "noeyes"
  And the no contacts image should be displayed on the "PENDING" tab "eyes"

@17.8
Scenario: 17.8 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  Then the Contacts list should be empty "eyes"
  When the user navigates to Select an Event "noeyes"
  And the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "Top Gear" contacts
  And the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"
  And the "ALL" tab will indicate the correct number of contacts

@17.9
Scenario: 17.9 Add NFL contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user adds the "NFL" contacts
  And the user navigates to Contacts "eyes"
  And the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "NFL" contacts "eyes"
  And the "ALL" tab will indicate the correct number of contacts

# Currently does not work due to code change that affects the contact timestamp in simulator, so will not show in Today Tab.
@17.10 @wip
Scenario: 17.10 Switch between events with different number of contacts
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  And the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan and Check-In page should open "noeyes"
  When the user navigates to Contacts "eyes"
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "ALL" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the "ALL" tab should contain "5" contacts
  And the "TODAY" tab should contain "5" contacts
  When the user selects the "Mobile Automation Event 1" event on the Contact page
  Then the "ALL" tab should contain "2" contacts
  And the "TODAY" tab should contain "2" contacts

# Is event suppose to be a filter item? It is not currently in Android CS.
@wip @17.11
Scenario: 17.11 Filter Contacts shows Event option in All Events
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is logged into the app and the tutorial is displayed "noeyes"
  When the user clears the tutorial overlay
  Then the user is on the landing page "eyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  And the user selects All Events
  #And the event dropdown should have "  All events and occasions" selected by default "eyes"
  When the user clicks "Filter"
  Then the "Event" filter option will be displayed

# Is event suppose to be a filter item? It is not currently in Android CS.
@17.12 @wip
Scenario: 17.12 Filter Contacts does not show Event option in a specific event
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is logged into the app and the tutorial is displayed "noeyes"
  When the user clears the tutorial overlay
  Then the user is on the landing page "eyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 1" event on the Contact page
  When the user clicks "Filter"
  Then the "Event" filter option will not be displayed
  