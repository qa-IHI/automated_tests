# encoding: UTF-8

@checkin_feature_sort_attendees @wip @10
Feature: Sort Attendees

Background:
  Given I am logged out on the app
  

@10.1
Scenario: 10.1 Add F1 contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "F1" contacts
 	Then the attendees list should include "F1" contacts


# These test cases cannot be automated until Josh adds state values to the column labels to indicate ascending, descending sort orders etc.
@10.2 @wip
Scenario: 10.2 Sort attendees by last name in ascending alphabetic order and select an attendee

@10.3 @wip
Scenario: 10.3 Sort attendees by last name in descending alphabetic order

@10.4 @wip
Scenario: 10.4 Sort attendees by Check-in status in ascending alphabetic order

@10.5 @wip
Scenario: 10.5 Sort attendees by Check-in status in descending alphabetic order

@10.6 @wip
Scenario: 10.6 Sort attendees by first name in ascending alphabetic order

@10.7 @wip
Scenario: 10.7 Sort attendees by first name in descending alphabetic order

@10.8 @wip
Scenario: 10.8 Sort attendees by company name in ascending alphabetic order

@10.9 @wip
Scenario: 10.9 Sort attendees by company name in descending alphabetic order

@10.10 @wip
Scenario: 10.10 Sort attendees by job title in ascending alphabetic order

@10.11 @wip
Scenario: 10.11 Sort attendees by job title in descending alphabetic order

@10.12 @wip
Scenario: 10.12 Sort attendees by Lead Score in ascending numeric order

@10.13 @wip
Scenario: 10.13 Sort attendees by Lead Score in descending numeric order

@10.14 @wip
Scenario: 10.14 Edit an attendee, sort by last name, and find the attendee
