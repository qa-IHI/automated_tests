# encoding: UTF-8

@checkin_feature_adding_attendees @qa_ready @7
Feature: Adding Attendees

Background:
  Given I am logged out on the app
  

@7.1
Scenario: 7.1 Press X button
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 1"


@7.2
Scenario: 7.2 User tries to add attendee with no last name
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the email
	And the user enters the company name
	And the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Last Name can't be empty" message


@7.3
Scenario: 7.3 User tries to add attendee with no company
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the email
	And the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Company can't be empty" message


@7.4
Scenario: 7.4 User tries to add attendee with no email
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the company name
	And the user clicks "Done"
	Then the user will receive a "Email can't be empty" message


@7.5
Scenario: 7.5 User tries to add attendee with incorrectly formatted email
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters an incorrectly formatted email address
	And the user enters the company name
	And the user clicks "Done"
	Then the user will receive a "Invalid email address" message


@7.6
Scenario: 7.6 User has entered all required fields and clicks Edit Topics
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the email
	And the user enters the company name
	And the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page 


@7.7
Scenario: 7.7 User has entered all required fields and clicks Edit Follow-Up Actions
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the email
	And the user enters the company name
	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page


@7.8
Scenario: 7.8 User has entered all required fields and clicks Cancel
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the email
	And the user enters the company name
	And the user clicks "Cancel"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"


@7.9
Scenario: 7.9 User has entered all required fields and clicks Done
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open
	When the user enters the first name
	And the user enters the last name
	And the user enters the email
	And the user enters the company name
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"

# Currently fails bc we can't tell when Topic/FUAs are selected or deselected.
@wip @7.10
Scenario: 7.10 User correctly enters all required fields for new contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the ")(*&^%$$##@!><?:{" contacts
 	Then the attendees list should include ")(*&^%$$##@!><?:{" contacts
	When the user clicks "open" the Select an Event collapsible arrow
 	And the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
 	When the user adds the "¿¡ÀÈÌÒÙÇÃÑÕ" contacts
 	Then the attendees list should include "¿¡ÀÈÌÒÙÇÃÑÕ" contacts
	When the user clicks "open" the Select an Event collapsible arrow
	And the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "Ronaldo" contacts
 	Then the attendees list should include "Ronaldo" contacts
	When the user clicks "open" the Select an Event collapsible arrow
 	And the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
 	When the user adds the "Messi" contacts
 	Then the attendees list should include "Messi" contacts

 	