# encoding: UTF-8

@checkin_feature_report_a_problem @qa_ready @5
Feature: Report a Problem

Background:
  Given I am logged out on the app


@5.1
Scenario: 5.1 User selects Report a Problem
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	And the user chooses Report a Problem
 	Then the To field should be "support@at-event.com"
 	And the default text should be "Describe your problem here:"