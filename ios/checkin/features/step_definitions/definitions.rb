# encoding: UTF-8
Given(/^I am logged out on the app$/) do
  login_button = @driver.find_element(:accessibility_id, "Log In").displayed?

  if !login_button
    logout
  end
end

Given(/^the user is on the Login screen of the Check\-In app$/) do
  #accept_notifications
  #all_elements_visible?(["login-header-ci", "Forgot Password?", "Keep me logged in", "Log In"])
  all_elements_visible?(["Forgot password?", "Keep me logged in", "Log In"])
end

Given(/^the user successfully logs in as "(.*?)"$/) do |user|
  case user
  when "Uzair"
    username = $test["uzair_username"]
    password = $test["uzair_password"]
  when "QA"
    username = $test["qa_username"]
    password = $test["qa_password"]
  when "AutoQA"
    username = $test["auto_username"]
    password = $test["auto_password"]
  end

  enter_valid_email(username)
  #click_button("Hide keyboard")
  hide_keyboard
  enter_valid_password(password)
  #click_button("Hide keyboard")
  hide_keyboard
  click_button("Log In")
  checkin_landing_page_check
end


When(/^the user logs out of the Check-In app$/) do
  logout
end


# Click a button
When(/^the user clicks "([^"]*)"$/) do |name|
  click_button(name)
end

Then(/^the user is on the Request a Trial page$/) do
  request_trial_page_check
end

Then(/^the "(.*?)" checkbox is unchecked$/) do |boxname|
  agree_terms_of_service_unchecked?
end

When(/^the user enters a first name$/) do
  enter_requesttrial_field("First Name", "QA")
end

When(/^the user enters a last name$/) do
	enter_requesttrial_field("Last Name", "Tester")
end

When(/^the user enters a company name$/) do
  enter_requesttrial_field("Company", "Mobile Automation")
end

When(/^the user enters a phone number$/) do
  enter_requesttrial_field("Phone", "12345678900")
end

When(/^the user enters a phone number contain letters and symbols$/) do
	enter_requesttrial_field("Phone", "asdfgh!@#|*#")
end

When(/^the user enters an email$/) do
  enter_requesttrial_field("Email", "qa@tester.com")
end

When(/^the user enters invalid email$/) do
  enter_requesttrial_field("Email", "!!!!!!!$$}}}}}}}")
end

When(/^user accepts the Terms of Service$/) do
  accept_terms_of_service(true)
end

# Find the error message
Then(/^the user will receive an* "([^"]*)" message$/) do |message|
  wait_for_error(message)
end

Then(/^the user is back on the Request a Trial page$/) do
  error_request_trial_page_check
end

When(/^the user clicks the X button$/) do
  click_button("ci btnCloseX")
end

# Log into the app with a valid username/password
When(/^the user enters valid email and password$/) do
  enter_valid_email($test["qa_username"])
  enter_valid_password($test["qa_password"])
  #click_button("Done")
  hide_keyboard
end

# After logging in, dismiss the tutorial overlay
#Then(/^the user is logged into the app$/) do
#  clear_tutorial_overlay
#end

# Once we have logged in, we should not see the "Keep Me Logged in" checkbox
# We should see the other elements on the landing screen
Then(/^the user is on the landing page$/) do
  checkin_landing_page_check
end

# Check that we are on the Password Reset screen
Then(/^the Password Reset request window should appear$/) do
  password_reset_page_check
end

# Make sure Keep Me Logged in is enabled by default
When(/^Keep Me Logged in is enabled by default$/) do
  keep_me_logged_in_checked?
end

# Background the app for a time
When(/^the user backgrounds the Check\-In app and returns$/) do
  #@driver.background_app(DEFAULT_BACKGROUND_TIME)
  @driver.background_app(3)
end

# Enter nothing for the login email address.
When(/^the user doesn't enter an email address$/) do
  enter_empty_email
end

# Enter an invalid email address format
When(/^the user enters an invalid email$/) do
  enter_invalid_email
end

# Enter a valid email address.
When(/^the user enters a valid email$/) do
  enter_valid_email($test["qa_username"])
end

# Don't enter a password
When(/^does not enter a password$/) do
  enter_empty_password
end

When(/^the user enters an invalid email format$/) do
  enter_invalid_email_format
end

When(/^the user enters an invalid password$/) do
  enter_invalid_password
end

When(/^enters a valid email account for reset$/) do
  enter_valid_email($test["qa_reset_username"])
end

When(/^the user closes the iOS keyboard$/) do
	#click_button("Hide keyboard")
  hide_keyboard
end

When(/^the user goes to the About page$/) do
  navigate_to_about
end

When(/^the user goes to the Contact page$/) do
	navigate_to_contact
end

When(/^the user clicks the Home button$/) do
  click_home_button
end

Then(/^the correct app version is displayed$/) do
  verify_app_version
end

Then(/^the app menu should open and "([^"]*)" should be highlighted$/) do |option|
	verify_app_menu_highlight(option)
end

When(/^the user clicks the Menu Icon$/) do
  click_menu_button
end

When(/^the user chooses Report a Problem$/) do
	navigate_to_reportaproblem
end

Then(/^the To field should be "([^"]*)"$/) do |text|
	verify_to_field(text)
end

Then(/^the default text should be "(.*?)"$/) do |text|
	verify_problem_field(text)
end

When(/^the user chooses event "(.*?)" on the landing page$/) do |event|
 	choose_event(event)
end

Then(/^the user will be on the attendee page for event "(.*?)"$/) do |event|
	verify_attendee_page(event)
end

Then(/^the "(.*?)" event shuld be selected$/) do |event|
	verify_selected_event(event)
end

When(/^the user searches for the "(.*?)" event$/) do |event| 
  search_event(event)
end

Then(/^only the "(.*?)" event will be displayed$/) do |event|
  only_event_displayed(event)
end

When(/^the user clicks "(.*?)" the Select an Event collapsible arrow$/) do |option|
	if option == "open"
		click_button("toggleDown arrow")
	elsif option == "close"
		click_button("toggleUp arrow")
	end
end

Then(/^the Select an Event menu should be displayed$/) do
	select_an_event_menu_check
end

When(/^the user clicks the '\+' button$/) do
  click_button("ci plusManual")
end

Then(/^the Add an Attendee page should open$/) do
	add_attendee_page_check
end

When(/^the user adds the "(.*?)" contacts$/) do |group|
  if group == "Top Gear"
    add_contacts ["Clarkson", "Hammond", "May"]
  elsif group == "F1"
    add_contacts ["Hamilton", "Vettel", "Rosberg"]
  elsif group == "Starfleet"
    add_contacts ["Kirk", "McCoy"]
  elsif group == "NFL"
    add_contacts ["Brady", "Manning"]
  elsif group == "Other"
    add_contacts ["Brown", "Baggins", "Abrams"]
  else
  	add_contacts [group]
  end
  sleep 10
end

When(/^the user adds new contact "(.*?)"$/) do |person|
	add_contacts [person]
end

Then(/^the attendees list should include "(.*?)" contacts$/) do |group|
	case group
	when "Top Gear"
		verify_contact_list ["Clarkson", "Hammond", "May"]
	when "F1"
		verify_contact_list ["Hamilton", "Vettel", "Rosberg"]
  when "NFL"
    verify_contact_list ["Brady", "Manning"]
  when "Starfleet"
    verify_contact_list ["Kirk", "McCoy"]
	else
		verify_contact_list [group]
	end
end

When(/^the user selects the "(.*?)" contact$/) do |position|
	magnify_contact(position)
end
	

Then(/^the Attendee Information page will open$/) do
  verify_attendee_info_page
end


Then(/^the Attendee Information page for the contact will appear$/) do
	verify_contact_info
end

Then(/^the Attendee Information page for "(.*?)" will appear$/) do |lname|
	verify_named_contact_info(lname)
end

Then(/^the Edit Attendee Information page for "(.*?)" will appear$/) do |lname|
	edit_attendee_page_check(lname)
end

When(/^the user searches for the contact with "(.*?)" "(.*?)"$/) do |key, value|
 	search_for_contact(value)
end

Then(/^the contact with "(.*?)" "(.*?)" should be returned by the search$/) do |key, value|
  verify_search_contact_returned(value)
end

Then(/^the contact with "(.*?)" "(.*?)" should not be returned by the search$/) do |key, value|
  verify_no_contacts
end

When(/^the user changes the "(.*?)" from "(.*?)" to "(.*?)"$/) do |key, old_value, new_value|
  edit_field(old_value, new_value)
end

When(/^the user clears the search box$/) do
	clear_search_box
end


When(/^the user kills and relaunches the app$/) do
  @driver.close_app
  @driver.launch_app
end


Then(/^the contact information for "(.*?)" will have "(.*?)" "(.*?)"$/) do |lname, key, value|
 	contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

	attendee_info_box = @driver.find_element(:class, "UIAScrollView")
    attendee_info_box.find_element(:name, fullname).displayed?

    searchstring = $test["#{contact}_job_title"] + " " + value 
    attendee_info_box.find_element(:name, searchstring).displayed?
end

Then(/^the user should be on the Filter Contacts page$/) do 
	filter_contacts_page_check
end

When(/^the user selects the "(.*?)" filter$/) do |filter|
	select_filter_type(filter)
end

When(/^the user selects "(.*?)" on the "(.*?)" filter$/) do |option, filter_type|
	click_filter_box(option)
end

Then(/^the "(.*?)" filter will have a green check mark$/) do |filter_type|
	verify_filter_selected(filter_type)
end

Then(/^the Filter button will show "(.*?)" filter applied$/) do |filter_count|
	verify_filter_count(filter_count)
end

When(/^the user enters the first name$/) do
 	edit_field("First Name *", $test["contact2_fname"])
end

When(/^the user enters the company name$/) do
	edit_field("Company *", $test["contact2_company"])
end

When(/^the user enters the last name$/) do
    edit_field("Last Name *", $test["contact2_lname"])
end

When(/^the user enters the email$/) do
	edit_field("Email *", $test["contact2_email"])
end

When(/^the user enters an incorrectly formatted email address$/) do
	#enter_new_contact_invalid_email
	edit_field("Email *", "$$$$$$$$$$$$$")
end

Then(/^the user should be on the Edit Topics page$/) do
	edit_topics_page_check
end


Then(/^the user should be on the Follow\-Up Actions page$/) do
	edit_fua_page_check
end

When(/^the user waits "(.*?)" seconds$/) do |secs|
	sleep secs.to_i
end

Then(/^the Attendee Information page should contain a business card image$/) do
	verify_attendee_with_card_image
end
	
When(/^the user clicks the business card magnify button$/) do
	magnify_business_card
end

Then(/^the enlarged business card should appear$/) do
	verify_magnified_card
end

When(/^the user closes the magnified business card$/) do
	close_magnified_card
end

Then(/^the "(.*?)" field should contain "(.*?)"$/) do |key, value|
  verify_fieldvalue(key, value)
end

When(/^the user selects the "(.*?)" topics$/) do |list|
  select_topics(list)
end

When(/^the user deselects the "(.*?)" topics$/) do |list|
  deselect_topics(list)
end

Then(/^the "(.*?)" topics should be "(.*?)"$/) do |list, state|
  verify_topics_state(list, state)
end

When(/^the user changes the text from "(.*?)" to "(.*?)"$/) do |old_text, new_text|
  replace_text(old_text, new_text)
end

Then(/^the text should match "(.*?)"$/) do |text|
  match_text(text)
end

Then(/^the "(.*?)" field should be empty$/) do |key|
  verify_empty_fieldvalue(key)
end

Then(/^the sub\-topics should open with "(.*?)" sub\-topics$/) do |list|
  subtopics_page_check(list)
end

When(/^the user selects the "(.*?)" sub\-topics$/) do |list|
  select_subtopics(list)
end

Then(/^the "(.*?)" sub\-topics should be "(.*?)"$/) do |list, state|
  verify_subtopics_state(list, state)
end

When(/^the user deselects the "(.*?)" sub\-topics$/) do |list|
  deselect_subtopics(list)
end

Then(/^the Topics page should display "(.*?)" topics in alpha order$/) do |topics_list|
  verify_topics_list_alpha_order(topics_list)
end

Then(/^the sub\-topics page should display "(.*?)" sub\-topics in alpha order$/) do |subtopics_list|
  verify_subtopics_list_alpha_order(subtopics_list)
end

When(/^the user scrolls to the end of the topics list$/) do
  #@driver.swipe(:start_x => 845, :start_y => 477, :end_x => 97, :end_y => 477, :touchCount => 1, :duration => 1000)
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 950, :y => 282).move_to(:x => -885, :y => 0).release.perform
end

When(/^the user scrolls to the end of the sub\-topics list$/) do
  #@driver.swipe(:start_x => 720, :start_y => 540, :end_x => 720, :end_y => 238, :touchCount => 1, :duration => 1000)
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 581, :y => 499).move_to(:x => 0, :y => -285).release.perform
end

Then(/^the "(.*?)" sub\-topics pop up should appear$/) do |title|
  @driver.wait { @driver.find_element(:name, "#{title} Topics").displayed? }
  @driver.wait { @driver.find_element(:name, "Done").displayed? }
end

When(/^the user selects the "(.*?)" follow\-up actions$/) do |list|
  select_fuas(list)
end

When(/^the user deselects the "(.*?)" follow\-up actions$/) do |list|
  deselect_fuas(list)
end

Then(/^the "(.*?)" follow\-up actions should be "(.*?)"$/) do |list, state|
  verify_fuas_state(list, state)
end

Then(/^the sub follow-up actions should open with "(.*?)"$/) do |list|
  subfuas_page_check(list)
end

When(/^the user selects the "(.*?)" sub follow\-up actions$/) do |list|
  select_subfuas(list)
end

When(/^the user deselects the "(.*?)" sub follow\-up actions$/) do |list|
  deselect_subfuas(list)
end

Then(/^the "(.*?)" sub follow\-up actions should be "(.*?)"$/) do |list, state|
  verify_subfuas_state(list, state)
end

Then(/^the Note pop\-up should appear with title "(.*?)"$/) do |title|
  verify_note_popup(title)
end

When(/^the user changes the text from "(.*?)" to more than 500 characters$/) do |old_text|
  new_text = ""
  base = "abcdefghijk"

  for i in 1..50 do
    new_text = new_text + base
  end

  replace_text(old_text, new_text)
end

Then(/^the Free Text pop-up should appear with title "(.*?)"$/) do |title|
  verify_note_popup(title)
end

Then(/^the Follow Up Actions page should display "(.*?)" follow up actions in alpha order$/) do |fua_list|
  verify_fua_list_alpha_order(fua_list)
end

Then(/^the sub\-follow up actions page should display "(.*?)" sub\-follow up actions in alpha order$/) do |subfua_list|
  verify_subfua_list_alpha_order(subfua_list)
end

When(/^the user scrolls to the end of the follow up actions list$/) do
  #@driver.swipe(:start_x => 845, :start_y => 477, :end_x => 97, :end_y => 477, :touchCount => 1, :duration => 1000)
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 950, :y => 282).move_to(:x => -885, :y => 0).release.perform
end

Then(/^the "(.*?)" sub\-follow up actions pop up should appear$/) do |title|
  @driver.wait { @driver.find_element(:name, "#{title} Follow-up Actions").displayed? }
  @driver.wait { @driver.find_element(:name, "Done").displayed? }
end

When(/^the user scrolls to the end of the sub\-follow up actions list$/) do
  #@driver.swipe(:start_x => 720, :start_y => 540, :end_x => 720, :end_y => 238, :touchCount => 1, :duration => 1000)
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 581, :y => 499).move_to(:x => 0, :y => -285).release.perform
end

Then(/^all follow\-up actions should be "(.*?)"$/) do |state|
  verify_all_fuas_state(state)
end

Then(/^all Topics should be "(.*?)"$/) do |state|
  verify_all_topics_state(state)
end

When(/^the user clicks the "(.*?)" follow-up actions$/) do |list|
  click_fuas(list)
end


# Uncheck the Keep Me Logged in checkbox
When(/^the user unchecks Keep Me Logged in$/) do
  keep_me_logged_in(false)
end


# Check the Keep Me Logged in checkbox
When(/^the user checks Keep Me Logged in$/) do
  keep_me_logged_in(true)
end


When(/^the user deletes the first "(.*?)" contacts$/) do |number|
  delete_contacts(number)
end


Then(/^the first "(.*?)" contacts will not be on the Contacts List$/) do |number|
  verify_contacts_deleted(number)
end


When(/^the user filters by the "(.*?)" named "(.*?)"$/) do |filter, value|
  filter_contacts(filter, value)
end


When(/^the user applies the filter$/) do
  apply_filter
end


When(/^the user selects the first "(.*?)" in contact$/) do |status|
  select_first_contact(status)
end


When(/^the Topics field should contain "(.*?)"$/) do |value|
  verify_topic_field_value(value)
end


When(/^the Follow\-Up Actions field should contain "(.*?)"$/) do |value|
  verify_fua_field_value(value)
end


When(/^the user clicks "(.*?)" FUA "(.*?)"$/) do |state, fua|
  if state == "deselected"
    text = "Topic FUA Not Selected " + fua
  elsif state == "selected"
    text = "Topic FUA Selected " + fua
  end
  @driver.find_element(:accessibility_id, text).click
end


When(/^the user scrolls to the "(.*?)" field$/) do |field|
  scroll_to(field)
  
  #new_contact_scroll_to_bottom
  #@driver.scroll_to(field)
end

