# encoding: UTF-8

require 'appium_lib'
require 'rubygems'
require 'cucumber/ast'
require 'yaml'
require 'sauce_whisk'
require 'json'
require 'unicode_utils'
require 'hiptest-publisher'

# Read in test parameters from the env.cfg file.
CONFIG_FILE = Dir.pwd + '/features/support/env.cfg'
DEFAULT_BACKGROUND_TIME = 7

if File.file?(CONFIG_FILE) == false
	puts "ERROR: The config file #{CONFIG_FILE} does not exist. Exiting."
	exit 1
else
	config = begin
  		YAML.load(File.open(CONFIG_FILE))
	rescue ArgumentError => e
  		puts "Could not parse YAML: #{e.message}"
	end

	$caps = config["caps"]
	$test = config["test"]
end

# Determine if we're running against a local or Sauce Labs Appium server.
# Run a Sauce Labs servers.
if ENV['SERVER'] == "sauce"
	# Get the filename of the zipped app package from the Jenkins environment.
	if ENV['APPFILE'] == nil
		puts "ERROR: The zipped app package is undefined. Exiting."
		exit 1
	else
		$app_file = ENV['APPFILE']
		puts "app_file = #{$app_file}"

		# Extract the app version from the filename.
		$app_version = $app_file.match(/^AtEvent-(.*).zip$/).captures[0]
		puts "app_version = #{$app_version}"
	end

	# Get the Sauce user and Sauce key from the Jenkins environment.
	if ENV['SAUCE_USERNAME'] == nil
		puts "ERROR: The Sauce user is undefined. Exiting."
		exit 1
	else
		$sauce_user = ENV['SAUCE_USERNAME']
		puts "sauce_user = #{$sauce_user}"
	end

	if ENV['SAUCE_ACCESS_KEY'] == nil
		puts "ERROR: The Sauce key is undefined. Exiting."
		exit 1
	else
		$sauce_key = ENV['SAUCE_ACCESS_KEY']
		puts "sauce_key = #{$sauce_key}"
	end

	if ENV['SAUCEJSONURL'] == nil
		puts "ERROR: The Sauce Labs JSON endpoint is undefined. Exiting."
		exit 1
	else
		$sauce_json_url = ENV['SAUCEJSONURL']
		puts "sauce_json_url = #{$sauce_json_url}"
	end

	if ENV['SAUCEMINTIME'] == nil
		puts "ERROR: The Sauce Labs minimum available time is undefined. Exiting."
		exit 1
	else
		$sauce_min_time = ENV['SAUCEMINTIME']
		puts "sauce_min_time = #{$sauce_min_time}"
	end

	if ENV['IOSVERSION'] == nil
		puts "ERROR: The iOS Version is undefined. Exiting."
		exit 1
	else
		$ios_version = ENV['IOSVERSION'].to_s
		puts "ios_version = #{$ios_version}"
	end

	if ENV['IOSDEVICE'] == nil
		puts "ERROR: The iOS device is undefined. Exiting."
		exit 1
	else
		$ios_device = ENV['IOSDEVICE']
		puts "ios_device = #{$ios_device}"
	end

	if ENV['APPIUMVERSION'] == nil
		puts "ERROR: The Appium version is undefined. Exiting."
		exit 1
	else
		$appium_version = ENV['APPIUMVERSION'].to_s
		puts "appium_version = #{$appium_version}"
	end

	# Continue only if we have at least 180 mins of test time remaining on Sauce Labs.
	#json = `curl -s https://saucelabs.com/rest/v1/users/shawnakumar -u shawnakumar:b90ca1c2-7ca1-4159-8b43-f9a8aac71ed9`
	json = `curl -s #{$sauce_json_url}/#{$sauce_user} -u #{$sauce_user}:#{$sauce_key}`
	result = JSON.parse(json)
	
	if result["minutes"].to_i <= $sauce_min_time.to_i
		puts "ERROR: There are only #{result["minutes"]} minutes left on Sauce Labs. Automated tests will not start."
		exit 1
	else
		puts "Excellent!! There are #{result["minutes"]} minutes left of testing time on Sauce Labs. Proceeding..."
	end

	$appium_url = "http://#{$sauce_user}:#{$sauce_key}@ondemand.saucelabs.com:80/wd/hub"
	$app_location = "sauce-storage:#{$app_file}"
# Run on a local server.
else
	$appium_url = "http://127.0.0.1:4723/wd/hub"
	$app_location = ENV['APP_PATH'] || $test["app_path"]
	$app_version = $test["app_version"]
	$ios_device = $caps["deviceName"]
	$ios_version = $caps["platformVersion"].to_s
end
