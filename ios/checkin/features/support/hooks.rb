#Touch Action example
# TouchAction example
# Chain together gestures and then perform.
# http://stackshare.io/sauce-labs/mobile-automation-with-appium-and-sauce-labs
#
#source = @driver.wait { @driver.find_element(:accessibility_id, "name") }
#action = Appium::TouchAction.new
#action.long_press(element: source)
#action.wait(500)
#action.release
#action.perform



# Setup section that executes before each test scenario.
# Set the Appium test capabilities.
# Start the Appium driver.
Before do |scenario|
    @scenario_name = "iPad Checkin: " + scenario.name

    def desired_capabilities
    {  caps:
        {
            #appiumVersion:      $caps["appiumVersion"],
            appiumVersion:      $appium_version,
            platformName:       $caps["platformName"],
            #platformVersion:    $caps["platformVersion"].to_s,
            platformVersion:    $ios_version,
            #deviceName:         $caps["deviceName"],
            deviceName:         $ios_device,
            #udid: ,
            orientation:        $caps["orientation"],
            sendKeyStrategy:    $caps["sendKeyStrategy"],
            app:                $app_location,
            name:               @scenario_name,
            simpleIsVisibleCheck: false,
            automationName:      "XCUITest",
            #language:           "es_MX",
            #locale:             "es_MX",
            noReset:             true,
            fullReset:           false,
            useNewWDA:           true,
        },
        appium_lib:
        {
            server_url:         $appium_url
            #server_url:         'http://krishuynh:ee5bb81b-24bb-4308-a22e-85db1ab9433c@ondemand.saucelabs.com:80/wd/hub'
        }
    }
    end

    @driver = Appium::Driver.new(desired_capabilities)
	@driver.start_driver
    $job_id = @driver.session_id
    #sleep 7

    # Create a new hash map for the names of fields in the app. We want to keep track of the field names in case they change during a session.
    $fields_hash = Hash.new
    $fields_hash["login_email"]     = "Email Address"
    $fields_hash["login_password"]  = "Password"
    $fields_hash["model"]           = $caps["deviceName"]

    case $fields_hash["model"]
    when "iPhone 6"
        model = "iphone6"
    when "iPhone 6 Plus"
        model = "iphone6plus"
    when "iPad Air"
        model = "ipadair"
    else
        model = "unknown"
        #puts "Unable to test on #{$caps["deviceName"]} right now. Exiting."
        #exit 1
    end
        
    $fields_hash["bcard_small_x"]       = $test["#{model}_bcard_small_x"].to_f
    $fields_hash["bcard_small_y"]       = $test["#{model}_bcard_small_y"].to_f
    $fields_hash["bcard_small_height"]  = $test["#{model}_bcard_small_height"].to_f
    $fields_hash["bcard_small_width"]   = $test["#{model}_bcard_small_width"].to_f

    $fields_hash["bcard_large_x"]       = $test["#{model}_bcard_large_x"].to_f
    $fields_hash["bcard_large_y"]       = $test["#{model}_bcard_large_y"].to_f
    $fields_hash["bcard_large_height"]  = $test["#{model}_bcard_large_height"].to_f
    $fields_hash["bcard_large_width"]   = $test["#{model}_bcard_large_width"].to_f
    
    $fields_hash["first"]  = 0
    $fields_hash["second"] = 1
    $fields_hash["third"]  = 2
    $fields_hash["fourth"] = 3
    $fields_hash["fifth"]  = 4

    @deleted_contacts = Array.new(10)
end

# Teardown section that executes after each test scenario.
# Report pass or fail to Sauce Labs.
# Stop the Appium driver.
# Clear the hash.
After do |scenario|
    if ENV['SERVER'] == "sauce"
        if ENV['SAUCE_USERNAME'] && !ENV['SAUCE_USERNAME'].empty? && ENV['SAUCE_ACCESS_KEY'] && !ENV['SAUCE_ACCESS_KEY'].empty?
            passed = !(scenario.failed?)
            SauceWhisk::Jobs.change_status($job_id, passed)
        end
    end

	@driver.driver_quit
    $fields_hash.clear
end


def all_elements_visible?(list)
    for element in list do
        @driver.find_element(:id, element).displayed?
    end
end


def click_button(name)
    @driver.wait { @driver.find_element(:accessibility_id, name).click }
end


def error_request_trial_page_check
    #sleep 3
    @driver.wait { @driver.find_element(:id, "ci_featureBadge").displayed? }
    @driver.wait { @driver.find_element(:id, "Request a Trial").displayed? }
    @driver.wait { @driver.find_element(:id, "Submit").displayed? }
    @driver.wait { @driver.find_element(:id, "I agree to atEvent").displayed? }
    @driver.wait { @driver.find_element(:id, "Terms of Service").displayed? }
    @driver.wait { @driver.find_element(:id, "ci btnCloseX").displayed? }
end


def request_trial_page_check
    #sleep 3
    @driver.wait { @driver.find_element(:id, "ci_featureBadge").displayed? }
    @driver.wait { @driver.find_element(:id, "Request a Trial").displayed? }
    @driver.wait { @driver.find_element(:id, "First Name").displayed? }
    @driver.wait { @driver.find_element(:id, "Last Name").displayed? }
    @driver.wait { @driver.find_element(:id, "Company").displayed? }
    @driver.wait { @driver.find_element(:id, "Phone").displayed? }
    @driver.wait { @driver.find_element(:id, "Email").displayed? }
    @driver.wait { @driver.find_element(:id, "I agree to atEvent").displayed? }
    @driver.wait { @driver.find_element(:id, "Submit").displayed? }
    @driver.wait { @driver.find_element(:id, "Terms of Service").displayed? }
    @driver.wait { @driver.find_element(:id, "ci btnCloseX").displayed? }
end


def agree_terms_of_service_unchecked?
    @driver.wait { @driver.find_element(:id, "checkbox").displayed? }
end


def enter_requesttrial_field(field, value)
    field_element = @driver.wait { @driver.find_element(:id, field) }
    field_element.click
    field_element.type value
end


def wait_for_error(message)
    @driver.wait { @driver.find_element(:accessibility_id, message).displayed? }
end


def accept_terms_of_service(choice)
    if choice == true
        if @driver.wait { @driver.find_element(:id, "checkbox") }
            checkbox_element = @driver.find_element(:id, "checkbox")
            checkbox_element.click
        end
    elsif choice == false
        if @driver.wait { @driver.find_element(:id, "checkbox checked") }
            checkbox_element = @driver.find_element(:id, "checkbox checked")
            checkbox_element.click
        end
    end
end


def enter_valid_email(email)
    email_element = @driver.wait { @driver.find_element(:accessibility_id, "Email Address") }
    #email_element = @driver.wait { @driver.find_element(:accessibility_id, 'Email Address') }
    email_element.click
    email_element.clear
    email_element.type email
    $fields_hash["login_email"] = email
end


def enter_valid_password(password)
    pw_element = @driver.wait { @driver.find_element(:accessibility_id, "Password") }
    #pw_element = @driver.wait { @driver.find_element(:accessibility_id, 'Password') }
    pw_element.type password
end


def checkin_landing_page_check
    #sleep 4
    #if @driver.find_element(:id, "Keep me logged in").displayed?
    #    raise "Error: Shouldn't see Keep me logged in on the landing page."
    #end

    @driver.wait { @driver.find_element(:id, "bg_eventSelector").displayed? }
    @driver.wait { @driver.find_element(:id, "toggleUp arrow").displayed? }
    @driver.wait { @driver.find_element(:id, "Check-In").displayed? }
    @driver.wait { @driver.find_element(:id, "Select an event").displayed? }
    @driver.wait { @driver.find_element(:id, "ci menuLines").displayed? }
end


def password_reset_page_check
    #sleep 3
    #@driver.wait { @driver.find_element(:id, "login-header-ci").displayed? }
    @driver.wait { @driver.find_element(:id, "logoLoginCheckin").displayed? }
    @driver.wait { @driver.find_element(:id, "Email Address").displayed? }
    @driver.wait { @driver.find_element(:id, "Cancel").displayed? }
    @driver.wait { @driver.find_element(:id, "Send").displayed? }
    #@driver.wait { @driver.find_element(:id, "ci menuLines").displayed? }
end


def keep_me_logged_in_checked?
    @driver.wait { @driver.find_element(:id, "checkbox checked").displayed? }
end


def keep_me_logged_in(choice)
    begin
        @driver.find_element(:id, "checkbox checked").displayed?
        checkbox_state = "checked"
    rescue
        checkbox_state = "unchecked"
    end

    if choice == true && checkbox_state == "unchecked"
        @driver.find_element(:id, "checkbox").click
    elsif choice == false && checkbox_state == "checked"
        @driver.find_element(:id, "checkbox checked").click
    end
end


def enter_empty_email
    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    email_element.click
    email_element.clear
end


def enter_invalid_email
    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    email_element.click
    email_element.clear
    email_element.type "lio@messi.com"
    $fields_hash["login_email"] = "lio@messi.com"
end


#def enter_valid_email(email)
#    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    #email_element = @driver.wait { @driver.find_element(:accessibility_id, 'Email Address') }
#    email_element.type email
#    $fields_hash["login_email"] = email
#end


def enter_empty_password
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    pw_element.type ""
end


def enter_invalid_email_format
    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    email_element.type "$$$$$$$$$$$$$"
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    pw_element.type ""
    $fields_hash["login_email"] = "$$$$$$$$$$$$$"
end


def enter_invalid_password
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    pw_element.type "futbolllll"
    $fields_hash["login_password"] = "futbolllll"
end


def click_menu_button
    sleep 5
    #menu_element = @driver.wait { @driver.find_element(:name, "ci menuLines") }
    menu_element = @driver.find_element(:accessibility_id, "ci menuLines")
    menu_element.click
end


def click_home_button
    button_element = @driver.wait { @driver.find_element(:name, "btn home") }
    button_element.click
end


def navigate_to_contact
    click_menu_button
    click_menu_choice("Contact")
    #contact = Appium::TouchAction.new 
    #contact.tap(:x => 44, :y => 204, :fingers => 1, :tapCount => 1, :duration => 1.0).perform
    @driver.wait { @driver.find_element(:name, "btn home").displayed? }
    @driver.wait { @driver.find_element(:name, "ci menuLines").displayed? }
    @driver.wait { @driver.find_element(:name, "ci_featureBadge").displayed? }
    @driver.wait { @driver.find_element(:name, "atEvent").displayed? }
    @driver.wait { @driver.find_element(:name, "6111 Bollinger Canyon Road, Suite 555 San Ramon, CA 94583").displayed? }
    @driver.wait { @driver.find_element(:name, "Phone").displayed? }
    @driver.wait { @driver.find_element(:name, "925 394 4440").displayed? }
    @driver.wait { @driver.find_element(:name, "Email").displayed? }
    @driver.wait { @driver.find_element(:name, "info@at-event.com").displayed? }
    @driver.wait { @driver.find_element(:name, "contact btn").displayed? }
    @driver.wait { @driver.find_element(:name, "email btn").displayed? }
    @driver.wait { @driver.find_element(:name, "Get Directions").displayed? }
    @driver.wait { @driver.find_element(:name, "ci_mapAndPlacemark").displayed? }
    @driver.wait { @driver.find_element(:name, "Contact").displayed? }
end


def navigate_to_reportaproblem
    click_menu_button
    click_menu_choice("Report A Problem")
    #problem = Appium::TouchAction.new 
    #problem.tap(:x => 44, :y => 245, :fingers => 1, :tapCount => 1, :duration => 1.0).perform
    @driver.wait { @driver.find_element(:name, "btn home").displayed? }
    @driver.wait { @driver.find_element(:name, "ci menuLines").displayed? }
    @driver.wait { @driver.find_element(:name, "Report A Problem").displayed? }
    @driver.wait { @driver.find_element(:name, "To:").displayed? }
    @driver.wait { @driver.find_element(:name, "Send").displayed? }
    @driver.wait { @driver.find_element(:name, "Cancel").displayed? }
end


def click_menu_choice(choice)
    menu_table = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeTable")
    about_row = menu_table.find_element(:name, choice)
    about_row.click
end
   

def navigate_to_about
    click_menu_button
    click_menu_choice("About")

    @driver.wait { @driver.find_element(:name, "btn home").displayed? }
    @driver.wait { @driver.find_element(:name, "About").displayed? }
    @driver.wait { @driver.find_element(:name, "ci menuLines").displayed? }
    @driver.wait { @driver.find_element(:name, "ci_featureBadge").displayed? }

    @driver.wait { @driver.find_element(:id, "atEvent provides event specific solutions that have real-time lead retrieval and eNurturing functionality.").displayed? }
    #@driver.wait { @driver.find_element(:id, "The Card Scanner app is a part of atEvent’s suite of event marketing automation apps that enable corporate marketers to automate the process of capturing prospect interactions at every event touch-point, providing a true 360 degree view of prospect activity across multiple events. Giveaway Terms and conditions: http://admin.at-event.com/terms").displayed? }
    verify_app_version
end


def verify_app_version
    @driver.wait { @driver.find_element(:name, "Version #{$app_version}").displayed? }
end


def verify_app_menu_highlight(option)
    @driver.wait { @highlight = @driver.find_element(:name, option).selected? }
    if @highlight == false
        raise "Error: Expected #{option} to be highlighted."
    end
end


def verify_to_field(text)
    @driver.wait { @driver.find_element(:id, text).displayed? }
end

def verify_problem_field(text)
    @driver.wait { @driver.find_element(:id, text).displayed? }
end

#def click_menu_button
#    button_element = @driver.wait { @driver.find_element(:name, "ci menuLines") }
#    button_element.click
#end


def choose_event(event)
    choice = @driver.wait { @driver.find_element(:name, event) }
    choice.click
end


def verify_attendee_page(event)
    @driver.wait { @driver.find_element(:name, event).displayed? }
    @driver.wait { @driver.find_element(:name, "Check-In").displayed? }
    @driver.wait { @driver.find_element(:name, "ci plusManual").displayed? }
    @driver.wait { @driver.find_element(:name, "bg_eventSelector").displayed? }
    @driver.wait { @driver.find_element(:name, "toggleDown arrow").displayed? }
    @driver.wait { @driver.find_element(:name, "ci menuLines").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureCard").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureBarcode").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureQRcode").displayed? }
    #@driver.wait { @driver.find_element(:id, "Search by name or company").displayed? }
    @driver.wait { @driver.find_element(:name, "Filter").displayed? }
    @driver.wait { @driver.find_element(:name, "Check-in").displayed? }
    @driver.wait { @driver.find_element(:name, "lastName").displayed? }
    @driver.wait { @driver.find_element(:name, "First Name").displayed? }
    @driver.wait { @driver.find_element(:name, "Company").displayed? }
    @driver.wait { @driver.find_element(:name, "Title").displayed? }
    @driver.wait { @driver.find_element(:name, "Lead Score").displayed? }
end


def attendee_page_check
    @driver.wait { @driver.find_element(:name, "Check-In").displayed? }
    @driver.wait { @driver.find_element(:name, "ci plusManual").displayed? }
    @driver.wait { @driver.find_element(:name, "bg_eventSelector").displayed? }
    @driver.wait { @driver.find_element(:name, "toggleDown arrow").displayed? }
    @driver.wait { @driver.find_element(:name, "ci menuLines").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureCard").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureBarcode").displayed? }
    @driver.wait { @driver.find_element(:name, "ci captureQRcode").displayed? }
    @driver.wait { @driver.find_element(:id, "Search by name or company").displayed? }
    @driver.wait { @driver.find_element(:name, "Filter").displayed? }
    @driver.wait { @driver.find_element(:name, "Check-in").displayed? }
    @driver.wait { @driver.find_element(:name, "lastName").displayed? }
    @driver.wait { @driver.find_element(:name, "First Name").displayed? }
    @driver.wait { @driver.find_element(:name, "Company").displayed? }
    @driver.wait { @driver.find_element(:name, "Title").displayed? }
    @driver.wait { @driver.find_element(:name, "Lead Score").displayed? }
end


def verify_selected_event(event)
    @driver.wait { @selected = @driver.find_element(:name, event).selected? }
    if @selected == false
        raise "Error: Expected #{event} to be selected."
    end
end


def select_an_event_menu_check
    @driver.wait { @driver.find_element(:id, "bg_eventSelector").displayed? }
    @driver.wait { @driver.find_element(:id, "toggleUp arrow").displayed? }
    @driver.wait { @driver.find_element(:id, "Check-In").displayed? }
    @driver.wait { @driver.find_element(:id, "Select an event").displayed? }
    @driver.wait { @driver.find_element(:id, "ci menuLines").displayed? }
end


def add_attendee_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "Contact").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "First Name *").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Last Name *").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Email *").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Phone").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Fax").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Mobile").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Office Address").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "City").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Zip Code").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "State").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Country").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Title").displayed? }

    # @driver.wait { @driver.find_element(:name, "Edit Topics").displayed? }
    # @driver.wait { @driver.find_element(:name, "Edit Follow-Up Actions").displayed? }
    # @driver.wait { @driver.find_element(:name, "Done").displayed? }
    # @driver.wait { @driver.find_element(:name, "Cancel").displayed? }
    # @driver.wait { @driver.find_element(:name, "ci btnCloseX").displayed? }

    # expected_fields = ["First Name", "Last Name*", "Company*", "Job Title", "Phone", "Mobile", "Email*"]

    # field_list = Array.new(7)
    # add_attendee_box = @driver.find_element(:class, "UIAScrollView")
    # attendee_fields = add_attendee_box.find_elements(:class, "UIATextField")
    # attendee_fields.each do |field|
    #     field_list.push(field.value)
    # end
    # raise "Error: Could not find an expected field." unless (expected_fields - field_list).empty?
end


def scroll_to(field)
    @driver.scroll(:direction => 'down', name: field)
end


def add_contacts(list)
    list.each do |person|
        #click_button("ci plusManual")
        add_new_contact(person)
        add_topic_fua(person)
        #sleep 5
    end
end


def determine_contact(criteria)
    case criteria
    when $test["contact0_fname"], $test["contact0_lname"], $test["contact0_company"], $test["contact0_email"] # special chars
        contact = "contact0"
    when $test["contact1_fname"], $test["contact1_lname"], $test["contact1_company"], $test["contact1_email"] # Messi
        contact = "contact1"
    when $test["contact2_fname"], $test["contact2_lname"], $test["contact2_company"], $test["contact2_email"] # Ronaldo
        contact = "contact2"
    when $test["contact3_fname"], $test["contact3_lname"], $test["contact3_company"], $test["contact3_email"] # international chars
        contact = "contact3"
    when $test["contact4_fname"], $test["contact4_lname"], $test["contact4_company"], $test["contact4_email"] # Clarkson
        contact = "contact4"
    when $test["contact5_fname"], $test["contact5_lname"], $test["contact5_company"], $test["contact5_email"] # Hammond
        contact = "contact5"
    when $test["contact6_fname"], $test["contact6_lname"], $test["contact6_company"], $test["contact6_email"] # May
        contact = "contact6"
    when $test["contact7_fname"], $test["contact7_lname"], $test["contact7_company"], $test["contact7_email"] # Hawking
        contact = "contact7"
    when $test["contact8_fname"], $test["contact8_lname"], $test["contact8_company"], $test["contact8_email"] # Brown
        contact = "contact8"
    when $test["contact9_fname"], $test["contact9_lname"], $test["contact9_company"], $test["contact9_email"] # Abrams
        contact = "contact9"
    when $test["contact10_fname"], $test["contact10_lname"], $test["contact10_company"], $test["contact10_email"] # Baggins
        contact = "contact10"
    when $test["contact11_fname"], $test["contact11_lname"], $test["contact11_company"], $test["contact11_email"] # Hamilton
        contact = "contact11"
    when $test["contact12_fname"], $test["contact12_lname"], $test["contact12_company"], $test["contact12_email"] # Vettel
        contact = "contact12"
    when $test["contact13_fname"], $test["contact13_lname"], $test["contact13_company"], $test["contact13_email"] # Rosberg
        contact = "contact13"
    when $test["contact14_fname"], $test["contact14_lname"], $test["contact14_company"], $test["contact14_email"] # Manning
        contact = "contact14"
    when $test["contact15_fname"], $test["contact15_lname"], $test["contact15_company"], $test["contact15_email"] # Brady
        contact = "contact15"
    when $test["contact16_fname"], $test["contact16_lname"], $test["contact16_company"], $test["contact16_email"] # Kirk
        contact = "contact16"
    when $test["contact17_fname"], $test["contact17_lname"], $test["contact17_company"], $test["contact17_email"] # McCoy
        contact = "contact17"
    end

    return contact, $test["#{contact}_fname"] + " " + $test["#{contact}_lname"], $test["#{contact}_company"], $test["#{contact}_topic1"], $test["#{contact}_topic2"], $test["#{contact}_fua1"], $test["#{contact}_fua2"]
end


def add_new_contact(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    user_fields = ["#{contact}_fname", "#{contact}_lname", "#{contact}_email", "contact_phone", "contact_phone", "contact_mobile",
                    "contact_street", "contact_city", "contact_zip", "contact_state", "contact_country", "#{contact}_job_title", 
                    "#{contact}_company"]

                    #"contact_comments", "contact_phone", "contact_web"]

    @driver.wait { @driver.find_element(:accessibility_id, "ci plusManual").click }

    add_attendee_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable")
    attendee_fields = add_attendee_box.find_elements(:class, "XCUIElementTypeTextField")
    for i in 0..(user_fields.size - 1) do
        attendee_fields[i].type $test[user_fields[i]]
        @driver.find_element(:name, "Next").click
    end

    hide_keyboard
end


def add_topic_fua(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    topics_element = @driver.find_element(:accessibility_id, "Edit Topics")
    topics_element.click

    if topic1 =~ /\w+/
        topics_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeCollectionView")
        avail_topics = topics_view.find_elements(:class, "Button")
        topic_set = false
        avail_topics.each do |topic|
            if topic.name == "Topic FUA Not Selected #{topic1}"
                topic.click
                topic_set = true
            end
        end
        raise "Error: Could not set the Topic." unless topic_set
    end

    if topic2 =~ /\w+/
        topics_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeCollectionView")
        avail_topics = topics_view.find_elements(:class, "Button")
        topic_set = false
        avail_topics.each do |topic|
            if topic.name == "Topic FUA Not Selected #{topic2}"
                topic.click
                topic_set = true
            end
        end
        raise "Error: Could not set the Topic." unless topic_set
    end

    next_element = @driver.wait { @driver.find_element(:name, "Next") }
    next_element.click

    if fua1 =~ /\w+/
        fuas_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeCollectionView")
        avail_fuas = fuas_view.find_elements(:class, "Button")
        fua_set = false
        avail_fuas.each do |fua|
            if fua.name == "Topic FUA Not Selected #{fua1}"
                fua.click
                fua_set = true
            end
        end
        raise "Error: Could not set the Follow-Up Action." unless fua_set
    end

    if fua2 =~ /\w+/
        fuas_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeCollectionView")
        avail_fuas = fuas_view.find_elements(:class, "Button")
        fua_set = false
        avail_fuas.each do |fua|
            if fua.name == "Topic FUA Not Selected #{fua2}"
                fua.click
                fua_set = true
            end
        end
        raise "Error: Could not set the Follow-Up Action." unless fua_set
    end

    done_element = @driver.wait { @driver.find_element(:name, "Done") }
    done_element.click
    attendee_page_check
end


def verify_contact_list(list)
    contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable[1]")
    contacts = contact_list.find_elements(:class, "XCUIElementTypeCell")

    list.each do |person|
        found = false
        contacts.each do |contact|
            text_strings = contact.find_elements(:class, "XCUIElementTypeStaticText")
            if UnicodeUtils.nfc(person) == UnicodeUtils.nfc(text_strings[0].value)
                found = true
                break
            end
        end
        raise "Error. Didn't find an expected contact." unless found
    end
end


def magnify_contact(position)
    index = $fields_hash[position]

    contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable[1]")
    contacts = contact_list.find_elements(:class, "XCUIElementTypeCell")
    contact = contacts[index]

    last_name = contact.find_elements(:class, "XCUIElementTypeStaticText")[0].value
    $fields_hash["magnified_contact"] = last_name

    magnify_button = contact.find_element(:id, "ci magGlass")
    magnify_button.click
end


def magnify_business_card
    attendee_info_box = @driver.find_element(:class, "UIAScrollView")
    buttons = attendee_info_box.find_elements(:class, "UIAButton")
    buttons.each do |button|
        if button.name == "ci magnify"
            button.click
        end
    end
end


def verify_magnified_card
    @driver.wait { @driver.find_element(:name, "capture close").displayed? }
    image = @driver.find_element(:name, $fields_hash["bcard_file"])
    image.displayed?
    raise "Error: The enlarged business card image is not visible." unless image.size.width == $fields_hash["bcard_large_width"] && image.size.height == $fields_hash["bcard_large_height"] && image.location.x == $fields_hash["bcard_large_x"] && image.location.y == $fields_hash["bcard_large_y"]
end


def close_magnified_card
    x_button = @driver.find_element(:name, "capture close")
    x_button.click
end


def verify_contact_info
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact($fields_hash["magnified_contact"])
    attendee_info_box = @driver.find_element(:class, "UIAScrollView")

    attendee_info_box.find_element(:name, fullname).displayed?
    company_title = $test["#{contact}_job_title"] + " " + $test["#{contact}_company"]
    #attendee_info_box.find_element(:name, company_title).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_company"]).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_job_title"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_phone"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_mobile"]).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_email"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_web"]).displayed?

    $fields_hash["magnified_contact"] = ""
end


def verify_attendee_info_page
    @driver.find_element(:accessibility_id, "Attendee Information").displayed?
    @driver.find_element(:accessibility_id, "ci magnify").displayed?
    @driver.find_element(:accessibility_id, "ci btnCamera").displayed?
    @driver.find_element(:accessibility_id, "ci btnCloseX").displayed?
    #@driver.find_element(:accessibility_id, "badge-checked").displayed?
    @driver.find_element(:accessibility_id, "Edit").displayed?
    @driver.find_element(:accessibility_id, "Select").displayed?
end


def verify_named_contact_info(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)
    attendee_info_box = @driver.find_element(:class, "UIAScrollView")

    attendee_info_box.find_element(:name, fullname).displayed?
    company_title = $test["#{contact}_job_title"] + " " + $test["#{contact}_company"]
    #attendee_info_box.find_element(:name, company_title).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_company"]).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_job_title"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_phone"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_mobile"]).displayed?
    #attendee_info_box.find_element(:name, $test["#{contact}_email"]).displayed?
    #attendee_info_box.find_element(:name, $test["contact_web"]).displayed?

    $fields_hash["magnified_contact"] = ""
end


def verify_attendee_with_card_image
    @driver.wait { @driver.find_element(:name, "Attendee Information") }
    @driver.wait { @driver.find_element(:name, "Edit") }
    @driver.wait { @driver.find_element(:name, "Select") }
    @driver.wait { @driver.find_element(:name, "ci btnCloseX") }

    attendee_info_box = @driver.find_element(:class, "UIAScrollView")
    images = attendee_info_box.find_elements(:class, "UIAImage")
    images.each do |image|
        if image.name =~ /^(.*).jpg$/
            @driver.wait { @driver.find_element(:name, image.name).displayed? }
            $fields_hash["bcard_file"] = image.name
            #raise "Error: The business card image is not visible." unless image.size.width == $fields_hash["bcard_small_width"] && image.size.height == $fields_hash["bcard_small_height"] && image.location.x == $fields_hash["bcard_small_x"] && image.location.y == $fields_hash["bcard_small_y"]
        end
    end
end


def edit_attendee_page_check(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    @driver.wait { @driver.find_element(:id, "Edit Attendee Information").displayed? }

    edit_info_box = @driver.find_element(:class, "UIAScrollView")
    edit_info_box.find_element(:id, $test["#{contact}_fname"]).displayed?
    edit_info_box.find_element(:id, $test["#{contact}_lname"]).displayed?
    edit_info_box.find_element(:id, $test["#{contact}_company"]).displayed?
    edit_info_box.find_element(:id, $test["#{contact}_job_title"]).displayed?
    #edit_info_box.find_element(:id, $test["contact_phone"]).displayed?

    @driver.wait { @driver.find_element(:name, "Edit Topics").displayed? }
    @driver.wait { @driver.find_element(:name, "Edit Follow-Up Actions").displayed? }
    @driver.wait { @driver.find_element(:name, "Done").displayed? }
    @driver.wait { @driver.find_element(:name, "Cancel").displayed? }
end


def edit_field(old_value, new_value)
    edit_info_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable")
    textfields = edit_info_box.find_elements(:class, "XCUIElementTypeCell")
    textfields.each do |field|
        field_name = field.find_element(:class, "XCUIElementTypeStaticText")
        if field_name.value == old_value
            edit_field = field.find_element(:class, "XCUIElementTypeTextField")
            edit_field.click
            edit_field.clear
            edit_field.type new_value
            break
        end
    end

    #hide_keyboard
    done_button = @driver.find_element(:name, "Done")
    done_button.click
end


def clear_search_box
    clear_button = @driver.find_element(:accessibility_id, "Clear text")
    clear_button.click
end


def search_for_contact(value)
    search_box = @driver.wait { @driver.find_element(:id, "Search by name or company") }
    search_box.click
    search_box.type value
    hide_keyboard
    #search_button = @driver.wait { @driver.find_element(:name, "Search") }
    #search_button.click
end


def verify_search_contact_returned(value)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(value)

    contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable[1]")
    persons = contact_list.find_elements(:class, "XCUIElementTypeCell")
    person = persons[0]
    last_name = person.find_elements(:class, "XCUIElementTypeStaticText")[0].value

    if last_name != $test["#{contact}_lname"]
        raise "Error: Could not search for the given contact #{value}."
    end
end


def verify_no_contacts
    begin
        contact_list = @driver.find_element(:class, "UIATableView")
        #contacts = contact_list.find_elements(:class, "UIATableCell")

        contact_exists = contact_list.ele_index(:UIATableCell, 0).displayed?
        raise "Error: There should be no contacts." if contact_exists
    rescue
        return
    end
end


def filter_contacts_page_check
    @driver.wait { @driver.find_element(:name, "Filter Contacts").displayed? }
    @driver.wait { @driver.find_element(:name, "ci btnCloseX").displayed? }
    @driver.wait { @driver.find_element(:name, "Apply").displayed? }
    @driver.wait { @driver.find_element(:name, "Clear").displayed? }
    
    filter_list = @driver.find_element(:class, "UIAScrollView")
    filter_buttons = filter_list.find_elements(:class, "UIAButton")
    raise "Error: Could not find expected filter button." unless filter_buttons[0].name == "Company" && filter_buttons[1].name == "Job Title" && filter_buttons[2].name == "Topics" && filter_buttons[3].name == "Follow Up Actions"
end


def select_filter_type(filter)
    filter_list = @driver.find_element(:class, "UIAScrollView")
    filter_buttons = filter_list.find_elements(:class, "UIAButton")

    filter_buttons.each do |button|
        if button.name == filter
            button.click
            return
        end
    end

    raise "Error: Could not click the #{filter} filter."
end


def click_filter_box(option)
    option_button = @driver.find_element(:name, option)
    option_button.click

    done_button = @driver.find_element(:name, "Done")
    done_button.click
end


def verify_filter_selected(filter_type)
    filter_list = @driver.find_element(:class, "UIAScrollView")
    filter_buttons = filter_list.find_elements(:class, "UIAButton")

    case filter_type
    when "Company"
        raise "Error: Expected Company filter to be selected." unless filter_buttons[0].value == 1
    when "Job Title"
        raise "Error: Expected Job Title filter to be selected." unless filter_buttons[1].value == 1
    when "Topics"
        raise "Error: Expected Topics filter to be selected." unless filter_buttons[2].value == 1
    when "Follow Up Actions"
        raise "Error: Expected Follow Up Actions filter to be selected." unless filter_buttons[3].value == 1
    end 
end


def verify_filter_count(filter_count)
    filter_string = "Filter(#{filter_count})"
    @driver.wait { @driver.find_element(:name, filter_string).displayed? }
end


def edit_topics_page_check
    @driver.wait { @driver.find_element(:name, "Please select one or multiple topics:").displayed? }
    @driver.wait { @driver.find_element(:name, "ci btnCloseX").displayed? }
    @driver.wait { @driver.find_element(:name, "Next").displayed? }
    @driver.wait { @driver.find_element(:name, "Clear").displayed? }
end


def edit_fua_page_check
    @driver.wait { @driver.find_element(:name, "Please select one or multiple follow up actions:").displayed? }
    @driver.wait { @driver.find_element(:name, "ci btnCloseX").displayed? }
    #@driver.wait { @driver.find_element(:name, "Done").displayed? }
    @driver.wait { @driver.find_element(:name, "Clear").displayed? }
end


def verify_fieldvalue(key, value)
    attendee_info_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]")
    textboxes = attendee_info_box.find_elements(:class, "XCUIElementTypeTextField")

    textboxes.each do |label|
        if label.name == key
            raise "Error: Could not find #{value} in #{key}." unless label.value.include? value
        end
    end
end


def replace_text(old_text, new_text)
    textfield = @driver.wait { @driver.find_element(:id, old_text) }
    textfield.click
    #textfield.clear
    textfield.send_keys new_text
    hide_keyboard
    note_click_done   
end


def match_text(text)
    @driver.wait { @driver.find_element(:id, text).displayed? }
    hide_keyboard
    note_click_done 
end


def hide_keyboard
    #screen_size = @driver.window_size

    # Hack to dismiss iOS KB bc Apple bug. We will tap the actual coordinates of hide KB button.
    hide_kb = Appium::TouchAction.new 
    hide_kb.tap(:x => 963, :y => 748, :fingers => 1, :tapCount => 1, :duration => 1000).perform
end


def note_click_done
    done_button = Appium::TouchAction.new 
    done_button.tap(:x => 511, :y => 399, :fingers => 1, :tapCount => 1, :duration => 1000).perform
end


def verify_empty_fieldvalue(key)
    attendee_info_box = @driver.find_element(:class, "UIAScrollView")
    textboxes = attendee_info_box.find_elements(:class, "UIAStaticText")
end


def select_topics(list)
    topics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView")

    list.split(", ").each do |topic|
        topic_string = "Topic FUA Not Selected #{topic}"
        begin
            topics_box.find_element(:accessibility_id, topic_string).click
        rescue
            raise "Error: Expected #{topic} Topic to be deselected."
        end
    end
end


def deselect_topics(list)
    topics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView")

    list.split(", ").each do |topic|
        topic_string = "Topic FUA Selected #{topic}"
        begin
            topics_box.find_element(:accessibility_id, topic_string).click
        rescue
            raise "Error: Expected #{topic} Topic to be selected."
        end
    end
end


def subtopics_page_check(list)
    @driver.wait { @driver.find_element(:name, "ci btnCloseX").displayed? }
    @driver.wait { @driver.find_element(:name, "Done").displayed? }

    list.split(", ").each do |subtopic|
        @driver.wait { @driver.find_element(:name, subtopic).displayed? }
    end
end


def select_subtopics(list)
    list.split(", ").each do |subtopic|
        subtopic_selected = false
        subtopics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView")
        subtopic_texts = subtopics_box.find_elements(:class, "XCUIElementTypeStaticText")
        subtopic_buttons = subtopics_box.find_elements(:class, "XCUIElementTypeButton")

        index = 0
        subtopic_buttons.each do |button|
            if button.name == "Sub Action not checked"
                #cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if subtopic_texts[index].value == subtopic
                    subtopic_buttons[index].click
                    subtopic_selected = true
                    break
                end
            end
            index = index + 1
        end
        raise "Error: Could not select #{subtopic} subtopic." unless subtopic_selected
    end
end


def deselect_subtopics(list)
    raise "TBD"
end


def verify_topics_state(list, state)
    topics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |topic|
        if state == "selected"
            topic_name = "Topic FUA Selected #{topic}"
        elsif state == "deselected"
            topic_name = "Topic FUA Not Selected #{topic}"
        end
        topics_box.find_element(:name, "#{topic_name}").displayed?
    end
end

def verify_subtopics_state(list, state)
    raise "TBD"
end

def verify_topics_list_alpha_order(list)
    list_array = Array.new
    list.split(", ").each do |topic|
        list_array.push(topic)
    end
    list_array.sort

    visible_topics_array = Array.new
    topics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")
    ui_topics = topics_box.find_elements(:class, "XCUIElementTypeButton")
    ui_topics.each do |topic|
        topic_name = topic.name.sub(/^Topic FUA (Not )?Selected /, '')
        visible_topics_array.push(topic_name)
    end

    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_topics_array[i] == list_array[i]
    end
end


def verify_subtopics_list_alpha_order(subtopics_list)
    list_array = Array.new
    subtopics_list.split(", ").each do |subtopic|
        list_array.push(subtopic)
    end
    list_array.sort

    visible_subtopics_array = Array.new
    ui_subtopics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]")
    ui_subtopics_list = ui_subtopics_box.find_elements(:class, "XCUIElementTypeStaticText")

    ui_subtopics_list.each do |subtopic|
        if subtopic.displayed?
            visible_subtopics_array.push(subtopic.name)
        end
    end

    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_subtopics_array[i] == list_array[i]
    end
end

def click_fuas(list)
    fua_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |fua|
        begin
            topic_string = "Topic FUA Not Selected #{fua}"
            fua_box.find_element(:id, topic_string).click
        rescue
            topic_string = "Topic FUA Selected #{fua}"
            fua_box.find_element(:id, topic_string).click
        end
    end
end

def select_fuas(list)
    fua_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |fua|
        topic_string = "Topic FUA Not Selected #{fua}"
        fua_box.find_element(:id, topic_string).click
    end
end


def deselect_fuas(list)
    fua_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |fua|
        topic_string = "Topic FUA Selected #{fua}"
        fua_box.find_element(:id, topic_string).click
    end
end


def verify_fuas_state(list, state)
    fua_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |fua|
        if state == "selected"
            fua_name = "Topic FUA Selected #{fua}"
        elsif state == "deselected"
            fua_name = "Topic FUA Not Selected #{fua}"
        end
        fua_box.find_element(:name, "#{fua_name}").displayed?
    end
end


def verify_all_topics_state(state)
    visible_topics_array = Array.new
    topics_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")
    topics = topics_box.find_element(:class, "XCUIElementTypeButton")

    topics.each do |topic|
        if topic.displayed?
            visible_topics_array.push(topic.name)
        end
    end

    for i in 0..visible_topics_array.size - 1
        if state == "selected"
            raise "Error: #{visible_topics_array[i]} is not selected." unless visible_topics_array[i].value == 1
        elsif state == "deselected"
            raise "Error: #{visible_topics_array[i]} is selected." unless visible_topics_array[i].value != 1
        end    
    end
end


def verify_all_fuas_state(state)
    visible_fua_array = Array.new
    fua_box = @driver.find_element(:class, "UIAScrollView")
    fuas = fua_box.find_element(:class, "UIAButton")

    fuas.each do |fua|
        if fua.displayed?
            visible_fua_array.push(fua.name)
        end
    end

    for i in 0..visible_fua_array.size - 1
        if state == "selected"
            raise "Error: #{visible_fua_array[i]} is not selected." unless visible_fua_array[i].value == 1
        elsif state == "deselected"
            raise "Error: #{visible_fua_array[i]} is selected." unless visible_fua_array[i].value != 1
        end    
    end
end


def subfuas_page_check(list)
    @driver.find_element(:name, "Done").displayed?
    subfua_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]")

    list.split(", ").each do |subfua|
        subfua_box.find_element(:name, "#{subfua}").displayed?
    end
end


def select_subfuas(list)
    scrollviews = @driver.find_elements(:class, "UIAScrollView")
    subfua_box = scrollviews[2]

    list.split(", ").each do |subfua|
        subfua_element = subfua_box.find_element(:name, subfua)
        if subfua_element.value != 1
            subfua_element.click
        else
            raise "Error: Expected #{subfua_element.name} sub follow-up action to be deselected but it is already selected."
        end
    end
end


def deselect_subfuas(list)
    scrollviews = @driver.find_elements(:class, "UIAScrollView")
    subfua_box = scrollviews[2]

    list.split(", ").each do |subfua|
        subfua_element = subfua_box.find_element(:name, subfua)
        if subfua_element.value == 1
            subfua_element.click
        else
            raise "Error: Expected #{subfua_element.name} sub follow-up action to be selected but it is already deselected."
        end
    end
end


def verify_subfuas_state(list, state)
    scrollviews = @driver.find_elements(:class, "UIAScrollView")
    subfua_box = scrollviews[2]

    list.split(", ").each do |subfua|
        subfua_element = subfua_box.find_element(:name, subfua)
        if state == "selected"
            raise "Error: The #{subfua_element.name} sub follow-up action is not #{state} as expected." unless subfua_element.selected?
        elsif state == "deselected"
            raise "Error: The #{subfua_element.name} sub follow-up action is not #{state} as expected." unless !subfua_element.selected?
        end
    end    
end


def verify_note_popup(title)
    @driver.wait { @driver.find_element(:name, title).displayed? }
    @driver.wait { @driver.find_element(:name, "Done").displayed? }
end


def verify_fua_list_alpha_order(fua_list)
    # list_array = Array.new
    # fua_list.split(", ").each do |fua|
    #     list_array.push(fua)
    # end
    # list_array.sort

    # visible_fua_array = Array.new
    # ui_fua_list = @driver.find_element(:class, "UIAScrollView").find_elements(:class, "UIAButton")
    # ui_fua_list.each do |fua|
    #     if fua.displayed?
    #         visible_fua_array.push(fua.name)
    #     end
    # end

    # for i in 0..list_array.size - 1
    #     raise "Error: #{list_array[i]} is not displayed in the list." unless visible_fua_array[i] == list_array[i]
    # end

    list_array = Array.new
    fua_list.split(", ").each do |fua|
        list_array.push(fua)
    end
    list_array.sort

    visible_fuas_array = Array.new
    fuas_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView[1]")
    ui_fuas = fuas_box.find_elements(:class, "XCUIElementTypeButton")
    ui_fuas.each do |fua|
        fua_name = fua.name.sub(/^Topic FUA (Not )?Selected /, '')
        visible_fuas_array.push(fua_name)
    end

    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_fuas_array[i] == list_array[i]
    end
end


def verify_subfua_list_alpha_order(subfua_list)
    # list_array = Array.new
    # subfua_list.split(", ").each do |subfua|
    #     list_array.push(subfua)
    # end
    # list_array.sort

    # visible_subfua_array = Array.new
    # ui_subfua_list = @driver.find_elements(:class, "UIAScrollView")[2].find_elements(:class, "UIAStaticText")
    # ui_subfua_list.each do |subfua|
    #     if subfua.displayed?
    #         puts subfua.name
    #         visible_subfua_array.push(subfua.name)
    #     end
    # end

    # for i in 0..list_array.size - 1
    #     raise "Error: #{list_array[i]} is not displayed in the list." unless visible_subfua_array[i] == list_array[i]
    # end

    list_array = Array.new
    subfua_list.split(", ").each do |subfua|
        list_array.push(subfua)
    end
    list_array.sort

    visible_subfuas_array = Array.new
    ui_subfuas_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]")
    ui_subfuas_list = ui_subfuas_box.find_elements(:class, "XCUIElementTypeStaticText")

    ui_subfuas_list.each do |subfua|
        if subfua.displayed?
            visible_subfuas_array.push(subfua.name)
        end
    end

    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_subfuas_array[i] == list_array[i]
    end

end


def logout
    click_menu_button
    logout_element = @driver.wait { @driver.find_element(:id, "Logout") }
    logout_element.click
end


def search_event(event)
    search_box = @driver.find_element(:accessibility_id, "Event List Search Field")
    search_box.click
    search_box.type event
    search_button = @driver.find_element(:accessibility_id, "Search")
    search_button.click
end


def only_event_displayed(event)
    events = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable")
    event_cells = @driver.find_elements(:class, "XCUIElementTypeCell")
    raise "Error: Only 1 event should be displayed." unless event_cells.size == 1

    event_name = event_cells.find_element(:accessibility_id, event)
end


def delete_contacts(number)
    for i in 0..(number.to_i - 1)
        contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable")
        contacts = contact_list.find_elements(:class, "XCUIElementTypeCell")
        
        first_contact = contacts[0]
        person = first_contact.find_elements(:class, "XCUIElementTypeStaticText")[0]
        @deleted_contacts.push(person.name)

        @driver.swipe(direction: "left", element: first_contact)
        click_button("Delete")
        click_button("Yes")
        sleep 3
    end
end


def verify_contacts_deleted(number)
    for i in 0..(number.to_i - 1)
        fullname = @deleted_contacts.pop
        @deleted_contacts.push(fullname)

        if element_exists_by_name?(fullname)
            raise "Error: Expected contact #{fullname} to have been deleted."
        end
    end
end


# Check if an element exists and is displayed. Return true or false.
def element_exists_by_name?(my_name)
    #sleep 3
    begin
        element = @driver.find_element(:accessibility_id, my_name).displayed?
        true if element
    rescue
        false
    end
end


def filter_contacts(filter, value)
    filter_element = @driver.wait { @driver.find_element(:accessibility_id, filter) }
    filter_element.click

    value_element = @driver.wait { @driver.find_element(:accessibility_id, value) }
    value_element.click

    done_element = @driver.wait { @driver.find_element(:accessibility_id, "Done") }
    done_element.click
end


def apply_filter
    apply_element = @driver.wait { @driver.find_element(:accessibility_id, "Apply") }
    apply_element.click
end


def select_first_contact(status)
    if status == "checked"
        match_criteria = "Attendee cell checked in / Green check mark"
    else
        match_criteria = "Attendee cell not checked in / Grey circle"
    end

    contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable")
    contacts = contact_list.find_elements(:class, "XCUIElementTypeCell")
    contacts.each do |row|
        if row.name == match_criteria
            row.find_element(:accessibility_id, "ci magGlass").click
            break
        end
    end
end


def verify_topic_field_value(value)
    topic_fields = @driver.find_elements(:accessibility_id, "Topic\(s\)")
    topic_fields.each do |row|
        return if row.value == value
    end
    raise "Error: The Topic field did not have the value #{value}."
end


def verify_fua_field_value(value)
    fua_fields = @driver.find_elements(:accessibility_id, "Follow-Up Action\(s\)")
    fua_fields.each do |row|
        return if row.value == value
    end
    raise "Error: The Follow-Up Action field did not have the value #{value}."
end
