# encoding: UTF-8

@checkin_feature_attendee_information @13
Feature: Attendee Information Page

Background:
  Given I am logged out on the app


# Currently fails because the image file name can't be seen.
@13.1 @wip
Scenario: 13.1 Enlarge attendee business card image
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "Uzair"
 	Then the user is on the landing page
 	When the user chooses event "Card Scanner Demo" on the landing page
	Then the user will be on the attendee page for event "Card Scanner Demo"
	When the user waits "10" seconds
	And the user searches for the contact with "Last Name" "wilde"
	And the user selects the "first" contact
	Then the Attendee Information page should contain a business card image
	When the user clicks the business card magnify button
   	Then the enlarged business card should appear
   	When the user closes the magnified business card
   	Then the Attendee Information page should contain a business card image


@13.2 @wip
Scenario: 13.2 Click the phone number on the Attendee Information page

@13.3 @wip
Scenario: 13.3 Click the cell phone number on the Attendee Information page

@13.4 @wip
Scenario: 13.4 Click the email address on the Attendee Information page


@13.5
Scenario: 13.5 Click Edit on the Attendee Information page and then click the X button
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
 	Then the Edit Attendee Information page for "Clarkson" will appear
 	When the user clicks the X button
 	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@13.6
Scenario: 13.6 Click Select on the Attendee Information page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Select"
	Then the user should be on the Edit Topics page


@13.7
Scenario: 13.7 Select an attendee that has checked in
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user selects the first "checked" in contact
	Then the Attendee Information page will open


@13.8
Scenario: 13.8 Select an attendee that has not checked in
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Auto Long Topics" on the landing page
	Then the user will be on the attendee page for event "Mobile Auto Long Topics"
	When the user selects the first "unchecked" in contact
	Then the Attendee Information page will open
