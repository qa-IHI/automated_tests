# encoding: UTF-8

@checkin_feature_follow_up_actions @16
Feature: Follow Up Actions

@16.1
Scenario: 16.1 Press the X button on the Follow Up Actions page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "May"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "May" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the X button
	And the user clicks the X button
	Then the Edit Attendee Information page for "May" will appear

@16.2
Scenario: 16.2 Select and de-select a follow-up action that has no sub follow-up actions
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Jupiter" follow-up actions
	Then the "Jupiter" follow-up actions should be "selected"
 	When the user deselects the "Jupiter" follow-up actions
 	Then the "Jupiter" follow-up actions should be "deselected"

@16.3
Scenario: 16.3 Select a follow-up action with sub follow-up actions
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Languages" follow-up actions
	Then the sub follow-up actions should open with "English, French, German, Spanish"


# Currently fails because we can't tell if a sub-FUA is selected or deselected. CEA-6151.
@16.4 @wip
Scenario: 16.4 Select and de-select a sub follow-up action
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Languages" follow-up actions
	Then the sub follow-up actions should open with "English, French, German, Spanish"
	When the user selects the "English, Spanish" sub follow-up actions
	Then the "English, Spanish" sub follow-up actions should be "selected"
	When the user clicks "Done"
	Then the "Languages" follow-up actions should be "selected"
	When the user selects the "Languages" follow-up actions
	Then the sub follow-up actions should open with "English, French, German, Spanish"
	And the "English, Spanish" sub follow-up actions should be "selected"
	When the user deselects the "English, Spanish" sub follow-up actions
	And the user clicks "Done"
	Then the "Languages" follow-up actions should be "deselected"

@16.5
Scenario: 16.5 Select a Note follow-up action
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "A test note" follow-up actions
	Then the Note pop-up should appear with title "A test note"
	And the text should match "This is a test note."

@16.6
Scenario: 16.6 Edit a Note's default text and press done
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "A test note" follow-up actions
	And the user changes the text from "This is a test note." to "A reasonably priced car"
	Then the user should be on the Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Follow-Up Action(s)" field should contain "A test note"
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"

@16.7
Scenario: 16.7 Edit follow-up action Note and use special characters
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the "A test note" follow-up actions
	And the user changes the text from "This is a test note." to "!@#$%&+?/_\"\",.()-:;*="
	Then the user should be on the Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Follow-Up Action(s)" field should contain "A test note"
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	And the "A test note" follow-up actions should be "selected"
	When the user clicks the "A test note" follow-up actions
	Then the Note pop-up should appear with title "A test note"
	And the text should match "!@#$%&+?/_\"\",.()-:;*="

@16.8
Scenario: 16.8 Add a follow-up action Note while editing a contact
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user changes the "Phone" from "01234567890" to "777777777"
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Note" follow-up actions
	And the user changes the text from "Some random comments." to "Charing Cross"
	Then the user should be on the Follow-Up Actions page
	And the "Note" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Comments" field should contain "Charing Cross"

@16.9
Scenario: 16.9 Enter more than 500 characters for a follow-up action Note
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the "Note" follow-up actions
	And the user changes the text from "Charing Cross" to more than 500 characters
 	Then the user will receive a "Notes have exceeded 500 maximum character limit." message

@16.10
Scenario: 16.10 De-select a follow-up action Note
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the "Note" follow-up actions
	And the user changes the text from "Charing Cross" to "Formula E Racing"
	Then the user should be on the Follow-Up Actions page
	And the "Note" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Comments" field should contain "Formula E Racing"
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	And the "Note" follow-up actions should be "selected"
	When the user clicks "Clear"
	Then the "Note" follow-up actions should be "deselected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Comments" field should be empty

@16.11
Scenario: 16.11 Select and edit a Free Text follow-up action
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Free Text" follow-up actions
	Then the Free Text pop-up should appear with title "Free Text"
	When the user changes the text from "default free text" to "Lets drive around Monaco!"
	Then the user should be on the Follow-Up Actions page
	And the "Free Text" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"

@16.12
Scenario: 16.12 De-select a follow-up action Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	And the "Free Text" follow-up actions should be "selected"
	When the user clicks "Clear"
	Then the "Free Text" follow-up actions should be "deselected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"

@16.13
Scenario: 16.13 Edit follow-up action Free Text and use special characters
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Free Text" follow-up actions
	And the user changes the text from "default free text" to "!@#$%&+?/_\"\",.()-:;*="
	Then the user should be on the Follow-Up Actions page
	And the "Free Text" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Follow-Up Action(s)" field should contain "Free Text"
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	And the "Free Text" follow-up actions should be "selected"
	When the user clicks the "Free Text" follow-up actions
	Then the Note pop-up should appear with title "Free Text"
	And the text should match "!@#$%&+?/_\"\",.()-:;*="

@16.14
Scenario: 16.14 Add a follow-up action Free Text while editing a contact
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user changes the "Phone" from "01234567890" to "777777777"
	And the user clicks "Edit"
	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the "Free Text" follow-up actions
	And the user changes the text from "!@#$%&+?/_\"\",.()-:;*=" to "Waterloo Station"
	Then the user should be on the Follow-Up Actions page
	And the "Free Text" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Follow-Up Action(s)" field should contain "Free Text"

@16.15
Scenario: 16.15 Enter more than 500 characters for a follow-up action Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user clicks the "Free Text" follow-up actions
	And the user changes the text from "Waterloo Station" to more than 500 characters
 	Then the user will receive a "Notes have exceeded 500 maximum character limit." message

#@****!!!!!! Check scrolling error with physical device on long list of sub follow up actions.
@16.16
Scenario: 16.16 Long list of follow-up actions and sub follow-up actions
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Auto Long Topics" on the landing page
	Then the user will be on the attendee page for event "Mobile Auto Long Topics"
	When the user searches for the contact with "Last Name" "Kirk"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Kirk" will appear
	When the user clicks "Edit Follow-Up Actions"
 	Then the user should be on the Follow-Up Actions page
	And the Follow Up Actions page should display "Catalunya, Circuit of the Americas, Daytona, Hungaroring, Laguna Seca, Melbourne, Monaco, Mont Tremblant, Nurburgring, Silverstone, Spa, Suzuka" follow up actions in alpha order
	When the user scrolls to the end of the follow up actions list
	Then the Follow Up Actions page should display "Texas Motor Speedway, Top Gear Track, Zandvoort" follow up actions in alpha order
	When the user clicks the X button 
	And the user clicks the X button
	Then the Edit Attendee Information page for "Kirk" will appear
	When the user clicks "Edit Follow-Up Actions"
 	Then the user should be on the Follow-Up Actions page
 	When the user selects the "Monaco" follow-up actions
    Then the "Monaco" sub-follow up actions pop up should appear
    And the sub-follow up actions page should display "A, B, D, F, G, S, V, Z, m, n, o, q" sub-follow up actions in alpha order
    When the user scrolls to the end of the sub-follow up actions list
    And the sub-follow up actions page should display "D,F, G, S, V, Z, m, n, o, q, t" sub-follow up actions in alpha order


# Currently fails because we can't tell if a sub-FUA is selected or deselected. CEA-6151. Need DEV support
@16.17 @wip
Scenario: 16.17 Follow-up actions clear button
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user clicks "Edit Follow-Up Actions"
 	Then the user should be on the Follow-Up Actions page
 	When the user selects the "Jupiter, Mars, Pluto" follow-up actions
 	And the user selects the "Languages" follow-up actions
 	Then the sub follow-up actions should open with "English, French, German, Spanish"
 	When the user selects the "English, Spanish" sub follow-up actions
	Then the "English, Spanish" sub follow-up actions should be "selected"
	When the user clicks "Done"
	Then the "Jupiter, Mars, Pluto, Languages" follow-up actions should be "selected"
	When the user clicks "Clear"
	Then all follow-up actions should be "deselected"

@16.18
Scenario: 16.18 Verify follow-up actions for a new contact
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Edit Follow-Up Actions"
 	Then the user should be on the Follow-Up Actions page
	And the "Pluto, Mars" follow-up actions should be "selected"

@16.19
Scenario: 16.19 Add new contact with follow-up actions but no topics
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "F1" contacts
 	Then the attendees list should include "F1" contacts
 	When the user searches for the contact with "Last Name" "Rosberg"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Rosberg" will appear
 	When the user clicks "Edit Topics"
 	Then the user should be on the Edit Topics page
 	And the "Android, Python, Ruby" topics should be "deselected"
 	When the user clicks "Next"
 	Then the user should be on the Follow-Up Actions page
	And the "Mars, Jupiter" follow-up actions should be "selected"
	