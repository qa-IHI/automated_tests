@checkin_feature_deleting_attendees @qa_ready @12
Feature: Deleting Attendees

Background:
  Given I am logged out on the app


@12.1
 Scenario: 12.1 Add Top Gear contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "Top Gear" contacts
 	Then the attendees list should include "Top Gear" contacts


@12.2
 Scenario: 12.2 Add F1 contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "F1" contacts
 	Then the attendees list should include "F1" contacts


@12.3
Scenario: 12.3 Delete an attendee
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user deletes the first "1" contacts
  	Then the first "1" contacts will not be on the Contacts List
  	When the user logs out of the Check-In app
  	Then the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	And the first "1" contacts will not be on the Contacts List


@12.4
Scenario: 12.4 Delete 2 attendees and then log out and log back in
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user deletes the first "2" contacts
  	Then the first "2" contacts will not be on the Contacts List
  	When the user logs out of the Check-In app
  	Then the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	And the first "2" contacts will not be on the Contacts List


@12.5
Scenario: 12.5 Search for an attendee and then delete the attendee
	Given the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
  	When the user searches for the contact with "Last Name" "Hamilton"
  	Then the contact with "Last Name" "Hamilton" should be returned by the search
  	When the user deletes the first "1" contacts
  	And the user logs out of the Check-In app
  	Then the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
  	And the first "1" contacts will not be on the Contacts List


# Currently cannot click on a filter without support from Dev.
@12.6 @wip
Scenario: 12.4 Filter an attendee and then delete the attendee
	Given the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user clicks "Filter"
  	And the user filters by the "Company" named "Top Gear"
  	And the user applies the filter
	Then the Filter button will show "1" filter applied
	And the contact with "Last Name" "Clarkson" should be returned by the search
  	When the user deletes the first "1" contacts
  	And the user logs out of the Check-In app
  	Then the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
  	And the first "1" contacts will not be on the Contacts List


@12.7
Scenario: 12.5 Add a new attendee and then delete the attendee
	Given the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 1" on the landing page
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
  	When the user adds the "Hawking" contacts
 	Then the attendees list should include "Hawking" contacts
 	When the user searches for the contact with "Last Name" "Hawking"
  	Then the contact with "Last Name" "Hawking" should be returned by the search
  	When the user deletes the first "1" contacts
  	And the user logs out of the Check-In app
  	Then the user is on the Login screen of the Check-In app
  	When the user successfully logs in as "QA"
  	Then the user is on the landing page
  	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
  	And the first "1" contacts will not be on the Contacts List

