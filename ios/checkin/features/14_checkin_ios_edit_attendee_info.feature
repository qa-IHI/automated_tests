# encoding: UTF-8

@checkin_feature_edit_attendee @qa_ready @14
Feature: Edit Attendee Information Page

Background:
  Given I am logged out on the app


@14.1
 Scenario: 14.1 NFL contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
 	When the user adds the "NFL" contacts
 	Then the attendees list should include "NFL" contacts


@14.2
Scenario: 14.2 Click X icon on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks the X button
	Then the Attendee Information page for "Clarkson" will appear


@14.3
Scenario: 14.3 Click Edit Topics on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page


@14.4
Scenario: 14.4 Click Edit Follow-Up Actions on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page


@14.5
Scenario: 14.5 Click Cancel on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Cancel"
	Then the Attendee Information page for "Clarkson" will appear


@14.6
Scenario: 14.6 Click Done on Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@14.7
Scenario: 14.7 Edit attendee without last name
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user changes the "Last Name*" from "Clarkson" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Last Name. Please try again." message


@14.8
Scenario: 14.8 Edit attendee without company
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user changes the "Company*" from "Top Gear" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Company. Please try again." message


@14.9
Scenario: 14.9 Edit attendee without email
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user changes the "Email*" from "jeremy@vroom.com" to ""
	And the user clicks "Done"
	Then the user will receive an "Invalid Email. Please try again." message


@14.10
Scenario: 14.10 Edit attendee with invalid email
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user changes the "Email*" from "jeremy@vroom.com" to "?????????!!!"
	And the user clicks "Done"
	Then the user will receive an "Invalid Email. Please try again." message


@14.11
Scenario: 14.11 Edit attendee and press cancel
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Clarkson"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Clarkson" will appear
	When the user changes the "Company*" from "Top Gear" to "Amazon.com"
	And the user clicks "Cancel"
	Then the Attendee Information page for "Clarkson" will appear
	And the "Company" field should contain "Top Gear"


@14.12
Scenario: 14.12 Edit attendee and press done
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Hammond"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hammond" will appear
	When the user changes the "Company*" from "Top Gear" to "Amazon.com"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Company" field should contain "Amazon.com"


@14.13
Scenario: 14.13 Edit Topics from Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Rosberg"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Rosberg" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Ruby" topics
	Then the "Ruby" topics should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Rosberg"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the Topics field should contain "Ruby"


# Currently fails bc meta value doesn't indicate when a subtopic is checked/selected. Need to open a JIRA.
@14.14 @wip
Scenario: 14.14 Edit Sub Topics from Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Brady"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Brady" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Goalkeeping" topics
	Then the sub-topics page should display "Long Kick, Punch, Throw In" sub-topics in alpha order

    When the user selects the "Long Kick, Punch" sub-topics

	Then the "Long Kick, Punch" sub-topics should be "selected"
	When the user clicks "Done"
	Then the "Goalkeeping" topics should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Brady"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the Topics field should contain "Long Kick, Punch"


@14.15
Scenario: 14.15 Edit Follow-Up Actions from Edit Attendee page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Brady"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Brady" will appear
	When the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Follow-Up Actions page
	When the user selects the "Han Solo" follow-up actions
	Then the "Han Solo" follow-up actions should be "selected"
	When the user clicks "Next"
	And the user clicks "Done"
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Brady"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the Follow-Up Actions field should contain "Han Solo"


@14.16
Scenario: 14.16 Edit Note/Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Manning" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "deselected" FUA "Free Text"
	And the user changes the text from "default free text" to "I edited this free text"
	And the user clicks "deselected" FUA "Note"
  	And the user changes the text from "Some random comments." to "I edited this note"
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Manning" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "selected" FUA "Free Text"
	Then the text should match "default free textI edited this free text"
	When the user clicks "selected" FUA "Note"
  	Then the text should match "Some random comments.I edited this note"


@14.17
Scenario: 14.17 Edit existing Note/Free Text
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Manning" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "selected" FUA "Free Text"
	And the user changes the text from "default free textI edited this free text" to "And on that bombshell"
	And the user clicks "selected" FUA "Note"
  	And the user changes the text from "Some random comments.I edited this note" to "Test"
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Manning" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "selected" FUA "Free Text"
	Then the text should match "default free textI edited this free textAnd on that bombshell"
	When the user clicks "selected" FUA "Note"
  	Then the text should match "Some random comments.I edited this noteTest"


@14.18
Scenario: 14.18 Clear an existing note
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Manning" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page
	When the user clicks "selected" FUA "Note"
  	And the user changes the text from "Some random comments.I edited this noteTest" to ""
  	And the user clicks "Done"
  	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Manning"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the "Comments" field should be empty


