# encoding: UTF-8

@checkin_feature_search_attendees @qa_ready @9
Feature: Search Attendees

Background:
  Given I am logged out on the app


@9.1
Scenario: 9.1 Search for an existing attendee by last name and then choose the attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"  	
	When the user searches for the contact with "Last Name" "Clarkson"
	Then the contact with "Last Name" "Clarkson" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@9.2
Scenario: 9.2 Search for an existing attendee by first name and then choose the attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"  	
	When the user searches for the contact with "First Name" "Richard"
	Then the contact with "First Name" "Richard" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@9.3
Scenario: 9.3 Search for an existing attendee by company name and then choose the attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"  	
	When the user searches for the contact with "Company Name" "FC Barcelona"
	Then the contact with "Company Name" "FC Barcelona" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 1"


@9.4
Scenario: 9.4 Search for a non-existant attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"  	
	When the user searches for the contact with "Last Name" "Pirlo"
	Then the contact with "Last Name" "Pirlo" should not be returned by the search


@9.5
Scenario: 9.5 Search for an existing attendee by last name with special characters and then choose the attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"  	
	When the user searches for the contact with "Last Name" ")(*&^%$$##@!><?:{"
	Then the contact with "Last Name" ")(*&^%$$##@!><?:{" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 2"


@9.6
Scenario: 9.6 Edit an attendee and immediately search for the attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Ronaldo"
	Then the contact with "Last Name" "Ronaldo" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user changes the "Company*" from "Real Madrid" to "Manchester United"
	And the user clicks "Done"
	And the user clears the search box
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Company Name" "Manchester United"
	Then the contact with "Last Name" "Ronaldo" should be returned by the search
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	And the contact information for "Ronaldo" will have "Company Name" "Manchester United"
