# encoding: UTF-8

@checkin_feature_filter_attendees @wip @11
Feature: Filter Attendees

Background:
  Given I am logged out on the app
  

# Add contacts here??

# Deferred. Cannot automate until filter checkboxes are associated with the filter choice.
@11.1 @wip
Scenario: 11.1 Select the filter button
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user clicks "Filter"
	Then the user should be on the Filter Contacts page


@11.2
Scenario: 11.2 Filter attendees based on company
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks "Filter"
	Then the user should be on the Filter Contacts page
	When the user selects the "Company" filter
	And the user selects "FC Barcelona" on the "Company" filter
	Then the "Company" filter will have a green check mark
	When the user clicks "Apply"
	Then the Filter button will show "1" filter applied
	And the attendees list should include "Messi" contacts
	When the user clicks "Filter"
	Then the user should be on the Filter Contacts page
	When the user selects the "Company" filter
	And the user selects "¿¡ÀÈÌÒÙÇÃÑÕ" on the "Company" filter
	Then the "Company" filter will have a green check mark
	When the user clicks "Apply"
	Then the Filter button will show "2" filter applied
	And the attendees list should include "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" contacts


@11.3 @wip
Scenario: 11.3 Filter attendees based on job title

@11.4 @wip
Scenario: 11.4 Filter attendees based on topics

@11.5 @wip
Scenario: 11.5 Filter attendees based on follow-up actions

@11.6 @wip
Scenario: 11.6 Filter attendees based on company

@11.7 @wip
Scenario: 11.7 Clear filters