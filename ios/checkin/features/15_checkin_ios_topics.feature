# encoding: UTF-8

@checkin_feature_topics @qa_ready @15
Feature: Topics

@15.1
Scenario: 15.1 Click X icon on the Topics page
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "Ronaldo"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Ronaldo" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks the X button
	Then the Edit Attendee Information page for "Ronaldo" will appear

@15.2
Scenario: 15.2 Select and de-select a topic that has no sub-topics
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "May"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "May" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Android" topics
	Then the "Android" topics should be "selected"
	When the user deselects the "Android" topics
	Then the "Android" topics should be "deselected"

@15.3
Scenario: 15.3 Select a topic that has sub-topics
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Goalkeeping" topics
	Then the sub-topics should open with "Long Kick, Punch, Throw In" sub-topics

# Currently cannot tell if a subtopic is selected or not. Need Dev support.
@15.4 @wip
Scenario: 15.4 Select and de-select a sub-topic
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user searches for the contact with "Last Name" "Messi"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Messi" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Corner Kicks" topics
	Then the sub-topics should open with "Center, Left, Right" sub-topics
	When the user selects the "Left" sub-topics
	Then the "Left" sub-topics should be "selected"
	When the user deselects the "Left" sub-topics
	Then the "Left" sub-topics should be "deselected"
	When the user clicks "Done"
	Then the "Corner Kicks" topics should be "deselected"

@15.5
 Scenario: 15.5 Add Starfleet contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Auto Long Topics" on the landing page
	Then the user will be on the attendee page for event "Mobile Auto Long Topics"
 	When the user adds the "Starfleet" contacts
 	Then the attendees list should include "Starfleet" contacts

@15.6
Scenario: 15.6 Long list of topics and sub-topics
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Auto Long Topics" on the landing page
	Then the user will be on the attendee page for event "Mobile Auto Long Topics"
	When the user searches for the contact with "Last Name" "Kirk"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Kirk" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	And the Topics page should display "Alfa Romeo, Aston Martin, Benetton, Brabham, Caterham, Ferrari, Force India, Honda, Lancia, Lotus, Marussia, McLaren" topics in alpha order
	When the user scrolls to the end of the topics list
    Then the Topics page should display "Mercedes, Porsche, Red Bull Racing, Sauber, Toro Rosso, Williams" topics in alpha order
    When the user clicks "Topic FUA Not Selected Williams"
    Then the "Williams" sub-topics pop up should appear
    And the sub-topics page should display "A, B, C, D, E, F, G, H, J, M, N, O" sub-topics in alpha order
    When the user scrolls to the end of the sub-topics list
    And the sub-topics page should display "N, O, P, S, T, U, X, Z" sub-topics in alpha order

@15.7
Scenario: 15.7 Topics clear button
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "May"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "May" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user selects the "Android, Python, Ruby" topics
	Then the "Android, Python, Ruby" topics should be "selected"
	When the user clicks "Clear"
	Then the "Android, Python, Ruby" topics should be "deselected"

@15.8
Scenario: 15.8 Topics next button
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user searches for the contact with "Last Name" "May"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "May" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	When the user clicks "Next"
	Then the user should be on the Follow-Up Actions page


# Currently affected by JIRA bug.
@15.9 @wip
Scenario: 15.9 Add new contact with topics but no follow up actions
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user adds the "F1" contacts
	And the user searches for the contact with "Last Name" "Hamilton"
	And the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks "Edit"
	Then the Edit Attendee Information page for "Hamilton" will appear
	When the user clicks "Edit Topics"
	Then the user should be on the Edit Topics page
	And the "Python, Ruby" topics should be "selected"


