# encoding: UTF-8

@checkin_features_contacts @qa_ready @4
Feature: Contacts

Background:
  Given I am logged out on the app

# This test currently fails because we can't tell if a menu item is highlighted.
@4.1 @wip
Scenario: 4.1 Menu icon from Contacts
	Given the user is on the Login screen of the Check-In app
  And the user successfully logs in as "QA"
  And the user goes to the Contact page
  And the user clicks the Menu Icon
  Then the app menu should open and "Contact" should be highlighted


@4.2
Scenario: 4.2 Home button from Contacts
  Given the user is on the Login screen of the Check-In app
  And the user successfully logs in as "QA"
  And the user goes to the Contact page
  And the user clicks the Home button
  Then the user is on the landing page
