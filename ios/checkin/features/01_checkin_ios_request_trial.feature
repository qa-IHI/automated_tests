# encoding: UTF-8

@checkin_feature_request_a_trial @wip @1
Feature: Request a Trial

@1.1
Scenario: 1.1 "I agree to atEvent Terms of Service" is unchecked by default
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	And the "I agree to atEvent Terms of Service" checkbox is unchecked

@1.2
Scenario: 1.2 Request a trial with no first name
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "First name required" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.3
Scenario: 1.3 Request a trial with no last name
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "Last name required" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.4
Scenario: 1.4 Request a trial with no company name
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a phone number
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "Company name required" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.5
Scenario: 1.5 Request a trial with no phone number
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "Phone number required" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.6
Scenario: 1.6 Request a trial with phone number containing letters and symbols
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number contain letters and symbols
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "Invalid phone number. Please try again." message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.7
Scenario: 1.7 Request a trial with no email address
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user clicks "Email"
   	And the user clicks "Submit"
   	Then the user will receive a "Email address required" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.8
Scenario: 1.8 Request a trial with invalid email address format
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user enters invalid email
   	And the user clicks "Submit"
   	Then the user will receive a "Invalid email address!" message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.9
Scenario: 1.9 Request a trial without accepting the Terms of Service
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user enters an email
   	And the user clicks "Submit"
   	Then the user will receive a "Please accept terms of services before proceeding further." message
	When the user clicks "OK"
	Then the user is back on the Request a Trial page

@1.10
Scenario: 1.10 Request a trial with all valid information
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user enters a first name
   	And the user enters a last name
   	And the user enters a company name
   	And the user enters a phone number
   	And the user enters an email
   	And user accepts the Terms of Service
   	And the user clicks "Submit"
   	Then the user will receive a "Thank you, a representative from atEvent will contact you shortly" message
	When the user clicks "OK"
	Then the user is on the Login screen of the Check-In app

@1.11
Scenario: 1.11 Request a trial and click the X button
	Given the user is on the Login screen of the Check-In app
	When the user clicks "Request a Trial"
	Then the user is on the Request a Trial page
	When the user clicks the X button
	Then the user is on the Login screen of the Check-In app
