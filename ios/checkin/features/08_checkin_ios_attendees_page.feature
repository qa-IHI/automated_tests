# encoding: UTF-8

@checkin_feature_attendees_page @qa_ready @8
Feature: Attendees Page

Background:
  Given I am logged out on the app
  

@8.1
 Scenario: 8.1 Add Top Gear contacts
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user adds the "Top Gear" contacts
 	Then the attendees list should include "Top Gear" contacts
 	

# Currently fails because currently we can't tell if an item is selected or not.
@8.2 @wip
Scenario: 8.2 Menu icon from Attendees
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user clicks the Menu Icon
	Then the app menu should open and "Attendees" should be highlighted


@8.3
Scenario: 8.3 Manually add contact button
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 1" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 1"
	When the user clicks the '+' button
	Then the Add an Attendee page should open


@8.4
Scenario: 8.4 Select an attendee
	Given the user is on the Login screen of the Check-In app
	And the user successfully logs in as "QA"
	Then the user is on the landing page
	When the user chooses event "Mobile Automation Event 2" on the landing page
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
	When the user selects the "first" contact
	Then the Attendee Information page for the contact will appear
	When the user clicks the X button
	Then the user will be on the attendee page for event "Mobile Automation Event 2"
