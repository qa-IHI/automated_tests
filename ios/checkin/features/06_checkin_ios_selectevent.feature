# encoding: UTF-8

@checkin_feature_select_an_event @qa_ready @6
Feature: Select an Event

Background:
  Given I am logged out on the app
  

@6.1
Scenario: 6.1 Select an event
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
 	Then the user will be on the attendee page for event "Mobile Automation Event 1"


#Currently fails because currently we can't tell if an event is selected or not.
@6.2 @wip
Scenario: 6.2 Landing page shows last event selected
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
 	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user chooses Report a Problem
 	And the user goes to the About page
	And the user goes to the Contact page
	And the user clicks the Home button
	Then the user is on the landing page
	And the "Mobile Automation Event 2" event shuld be selected


# Currently doesn't work because the Search box isn't visible to automation. The visible attribute for the search box is set to false.
@6.3 @wip
Scenario: 6.3 Search events
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user searches for the "Mobile Automation Event 2" event
 	Then only the "Mobile Automation Event 2" event will be displayed
 	When the user clears the search box
 	And the user searches for the "Mobile Auto Long Topics" event
 	Then only the "Mobile Auto Long Topics" event will be displayed


# Currently fails because currently we can't tell if an event is selected or not.
@6.4 @wip
Scenario: 6.4 Select an event collapsible arrow
	Given the user is on the Login screen of the Check-In app
 	And the user successfully logs in as "QA"
 	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 2" on the landing page
 	Then the user will be on the attendee page for event "Mobile Automation Event 2"
 	When the user clicks "open" the Select an Event collapsible arrow
 	Then the Select an Event menu should be displayed
 	And the "Mobile Automation Event 2" event shuld be selected
 	When the user clicks "close" the Select an Event collapsible arrow
 	Then the user will be on the attendee page for event "Mobile Automation Event 2"


# Currently fails because currently we can't tell if an item is selected or not.
@6.5 @wip
Scenario: 6.5 Menu icon from Select an Event
   	Given the user is on the Login screen of the Check-In app
   	And the user successfully logs in as "QA"
   	Then the user is on the landing page
 	When the user chooses event "Mobile Automation Event 1" on the landing page
 	Then the user will be on the attendee page for event "Mobile Automation Event 1"
   	When the user clicks the Menu Icon
   	Then the app menu should open and "Attendees" should be highlighted
