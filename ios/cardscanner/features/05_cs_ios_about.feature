# encoding: UTF-8

@cs_feature_about_tab @qa_ready @5
Feature: About Tab

Background:
  Given I am logged out on the app
  

@5.1
Scenario: 5.1 Home button from About
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the About page
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"


@5.2
Scenario: 5.2 Menu icon from About
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the About page
  And the user clicks the Menu Icon
  Then the app menu should open and "About" should be highlighted "eyes"


@5.3
Scenario: 5.3 The About tab displays the app version
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the About page
  Then the correct app version is displayed "eyes"


#Revisit with Eyes 3.x.
@wip @5.4
Scenario: 5.4 Click Terms and Conditions link
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the About page
  And the user clicks the Terms and Conditions link
  Then default web browser should open and display the Terms and Conditions page "eyes"
