# encoding: UTF-8

@cs_feature_search_contacts @qa_ready @9
Feature: Search Contacts

Background:
  Given I am logged out on the app
  

@9.1
Scenario: 9.1 Search for an existing contact by last name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" "Messi"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Messi"


@9.2
Scenario: 9.2 Search for an existing contact by first name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "First Name" "cri"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Ronaldo"


@9.3
Scenario: 9.3 Search for an existing contact by company name and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company Name" "barc"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Messi"


@9.4
Scenario: 9.4 Search for an existing contact by last name with special characters and then choose the contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" ")(*&^%$$##@!><?:{"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match ")(*&^%$$##@!><?:{"


@9.5
Scenario: 9.5 Search for a non-existant contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Last Name" "Pirlo"
   Then the contact with "Last Name" "Pirlo" should not be returned by the search

@9.6
Scenario: 9.6 Edit a contact and immediately search for the contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company *" "Real Madrid"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Ronaldo"
   When the user scrolls to the "Edit" field
   And the user clicks the Edit button
   Then the user should be on the Edit Contact page for "Ronaldo"
   And the user changes the "Company *" from "Real Madrid" to "Manchester United"
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   And the user waits "20" seconds
   Then the Contact page should appear "eyes"
   When the user clicks the Home button
   And the user goes to the Contacts page
   And the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user searches for the contact with "Company*" "Manchester United"
   And the user clicks the first contact
   Then the Contact page should appear "eyes"
   And the contact information for "Ronaldo" will have "Company" "Manchester United"

   