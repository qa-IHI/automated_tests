# encoding: UTF-8

@cs_feature_attendee_alerts @wip @18
Feature: Attendee Alerts

Background:
  Given I am logged out on the app
  
	
@18.1
Scenario: 18.1 Menu icon from Attendee Alerts
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
   Then the tutorial is displayed "eyes"
   When the user clears the tutorial overlay
   Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Alerts page
   Then the user should be on the Attendee Alerts page "eyes"
	When the user clicks the Menu Icon
	Then the app menu should open and "Attendee Alerts" should be highlighted "eyes"


@18.2
Scenario: 18.2 Home button from Attendee Alerts
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is logged into the app and the tutorial is displayed "noeyes"
   When the user clears the tutorial overlay
   Then the user is on the landing page "eyes"
   When the user goes to the Attendee Alerts page
   Then the user should be on the Attendee Alerts page "eyes"
   When the user clicks the Home button
   Then the user is on the landing page "noeyes"


@18.3
Scenario: 18.3 Attendee Alert event dropdown should match Scan a Card event dropdown
   Given the user is on the Login screen of the Card Scanner app "eyes"
   And the user successfully logs in as "QA"
   Then the user is logged into the app and the tutorial is displayed "noeyes"
   When the user clears the tutorial overlay
   Then the user is on the landing page "eyes"
   When the user allows the app to use location data
   And the user chooses event "Mobile Automation Event 2" on the Scan a Card page
   And the user goes to the Attendee Alerts page
   Then the user should be on the Attendee Alerts page "eyes"
   And the default Attendee Alert event dropdown should match "Mobile Automation Event 2"
   When the user clicks the Home button
   Then the user is on the landing page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Scan a Card page
   And the user goes to the Attendee Alerts page
   Then the user should be on the Attendee Alerts page "noeyes"
   And the default Attendee Alert event dropdown should match "Mobile Automation Event 1"
