# encoding: UTF-8

@cs_feature_fua @qa_ready @16 @coretests
Feature: Follow Up Actions

Background:
  Given I am logged out on the app

@16.1
Scenario: 16.1 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  #When the user allows the app to use location data
  When the user adds the "Top Gear" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"


@16.2
Scenario: 16.2 Press the X button on the Follow Up Actions page
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks the X icon
  Then the Edit Contact page should open for "May" "noeyes"
  

@16.3 @wip
Scenario: 16.3 Select and de-select a follow-up action that has no sub follow-up actions
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
	And the user searches for the contact with "Last Name" "Hammond"
	And the user clicks the first contact
	Then the Contact page should appear "eyes"
	And the contact information for the contact should match "Hammond"
	When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
	When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user selects the "Jupiter" follow-up actions
  Then the "Jupiter" follow-up actions should be "selected"
  When the user deselects the "Jupiter" follow-up actions
  Then the "Jupiter" follow-up actions should be "deselected"


@16.4 @wip
Scenario: 16.4 Select a follow-up action with sub follow-up actions
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"


@16.5 @wip
Scenario: 16.5 Select and de-select a sub follow-up action
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  When the user selects the "English, Spanish" sub follow-up actions
  Then the "English, Spanish" sub follow-up actions should be "selected"
  When the user clicks "Done"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Languages" topics should be "selected"
  When the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  And the "English, Spanish" sub follow-up actions should be "selected"
  When the user clears all sub follow-up actions
  Then all sub follow-up actions should be "deselected"
  When the user clicks "Done"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Languages" topics should be "deselected"


@16.6 @wip
Scenario: 16.6 Select a Note follow-up action
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	And the text should match "This is a test note."


@16.7 @wip
Scenario: 16.7 Edit a Note's default text and press done
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	When the user clicks "A test note"
 	And changes the text from "This is a test note." to "A reasonably priced car"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	And the "A test note" follow-up actions should be "selected"
  When the user clicks "Next"
	And the user clicks "Done"
	And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  Then the "Follow-Up Action(s)" field should contain "A test note"
  When the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	And the "A test note" follow-up actions should be "selected"


@16.8 @wip
Scenario: 16.8 Press the X icon for a note
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	When the user clicks the X icon
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	And the "A test note" follow-up actions should be "deselected"


@16.9 @wip
Scenario: 16.9 Edit follow-up action Note and use special characters
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "A test note"
	And changes the text from "This is a test note." to "!@#$%&+?/_,.()-:;*="
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	And the "A test note" follow-up actions should be "selected"
  When the user clicks "Next"
	And the user clicks "Done"
	And the user clicks the Home button
	Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "noeyes"
	When the user clicks "A test note"
	Then the Note pop-up should appear with title "A test note" "eyes"
	And the text should match "!@#$%&+?/_,.()-:;*="


@16.10 @wip
Scenario: 16.10 Add a follow-up action Note while editing a contact
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Hammond"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Mobile" from "12345678901" to "9999999999"
  And the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
  And changes the text from "Some random comments." to "Charing Cross"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Note" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Hammond"
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Topic(s)" field
  Then the "Comments" field should contain "Charing Cross"


@16.11 @wip
Scenario: 16.11 Enter more than 500 characters for a follow-up action Note
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "May"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
 	And the user clicks "Edit Follow-Up Actions"
	Then the user should be on the Edit Follow-Up Actions page "eyes"
	When the user clicks "Note"
 	And changes the text from "Some random comments." to more than 500 characters
 	Then the user will receive a "Notes have exceeded 500 maximum character limit." message


@16.12 @wip
Scenario: 16.12 De-select a follow-up action Note
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "May"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Note"
  And changes the text from "Some random comments." to "Formula E Racing"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Note" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "May"
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Topic(s)" field
  Then the "Comments" field should contain "Formula E Racing"
  When the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Clear"
  Then the "Note" follow-up actions should be "deselected"    
  When the user clicks "Next"
  And the user clicks "Done"
  Then the Contact page should appear "noeyes"
  And the "Comments" field should be empty


@16.13 @wip
Scenario: 16.13 Select and edit a Free Text follow-up action
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  Then the Free Text pop-up should appear with title "Free Text"
  And the text should match "default free text"
  When the user clicks "Done"
  And the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "May"
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "Lets drive around Monaco!"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"


@16.14 @wip
Scenario: 16.14 De-select a follow-up action Free Text
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "May"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Clear"
  Then the "Free Text" follow-up actions should be "deselected"


@16.15 @wip
Scenario: 16.15 Press the X icon for a Free Text follow-up action
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Hammond"
  Then the Contact page should appear "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the Free Text pop-up should appear with title "Free Text"
  When the user clicks the X icon
  Then the user should be on the Edit Follow-Up Actions page "noeyes"


@16.16 @wip
Scenario: 16.16 Edit follow-up action Free Text and use special characters
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Clarkson"
  Then the Contact page should appear "eyes"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "!@#$%&+?/_\"\",.()-:;*="
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Clarkson"
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the Note pop-up should appear with title "Free Text" "eyes"
  And the text should match "!@#$%&+?/_\"\",.()-:;*="


@16.17
Scenario: 16.17 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user adds the "Top Gear" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"


@16.18
Scenario: 16.18 Add Starfleet contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user adds the "Starfleet" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Starfleet" contacts "eyes"


@16.19 @wip
Scenario: 16.19 Add a follow-up action Free Text while editing a contact
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Hammond"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Topic(s)" field
  And the user presses the Edit button
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Phone" from "01234567890" to "7777777777"
  And the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "Waterloo Station"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Free Text" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Hammond"
  Then the Contact page should appear "noeyes"
  When the user scrolls to the "Topic(s)" field
  Then the "Follow-Up Action(s)" field should contain "Free Text"


# In 3.x, it is no longer a 500 char limit. Need to verify what the new char limit is.
@16.20 @wip
Scenario: 16.20 Enter more than 500 characters for a follow-up action Free Text
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Clarkson"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Edit" field
  #And the user presses the Edit button
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to more than 500 characters
  Then the user will receive a "Notes have exceeded 500 maximum character limit." message


@16.21
Scenario: 16.21 Long list of follow-up actions and sub follow-up actions
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Kirk"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Kirk"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  #And the user presses the Edit button
  Then the Edit Contact page should open for "Kirk" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  And the FUAs page should display "Catalunya, Circuit of the Americas, Daytona, Hungaroring, Laguna Seca, Melbourne, Monaco" FUAs in alpha order
  When the user scrolls to the bottom of the FUA list
  Then the FUAs page should display "Mont Tremblant, Nurburgring, Silverstone, Spa, Suzuka, Texas Motor Speedway, Top Gear Track" FUAs in alpha order
  When the user scrolls to the bottom of the FUA list
  Then the FUAs page should display "Nurburgring, Silverstone, Spa, Suzuka, Texas Motor Speedway, Top Gear Track, Zandvoort" FUAs in alpha order
  When the user clicks the X icon
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Monaco"
  Then the sub follow-up actions pop up should appear
  And the sub follow-up actions page should display "A, B, Z, F, D, G, V, t" sub follow-up actions in alpha order
  When the user scrolls to the bottom of the sub-FUA list
  Then the sub follow-up actions page should display "G, V, t, S, m, o, n, q" sub follow-up actions in alpha order


@16.22 @wip
Scenario: 16.22 Follow-up actions clear button
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "May"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "May"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  #And the user presses the Edit button
  Then the Edit Contact page should open for "May" "noeyes"
  And the user scrolls to the "Done" field
  When the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user selects the "Jupiter, Mars, Pluto" follow-up actions
  And the user selects the "Languages" follow-up actions
  Then the sub follow-up actions should open with "English, French, German, Spanish"
  When the user selects the "English, Spanish" sub follow-up actions
  And the user clicks "Done"
  Then the "Jupiter, Mars, Pluto, Languages" follow-up actions should be "selected"
  When the user clicks "Clear"
  Then all follow-up actions should be "deselected"


@16.23 @wip
Scenario: 16.23 Verify follow-up actions for a new contact
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user chooses contact "Clarkson"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  #And the user presses the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"  
  And the "Mars, Pluto" follow-up actions should be "selected"


@16.24 @wip
Scenario: 16.24 Add new contact with follow-up actions but no topics
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user adds the "F1" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user chooses contact "Rosberg"
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Rosberg"
  When the user scrolls to the "Edit" field
  And the user clicks "Edit"
  #And the user presses the Edit button
  Then the Edit Contact page should open for "Rosberg" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Mars, Jupiter" follow-up actions should be "selected"
    
