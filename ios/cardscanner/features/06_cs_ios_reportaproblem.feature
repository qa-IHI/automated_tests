# encoding: UTF-8

@cs_feature_report_a_problem @6
Feature: Report a Problem

Background:
  Given I am logged out on the app
  

@6.1
Scenario: 6.1 User selects Report a Problem
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to Report a Problem
  Then the To field should be "support@at-event.com"
  And the default text should be "Describe your problem here:" "eyes"


@6.2
Scenario: 6.2 User sends Report a Problem email
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to Report a Problem
  And the user changes the To field to personal email "kris.huynh@gmail.com"
  And the user clicks "Send"
  Then the email should be sent
