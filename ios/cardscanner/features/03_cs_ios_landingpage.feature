# encoding: UTF-8

@cs_feature_landing_page @qa_ready @3 @coretests
Feature: Landing Page

Background:
  Given I am logged out on the app

  
@3.1
Scenario: 3.1 Event Selection
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user navigates to other pages from Scan & Check-In
   And navigates back to the Scan & Check-In page
   Then the Scan & Check-In page should open "noeyes"
   And the chosen Event should be "Mobile Automation Event 1"


# @wip @3.2
# Scenario: 3.2 Tutorial
#    Given the user is on the Login screen of the Card Scanner app "noeyes"
#    When the user successfully logs in as "QA"
#    Then the user is on the Select an Event page "noeyes"
#    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
#    Then the tutorial is displayed "eyes"
#    When the user clears the tutorial overlay
#    Then the Scan & Check-In page should open "noeyes"
#    When the user clicks the tutorial question mark on the Scan & Check-In page
#    Then the tutorial overlay should be opened "eyes"
#    When the user clears the tutorial overlay
#    Then the Scan & Check-In page should open "noeyes"
#    When the user logs out of the Card Scanner app
#    Then the user is on the Login screen of the Card Scanner app "noeyes"


@3.3
Scenario: 3.3 Menu icon from Scan & Check-In
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Scan & Check-In page
   When the user clicks the Menu Icon
   Then the app menu should open and "Scan & Check-In" should be highlighted "eyes"


@3.4
Scenario: 3.4 Add contact manually button '+' from Scan & Check-In
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Scan & Check-In page
   And the user clicks the '+' button
   Then the Add Contact page should open "eyes"
   