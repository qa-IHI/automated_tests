# encoding: UTF-8

@cs_feature_topics @qa_ready @15 @coretests
Feature: Topics

Background:
  Given I am logged out on the app
  

@15.1
Scenario: 15.1 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  #When the user allows the app to use location data
  When the user adds the "Top Gear" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"


@15.2
Scenario: 15.2 Press the X button on the Topics page
  	Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
  	And the user searches for the contact with "Last Name" "Clarkson"
  	And the user clicks the first contact
  	Then the Contact page should appear "eyes"
  	And the contact information for the contact should match "Clarkson"
    When the user scrolls to the "Edit" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Clarkson" "noeyes"
  	When the user scrolls to the "Done" field
  	And the user clicks "Edit Topics"
  	Then the user should be on the Topics page "eyes"
  	When the user clicks the X icon
  	Then the user should go back to the Edit Contact page


# Can't tell if Topic/FUA is selected or not.
@15.3 @wip
Scenario: 15.3 Select and de-select a topic that has no sub-topics
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "May"
    When the user scrolls to the "Edit" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "May" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Android" topics
    Then the "Android" topics should be "selected"
    When the user deselects the "Android" topics
    Then the "Android" topics should be "deselected"


# Can't tell if Topic/FUA is selected or not.
@15.4 @wip
Scenario: 15.4 Select a topic that has sub-topics
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "May"
    When the user scrolls to the "Topic(s)" field
    And the user presses the Edit button
    Then the Edit Contact page should open for "May" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Goalkeeping" topics
    Then the sub-topics should open with "Long Kick, Punch, Throw In" sub-topics


# Can't tell if Topic/FUA is selected or not.
@15.5 @wip
Scenario: 15.5 Select and de-select a sub-topic
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "May"
    When the user scrolls to the "Topic(s)" field
    And the user presses the Edit button
    Then the Edit Contact page should open for "May" "noeyes"
    And the user scrolls to the "Done" field
    When the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Corner Kicks" topics
    Then the sub-topics should open with "Center, Left, Right" sub-topics
    When the user selects the "Left" subtopics
    Then the "Left" subtopics should be "selected"
    When the user deselects the "Left" subtopics
    Then the "Left" subtopics should be "deselected"


@15.6
Scenario: 15.6 Add Starfleet contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  #When the user allows the app to use location data
  When the user adds the "Starfleet" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Starfleet" contacts "eyes"


# Can't tell if Topic/FUA is selected or not.
@15.7 @wip
Scenario: 15.7 Long list of topics and sub-topics
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Auto Long Topics" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "McCoy"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "McCoy"
    When the user scrolls to the "Topic(s)" field
    And the user presses the Edit button
    Then the Edit Contact page should open for "McCoy" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    And the Topics page should display "Alfa Romeo, Aston Martin, Benetton, Brabham, Caterham, Ferrari, Force India, Honda" topics in alpha order
    When the user scrolls to the bottom of the Topics list
    Then the Topics page should display "Honda, Lancia, Lotus, Marussia, McLaren, Mercedes, Porsche, Red Bull Racing" topics in alpha order
    When the user scrolls to the bottom of the Topics list
    Then the Topics page should display "Marussia, McLaren, Mercedes, Porsche, Red Bull Racing, Sauber, Toro Rosso, Williams" topics in alpha order
    When the user clicks "Williams"
    Then the sub-topics pop up should appear "eyes"
    And the sub-topics page should display "A, B, C, D, E, F, G, H" sub-topics in alpha order
    When the user scrolls to the bottom of the Sub-Topics list
    Then the sub-topics page should display "J, M, N, O, P, S, T, U" sub-topics in alpha order
    When the user scrolls to the bottom of the Sub-Topics list
    Then the sub-topics page should display "N, O, P, S, T, U, X, Z" sub-topics in alpha order


# Can't tell if Topic/FUA is selected or not.
@15.8 @wip
  Scenario: 15.8 Topics clear button
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 1" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "May"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "May"
    When the user scrolls to the "Topic(s)" field
    And the user presses the Edit button
    Then the Edit Contact page should open for "May" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user selects the "Android" topics
    Then the "Android" topics should be "selected"
    When the user selects the "Checkin" topics
    Then the "Checkin" topics should be "selected"
    When the user selects the "iOS" topics
    Then the "iOS" topics should be "selected"
    When the user clicks "Clear"
    Then all topics should be de-selected


@15.9 @wip
Scenario: 15.9 Add new contact with topics but no follow up actions
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    And the user selects the "All" tab on the Contacts page "noeyes"
    And the user selects All Events
    And the user deletes existing contacts
    And the user clicks the Home button
    Then the Scan & Check-In page should open "noeyes"
    When the user adds the "F1" contacts
    When the user navigates to Contacts
    And the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Hamilton"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "Hamilton"
    When the user scrolls to the "Topic(s)" field
    And the user presses the Edit button
    Then the Edit Contact page should open for "Hamilton" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    And the "Python, Ruby" topics should be "selected"


@15.10
Scenario: 15.10 Click the Topics page Next button
    Given the user is on the Login screen of the Card Scanner app "noeyes"
    When the user successfully logs in as "QA"
    Then the user is on the Select an Event page "noeyes"
    When the user chooses event "Mobile Automation Event 2" on the Select an Event page
    Then the Scan & Check-In page should open "noeyes"
    When the user goes to the Contacts page
    Then the user should be on the Contacts page "eyes"
    When the user selects the "All" tab on the Contacts page "eyes"
    And the user selects All Events
    And the user searches for the contact with "Last Name" "Clarkson"
    And the user clicks the first contact
    Then the Contact page should appear "eyes"
    And the contact information for the contact should match "Clarkson"
    When the user scrolls to the "Edit" field
    And the user clicks "Edit"
    Then the Edit Contact page should open for "Clarkson" "noeyes"
    When the user scrolls to the "Done" field
    And the user clicks "Edit Topics"
    Then the user should be on the Topics page "eyes"
    When the user clicks "Next"
    Then the user should be on the Edit Follow-Up Actions page "eyes"

  