# encoding: UTF-8

@cs_feature_filter_contacts @qa_ready @11
Feature: Filter Contacts

Background:
  Given I am logged out on the app
  

@11.1
Scenario: 11.1 Use the Filter button on the Contacts page
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
	And the user clicks "Filter"
	Then the Filter Contacts page should appear "eyes"

@11.2
Scenario: 11.2 Choose a contact after filtering contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Company" named "FC Barcelona"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  When the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Messi"

@11.3
Scenario: 11.3 Filter contacts based on Company for All Events
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Company" named "FC Barcelona"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi" based on "Company"
  When the user clicks "Filter (1)"
  And the user filters by the "Company" named "¿¡ÀÈÌÒÙÇÃÑÕ"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Company"
  When the user clicks "Filter (2)"
  And the user filters by the "Company" named ")(*&^%$$##@!><?:{"
  And the user applies the filter
  Then the Filter button will show "3" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{" based on "Company"
  When the user clicks "Filter (3)"
  And the user filters by the "Company" named "Manchester United"
  And the user applies the filter
  Then the Filter button will show "4" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{, Ronaldo" based on "Company"

@11.4
Scenario: 11.4 Filter contacts based on Company for a single event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 1" event on the Contact page
  And the user clicks "Filter"
  And the user filters by the "Company" named "FC Barcelona"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "Messi" based on "Company"
  When the user clicks "Filter (1)"
  And the user filters by the "Company" named "¿¡ÀÈÌÒÙÇÃÑÕ"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Company"
  # When the user clicks "Filter (2)"
  # And the user filters by the "Company" named ")(*&^%$$##@!><?:{"
  # And the user applies the filter
  # Then the Filter button will show "3" filter applied
  # And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Company"

@11.5
Scenario: 11.5 Filter contacts based on Title for All Events
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Title" named "Football Player"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, Ronaldo" based on "Title"
  And the user clicks "Filter (1)"
  And the user filters by the "Title" named "Alien"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, Ronaldo, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Title"
  And the user clicks "Filter (2)"
  And the user filters by the "Title" named "Robot"
  And the user applies the filter
  Then the Filter button will show "3" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, Ronaldo, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{" based on "Title"

@11.6
Scenario: 11.6 Filter contacts based on Title for a single event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 1" event on the Contact page
  And the user clicks "Filter"
  And the user filters by the "Title" named "Football Player"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "Messi" based on "Title"
  And the user clicks "Filter (1)"
  And the user filters by the "Title" named "Alien"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Title"

@11.7
Scenario: 11.7 Filter contacts based on Event for All Events
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Event" named "Mobile Automation Event 2"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Ronaldo, )(*&^%$$##@!><?:{" based on "Event"
  And the user clicks "Filter (1)"
  And the user filters by the "Event" named "Mobile Automation Event 1"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Messi, Ronaldo, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{" based on "Event"


@11.8
Scenario: 11.8 Filter contacts based on Event for a single event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 1" event on the Contact page
  And the user clicks "Filter"
  And the user filters by the "Event" named "Mobile Automation Event 1"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Event"
  And the user clicks "Filter (1)"
  And the user filters by the "Event" named "Mobile Automation Event 2"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the Contacts list should contain contact(s) "Messi, Ronaldo, ¿¡ÀÈÌÒÙÇÃÑÕ, )(*&^%$$##@!><?:{" based on "Event"

@11.9
Scenario: 11.9 Filter contacts based on Topics for All Events
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Topics" named "iOS"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ" based on "Topics"
  And the user clicks "Filter (1)"
  And the user filters by the "Topics" named "Android"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ, Messi, Ronaldo" based on "Topics"
  And the user clicks "Filter (2)"
  And the user filters by the "Topics" named "Python"
  And the user applies the filter
  Then the Filter button will show "3" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ, Messi, Ronaldo" based on "Topics"

@11.10
Scenario: 11.10 Filter contacts based on Topics for a single event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 1" event on the Contact page
  And the user clicks "Filter"
  And the user filters by the "Topics" named "iOS"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ" based on "Topics"
  And the user clicks "Filter (1)"
  And the user filters by the "Topics" named "Android"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ, Messi" based on "Topics"
  # And the user clicks "Filter (2)"
  # And the user filters by the "Topics" named "Python"
  # And the user applies the filter
  # Then the Filter button will show "3" filter applied
  # And the Contacts list should contain contact(s) "¿¡ÀÈÌÒÙÇÃÑÕ, Messi, Ronaldo, )(*&^%$$##@!><?:{" based on "Topics"

@11.11
Scenario: 11.11 Filter contacts based on Follow Up Actions for All Events
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Follow Up Actions" named "Pluto"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Ronaldo" based on "Follow Up Actions"
  And the user clicks "Filter (1)"
  And the user filters by the "Follow Up Actions" named "Darth Vader"
  And the user applies the filter
  Then the Filter button will show "2" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Ronaldo, Messi" based on "Follow Up Actions"
  And the user clicks "Filter (2)"
  And the user filters by the "Follow Up Actions" named "Yoda"
  And the user applies the filter
  Then the Filter button will show "3" filter applied
  #And the "All" tab will indicate the correct number of contacts
  And the Contacts list should contain contact(s) "Ronaldo, Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Follow Up Actions"

@11.12
Scenario: 11.12 Filter contacts based on Follow Up Actions for a single event
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects the "Mobile Automation Event 2" event on the Contact page
  And the user clicks "Filter"
  And the user filters by the "Follow Up Actions" named "Pluto"
  And the user applies the filter
  Then the Filter button will show "1" filter applied
  And the Contacts list should contain contact(s) "Ronaldo" based on "Follow Up Actions"
  # And the user clicks "Filter (1)"
  # And the user filters by the "Follow Up Actions" named "Darth Vader"
  # And the user applies the filter
  # Then the Filter button will show "2" filter applied
  # And the Contacts list should contain contact(s) "Ronaldo, Messi" based on "Follow Up Actions"
  # And the user clicks "Filter (2)"
  # And the user filters by the "Follow Up Actions" named "Yoda"
  # And the user applies the filter
  # Then the Filter button will show "3" filter applied
  # And the Contacts list should contain contact(s) "Ronaldo, Messi, ¿¡ÀÈÌÒÙÇÃÑÕ" based on "Follow Up Actions"

@11.13
Scenario: 11.13 Selected filters should be highlighted green and Filter button will indicate number of filters applied
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Company" named "FC Barcelona"
  And the user filters by the "Event" named "Mobile Automation Event 2"
  And the user filters by the "Follow Up Actions" named "Jupiter"
  Then "Company" should be highlighted
  And "Event" should be highlighted
  And "Follow Up Actions" should be highlighted
  When the user applies the filter
  Then the Filter button will show "3" filter applied

# Cannot tell if item is highlighted.
@11.14 @wip
Scenario: 11.14 Clear filters
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks "Filter"
  And the user filters by the "Title" named "Football Player"
  And the user filters by the "Topics" named "Python"
  Then "Title" should be highlighted
  And "Topics" should be highlighted
  When the user applies the filter
  Then the Filter button will show "2" filter applied
  When the user clicks "Filter (2)"
  And the user clears all filters
  Then no filters should be highlighted
  When the user applies the filter
  Then the Filter button will show no filters applied
