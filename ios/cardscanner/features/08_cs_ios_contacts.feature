# encoding: UTF-8

@cs_feature_contacts @qa_ready @8
Feature: Contacts

Background:
  Given I am logged out on the app
  

@8.1
Scenario: 8.1 Menu icon from Contacts
	Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user clicks the Menu Icon
   Then the app menu should open and "Contacts" should be highlighted "eyes"


@8.2
Scenario: 8.2 Home button from Contacts
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   And the user clicks the Home button
   Then the Scan & Check-In page should open "noeyes"


@8.3
Scenario: 8.3 Choose a contact on the Contact List
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   And the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user clicks the contact "Ronaldo"
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Ronaldo"
