# encoding: UTF-8

@cs_feature_contact_info_page @qa_ready @13
Feature: Contact Info Page

Background:
  Given I am logged out on the app
  

@13.1
Scenario: 13.1 Home button from Contact Info page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  When the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"


@13.2
Scenario: 13.2 Back button from Contact Info page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  When the user clicks the Back button
  Then the user should be on the Contacts page "eyes"


@13.3
Scenario: 13.3 Enlarge contact business card image
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "komack" "eyes"


# Pinch and zoom are currently not implemented in Card Scanner
@wip @13.4
Scenario: 13.4 View a business card in full screen and zoom in
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "komack" "eyes"
  When the user "zooms" the business card
  #Then the business card should zoom in


@13.5
Scenario: 13.5 View a business card in full screen and press the X icon
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "veil"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "veil" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "veil" "eyes"
  When the user clicks the X icon
  Then the contact page should contain a business card image for "veil" "eyes"


@13.6
Scenario: 13.6 Click edit on the contact info page
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "atEvent General Event" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "veil"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "veil" "eyes"
  When the user scrolls to the "Topic(s)" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "veil" "eyes"

