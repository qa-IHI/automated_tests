# Setup section that executes before each test scenario.
# Set the Appium test capabilities.
# Start the Appium driver.
Before do |scenario|
    @app_name = $test["app_name"]# + "-" + $test["app_version"]
    @scenario_name = "iPhone Card Scanner: " + scenario.name

    # Use an Appium lib object
    def desired_capabilities
    {  caps:
        {
            #appiumVersion:      $appium_version,
            platformName:       $caps["platformName"],
            platformVersion:    $ios_version,
            deviceName:         $ios_device,
            #orientation:        $caps["orientation"],
            sendKeyStrategy:    $caps["sendKeyStrategy"],
            app:                $app_location,
            #udid:               $udid,
            #xcodeConfigFile:    "/Users/kaeris/Developer/git/new/automated_tests/ios/cardscanner/features/support/myconfig.xcconfig",
            #name:               @scenario_name,
            #autoAcceptAlerts:   true, 

            # Wait for the notifications dialog to appear. Must place a 3 sec sleep in the source code to wait for automation framework to load.
            waitForAppScript:   '$.delay(4000); UIATarget.localTarget().frontMostApp().alert(); $.acceptAlert(); true',
            #language:           "es_MX",
            #locale:             "es_MX",
            noReset:             true,
            fullReset:           false,
            useNewWDA:           true,  
            automationName:      "XCUITest",
            newCommandTimeout:   120000, 
        },
        appium_lib:
        {
            server_url:         $appium_url
        }
    }
    end

    @driver = Appium::Driver.new(desired_capabilities)
	@driver.start_driver
    $job_id = @driver.session_id


# Use a Selenium Webdriver object
#     capabilities = {
#         'platformName' => 'iOS',
#         'platformVersion' => '9.3',
#         'app' => '/Users/kaeris/Developer/card_scanner_builds/x86_64/ios/2.1.14.20/AtEvent-2.1.14.20.zip',
#         'deviceName' => 'iPhone 6',
#         'sendKeyStrategy' => 'setValue',
#         'waitForAppScript' => '$.delay(4000); UIATarget.localTarget().frontMostApp().alert(); $.acceptAlert(); true'
# }
#     @driver = Selenium::WebDriver.for   :remote,
#                                         :url => 'http://127.0.0.1:4723/wd/hub',
#                                         :desired_capabilities => capabilities 

    if $eyes_enabled == "true"
        # Set up Applitools Eyes object for visual validation.
        # Applitools namespace with 2.x
        #@eyes = Applitools::Eyes.new

        # Applitools namespace with 3.x
        @eyes = Applitools::Selenium::Eyes.new
        @eyes.api_key = $eyes_key
    

        # Enable eyes logging to STDOUT. Uncomment this line to see the Applitools log in STDOUT.
        #@eyes.log_handler = Logger.new(STDOUT)

        # Aggregate all test scenario validation points into it's own batch on the Applitools Eyes dashboard.
        # Applitools namespace with 2.x
        #@eyes_batch = Applitools::Base::BatchInfo.new(@scenario_name)

        # Applitools namespace with 3.x
        @eyes_batch = Applitools::BatchInfo.new(@scenario_name)
    end

    # Create a new hash map for the names of fields in the app. We want to keep track of the field names in case they change during a session.
    $fields_hash = Hash.new
    $fields_hash["login_email"]     = "Email Address"
    $fields_hash["login_password"]  = "Password"
    $fields_hash["cleared_overlay"] = false
    $fields_hash["model"]           = $caps["deviceName"]

    case $fields_hash["model"]
    when "iPhone 6"
        model = "iphone6"
    when "iPhone 6 Plus"
        model = "iphone6plus"
    else
        model = "unknown"
        #puts "Unable to test on #{$caps["deviceName"]} right now. Exiting."
        #exit 1
    end
        
    $fields_hash["bcard_small_x"]       = $test["#{model}_bcard_small_x"].to_f
    $fields_hash["bcard_small_y"]       = $test["#{model}_bcard_small_y"].to_f
    $fields_hash["bcard_small_height"]  = $test["#{model}_bcard_small_height"].to_f
    $fields_hash["bcard_small_width"]   = $test["#{model}_bcard_small_width"].to_f

    @deleted_contacts = Array.new(10)
    #@driver.set_wait(60)
end

# Teardown section that executes after each test scenario.
# Report pass or fail to Sauce Labs.
# Stop the Appium driver.
# Clear the hash.
After do |scenario|
    if ENV['SERVER'] == "sauce"
        if ENV['SAUCE_USERNAME'] && !ENV['SAUCE_USERNAME'].empty? && ENV['SAUCE_ACCESS_KEY'] && !ENV['SAUCE_ACCESS_KEY'].empty?
            passed = !(scenario.failed?)
            SauceWhisk::Jobs.change_status($job_id, passed)
        end
    end

	@driver.driver_quit
    #@driver.quit

    if $eyes_enabled == "true"
        @eyes.abort_if_not_closed
    end
    
    $fields_hash.clear
end

def element_exists_by_id?(my_id)
    if @driver.find_element(:id, my_id).size == 0
        false
    else
        true
    end
end

# Check if an element exists and is displayed. Return true or false.
def element_exists_by_name?(my_name)
    if my_name =~ /\w/
        begin
            element = @driver.find_element(:id, my_name)
            true if element
        rescue
            false
        end
    else
        return false
    end
end

def all_elements_visible?(list)
    for element in list do
        @driver.wait { @driver.find_element(:id, element).displayed? }
        #@driver.find_element(:id, element).displayed?
    end
end

def enter_valid_email(email)
    #email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }

    email_element = @driver.wait { @driver.find_element(:accessibility_id, 'Email Address') }
    email_element.click
    email_element.clear
    #email_element.type email
    email_element.send_keys email 
    $fields_hash["login_email"] = email
end

def logout
    click_menu_button
    logout_element = @driver.wait { @driver.find_element(:id, "Logout") }
    logout_element.click
end

def enter_valid_password(password)
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    #pw_element = @driver.wait { @driver.find_element(:accessibility_id, 'Password') }
    pw_element.send_keys password
    #pw_element.type password
end

def enter_invalid_email
    email_element = @driver.wait { @driver.find_element(:id, "Email Address") }
    #email_element.type "lio@messi.com"
    email_element.send_keys "lio@messi.com"
    $fields_hash["login_email"] = "lio@messi.com"
end


def enter_invalid_reset_email
    #email_element = @driver.wait { @driver.find_element(:id, "Email Address") }
    # Temporarily using xpath because the Email Address doesn't have a name or label. Open Jira ticket.
    #email_element = @driver.wait { @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIATextField[2]") }
    email_element = @driver.wait { @driver.find_element(:accessibility_id, "Forgot Password Email") }

    #email_element.type "lio@messi.com"
    email_element.send_keys "lio@messi.com"
    $fields_hash["login_email"] = "lio@messi.com"
end


def enter_valid_reset_email(email)
    #email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }

    #email_element = @driver.wait { @driver.find_element(:accessibility_id, 'Email Address') }

    # Temporarily using xpath because the Email Address doesn't have a name or label. Open Jira ticket.
    #email_element = @driver.wait { @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIATextField[2]") }
    email_element = @driver.wait { @driver.find_element(:accessibility_id, "Forgot Password Email") }

    #email_element.type email
    email_element.send_keys email
    $fields_hash["login_email"] = email
end


def enter_empty_email
    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    #email_element.type $test[""]
    email_element.send_keys $test[""]
    click_keyboard_next
    click_keyboard_done
end

def enter_empty_password
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    #pw_element.type ""
    pw_element.send_keys ""
    click_keyboard_done
end

def enter_invalid_email_format
    email_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_email"]) }
    #email_element.type "$$$$$$$$$$$$$"
    email_element.send_keys "$$$$$$$$$$$$$"
    $fields_hash["login_email"] = "$$$$$$$$$$$$$"
    click_keyboard_next
    click_keyboard_done
end

def enter_invalid_password
    pw_element = @driver.wait { @driver.find_element(:id, $fields_hash["login_password"]) }
    #pw_element.type "futbolllll"
    pw_element.send_keys "futbolllll"
    $fields_hash["login_password"] = "futbolllll"
    click_keyboard_done
end

def enter_contact_field(field, value)
    field_element = @driver.wait { @driver.find_element(:id, field) }
    #field_element.type value
    if field_element
        field_element.send_keys value
    else
        raise "Error: Could not find the #{field} field."
    end

    return_element = @driver.wait { @driver.find_element(:id, "Done") }
    #return_element = @driver.wait { @driver.find_element(:xpath, "(//XCUIElementTypeButton[@name=\"Done\"])[2]") }

    return_element.click
    #sleep 1
end

def enter_requesttrial_field(field, value)
    field_element = @driver.wait { @driver.find_element(:id, field) }
    field_element.click
    #field_element.type value
    field_element.send_keys value
end

def check_new_contact_added(name)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(name)
    click_contacts_tab("All")
    @driver.wait { @driver.find_element(:id, fullname).displayed? }
    @driver.wait { @driver.find_element(:id, company).displayed? }
end


def enter_new_contact_invalid_email
    email_element = @driver.wait { @driver.find_element(:id, "Email *") }
    #email_element.type "$$$$$$$$$$$$$"
    email_element.send_keys "$$$$$$$$$$$$$"

    return_element = @driver.wait { @driver.find_element(:id, "Done") }
    #return_element = @driver.wait { @driver.find_element(:xpath, "(//XCUIElementTypeButton[@name=\"Done\"])[2]") }
    return_element.click
    sleep 1
end

def add_new_contact(name)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(name)

    enter_contact_field("First Name *", $test["#{contact}_fname"])
    enter_contact_field("Last Name *", $test["#{contact}_lname"])
    enter_contact_field("Email *", $test["#{contact}_email"])
    enter_contact_field("Company *", $test["#{contact}_company"])
    enter_contact_field("Office Address", $test["contact_street"])
    enter_contact_field("Mobile", $test["contact_mobile"])
    enter_contact_field("Zip Code", $test["contact_zip"])
    enter_contact_field("City", $test["contact_city"])
    enter_contact_field("State", $test["contact_state"])
    enter_contact_field("Country", $test["contact_country"])
    enter_contact_field("Phone", $test["contact_phone"])
    enter_contact_field("Title", $test["#{contact}_job_title"])
    enter_contact_field("Comments", $test["contact_comments"])
    enter_contact_field("Website", $test["contact_web"])
end

def delete_contacts(number)
    #@deleted_contacts = Array.new(10)
    for i in 0..(number.to_i - 1)
        contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        contacts = contact_list.find_elements(:class, "XCUIElementTypeCell")
        
        first_contact = contacts[0]
        person = first_contact.find_elements(:class, "XCUIElementTypeStaticText")[0]
        @deleted_contacts.push(person.name)

        
        #@driver.swipe(:start_x => 286, :start_y => 371, :offset_x => -228, :offset_y => 0, :touchCount => 1, :duration => 500)
        @driver.drag_from_to_for_duration(:from_x => 286, :from_y => 371, :to_x => 58, :to_y => 371, :duration => 1)
        wait_for_error("Delete Contact")
        click_button("Delete")
        sleep 8
    end
end

# Delete contacts until the contacts list is empty.
def delete_all_contacts    
    while !element_exists_by_name?("ZeroContactsAll") do       
        #contact1 = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
        contact1 = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]")
        #contact1 = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
        person = contact1.find_elements(:class, "XCUIElementTypeStaticText")[0]
        @deleted_contacts.push(person.name)
        @driver.drag_from_to_for_duration(:from_x => 286, :from_y => 371, :to_x => 58, :to_y => 371, :duration => 1)
        wait_for_error("Delete Contact")
        click_button("Delete")
        sleep 5
    end
end


def verify_contacts_deleted(number)
    for i in 0..(number.to_i - 1)
        fullname = @deleted_contacts.pop
        @deleted_contacts.push(fullname)

        if element_exists_by_name?(fullname)
            raise "Error: Expected contact #{fullname} to have been deleted."
        end
    end
end

def top_of_contact_list?(first)
    click_contacts_tab("All")
    first_contact = @driver.wait { @driver.ele_index(:UIATableCell, 5) }
    #second_contact = @driver.wait { @driver.ele_index(:UIATableCell, 6) }
    #third_contact = @driver.wait { @driver.ele_index(:UIATableCell, 7) }
    #fourth_contact = @driver.wait { @driver.ele_index(:UIATableCell, 8) }
    if first_contact.name != first
        raise "Error: New contact should be at the top of the list."
    end
end

def scroll_to(field)
    @driver.scroll(:direction => 'down', name: field)

    # el = @driver.find_element(:accessibility_id, field)
    # #@driver.execute_script("mobile: scrollTo", :element => el.ref)

    # if el
    #     @driver.execute_script "mobile: scroll", :direction => 'down'
    # else
    #     raise "Error: Could not find the #{field} field."
    # end
end


def scroll_to_contacts_bottom
    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 359, :y => 513).move_to(:x => 0, :y => -392).release.perform
end

def click_button(name)
    #sleep 2
    button = @driver.wait { @driver.find_element(:accessibility_id, name) } 
    button.click
end

def keep_me_logged_in(choice)
    begin
        @driver.find_element(:id, "checkbox checked").displayed?
        checkbox_state = "checked"
    rescue
        checkbox_state = "unchecked"
    end

    if choice == true && checkbox_state == "unchecked"
        @driver.find_element(:id, "checkbox").click
    elsif choice == false && checkbox_state == "checked"
        @driver.find_element(:id, "checkbox checked").click
    end
end

def accept_terms_of_service(choice)
    if choice == true
        if @driver.wait { @driver.find_element(:id, "checkbox") }
            checkbox_element = @driver.find_element(:id, "checkbox")
            checkbox_element.click
        end
    elsif choice == false
        if @driver.wait { @driver.find_element(:id, "checkbox checked") }
            checkbox_element = @driver.find_element(:id, "checkbox checked")
            checkbox_element.click
        end
    end
end

def keep_me_logged_in_checked?
    @driver.wait { @driver.find_element(:id, "checkbox checked").displayed? }
end

def agree_terms_of_service_unchecked?
    @driver.wait { @driver.find_element(:id, "checkbox").displayed? }
end

def click_home_button
    sleep 4
    button_element = @driver.wait { @driver.find_element(:id, "btn home") }
    button_element.click
end

def click_menu_button
    sleep 3

    # button_element = @driver.wait { @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIAButton[2]") }
    # if button_element.location.x != 0
    #     button_element = @driver.wait { @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIAButton[1]") }
    # end
    # button_element.click

    menu = Appium::TouchAction.new
    menu.press(x: 37, y:58).move_to(x: 37, y:58).release.perform
    #menu.tap(x: 37, y:58, count: 3).perform
end

def click_add_contact_back_button
    #button_element = @driver.wait { @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIAButton[1]") }
    #button_element.click

    back = Appium::TouchAction.new
    back.press(x: 36, y:57).move_to(x: 36, y:57).release.perform

    #back.tap(x: 36, y:57, count: 3).perform
end

def find_tutorial_overlay
    @driver.wait { @driver.find_element(:id, "scannertips").displayed? }
end

def clear_tutorial_overlay
    #if $fields_hash["cleared_overlay"] == false
        sleep 2
        #tips_element = @driver.wait { @driver.find_element(:id, "scannertips.png") }
        #tips_element = @driver.wait { @driver.find_element(:id, "btn orange barcode") }
        #tips_element.click
        #@driver.execute_script('mobile: tap', :x => 150, :y => 156)
        tutorial = Appium::TouchAction.new
        tutorial.press(:x => 195, :y => 289).move_to(:x => 195, :y => 289).release.perform
        $fields_hash["cleared_overlay"] = true
    #end
end

def error_request_trial_page_check
    sleep 3
    @driver.wait { @driver.find_element(:id, "cs_featureBadge").displayed? }
    @driver.wait { @driver.find_element(:id, "ateventBanner.png").displayed? }
    @driver.wait { @driver.find_element(:id, "Request a Trial").displayed? }
    @driver.wait { @driver.find_element(:id, "Submit").displayed? }
    @driver.wait { @driver.find_element(:id, "Terms of Service").displayed? }
    @driver.wait { @driver.find_element(:id, "ci btnCloseX").displayed? }
end

def request_trial_page_check
    sleep 3
    @driver.wait { @driver.find_element(:id, "cs_featureBadge").displayed? }
    @driver.wait { @driver.find_element(:id, "ateventBanner.png").displayed? }
    @driver.wait { @driver.find_element(:id, "Request a Trial").displayed? }
    @driver.wait { @driver.find_element(:id, "First Name").displayed? }
    @driver.wait { @driver.find_element(:id, "Last Name").displayed? }
    @driver.wait { @driver.find_element(:id, "Company").displayed? }
    @driver.wait { @driver.find_element(:id, "Phone").displayed? }
    @driver.wait { @driver.find_element(:id, "Email").displayed? }
    @driver.wait { @driver.find_element(:id, "Submit").displayed? }
    @driver.wait { @driver.find_element(:id, "Terms of Service").displayed? }
    @driver.wait { @driver.find_element(:id, "ci btnCloseX").displayed? }
end

def scan_and_checkin_page_check
    # sleep 7
    # if @driver.find_element(:accessibility_id, "Keep me logged in").displayed?
    #     raise "Error: Shouldn't see Keep me logged in on the landing page."
    # end

    # @driver.wait { @driver.find_element(:name, "atEventTopNav.png").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "Welcome to").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "btn orange card").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "btn orange barcode").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "btn orange addContact").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "hintBlackCircle").displayed? }
    # @driver.wait { @driver.find_element(:accessibility_id, "checkInTabUp").displayed? }
end

def select_event_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "Select an event or occasion").displayed? }
    @driver.wait { @driver.find_element(:name, "atEventTopNav.png").displayed? }
end

def password_reset_page_check
    sleep 3
    @driver.wait { @driver.find_element(:id, "logoLogin.png").displayed? }
    @driver.wait { @driver.find_element(:id, "Email Address").displayed? }
    @driver.wait { @driver.find_element(:id, "Cancel").displayed? }
    @driver.wait { @driver.find_element(:id, "Send").displayed? }
end

def add_contact_page_check
    sleep 3
    #@driver.wait { @driver.find_element(:id, "hdr_addContact.png").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
end

def edit_topics_page_check 
    sleep 5
    @driver.wait { @driver.find_element(:id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:id, "Topics").displayed? }
    @driver.wait { @driver.find_element(:id, "Next").displayed? }
    @driver.wait { @driver.find_element(:id, "Clear").displayed? }
end

def edit_followup_actions_page_check
    sleep 6
    @driver.wait { @driver.find_element(:accessibility_id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Follow Up Actions").displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "Next").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Clear").displayed? }
end

def contacts_page_check
    sleep 10
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:id, "hdr_contacts.png").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Select an event or occasion").displayed? }
    @driver.wait { @driver.find_element(:id, "Search").displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "Filter").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Sort").displayed? }
    
    # Verify the contacts tabs (TODAY, ALL, PENDING) exist.
    @driver.wait { @driver.find_element(:accessibility_id, "Today Tab").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "All Tab").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Pending Tab").displayed? }
end

def click_contacts_tab(tab_name)
    case tab_name
    when "Today"
        #@driver.wait { @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[1]").click }
        @driver.wait { @driver.find_element(:accessibility_id, "Today Tab").click }
        #today_tab = Appium::TouchAction.new
        #today_tab.press(:x => 95, :y => 319).move_to(:x => 95, :y => 319).release.perform 
        #today_tab.tap(:x => 95, :y => 319, :fingers => 1, :tapCount => 1, :duration => 1000).perform
    when "All"
        #@driver.wait { @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[2]").click }
        @driver.wait { @driver.find_element(:accessibility_id, "All Tab").click }
        #all_tab = Appium::TouchAction.new 
        #all_tab.press(:x => 223, :y => 319).move_to(:x => 223, :y => 319).release.perform
        #all_tab.tap(:x => 223, :y => 319, :fingers => 1, :tapCount => 1, :duration => 1000).perform
    when "Pending"
        #@driver.wait { @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeButton[3]").click }
        @driver.wait { @driver.find_element(:accessibility_id, "Pending Tab").click }
        #pending_tab = Appium::TouchAction.new
        #pending_tab.press(:x => 313, :y => 319).move_to(:x => 313, :y => 319).release.perform  
        #pending_tab.tap(:x => 313, :y => 319, :fingers => 1, :tapCount => 1, :duration => 1000).perform
    else
        raise "Error: Could not click on the #{tab_name} tab on the Contacts Page."
    end
end

def edit_contact_page_check(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Edit Contact").displayed? }

    name_array = Array.new
    fields = @driver.find_elements(:class, "XCUIElementTypeTextField")
    fields.each do |field|
        name_array.push(field.value)
    end

    raise "Error: Could find #{$test["#{contact}_fname"]}." unless name_array.include? $test["#{contact}_fname"]
    raise "Error: Could find #{$test["#{contact}_lname"]}." unless name_array.include? $test["#{contact}_lname"]
    raise "Error: Could find #{$test["#{contact}_company"]}." unless name_array.include? $test["#{contact}_company"]
    raise "Error: Could find #{$test["#{contact}_email"]}." unless name_array.include? $test["#{contact}_email"]
end

def back_to_edit_contact_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "Edit Contact").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Edit Topics").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Edit Follow-Up Actions").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Done").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Cancel").displayed? }
end

def edit_contact_page_check2
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Edit Contact").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "First Name *").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Last Name *").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Email *").displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "Company *").displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "Title").displayed? }
    ##@driver.wait { @driver.find_element(:accessibility_id, "Phone").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Mobile").displayed? }
end

def choose_event(event)
    # First click the dropdown menu
    # if element_exists_by_name?("  Mobile Automation Event 1")
    #     event_element = @driver.wait { @driver.find_element(:id, "  Mobile Automation Event 1") }
    #     event_element.click
    # elsif element_exists_by_name?("  Mobile Automation Event 2")
    #     event_element = @driver.wait { @driver.find_element(:id, "  Mobile Automation Event 2") }
    #     event_element.click
    # elsif element_exists_by_name?("  Mobile Auto Long Topics")
    #     event_element = @driver.wait { @driver.find_element(:id, "  Mobile Auto Long Topics") }
    #     event_element.click
    # elsif element_exists_by_name?("  Card Scanner Demo")
    #     event_element = @driver.wait { @driver.find_element(:id, "  Card Scanner Demo") }
    #     event_element.click
    # else
    #     raise "Error: Unknown event."
    # end

    #menu = Appium::TouchAction.new 
    #menu.tap(:x => 335, :y => 181, :fingers => 1, :tapCount => 1, :duration => 1000).perform

    # Then choose the event
    choice_element = @driver.wait { @driver.find_element(:accessibility_id, event) }
    choice_element.click
end

def choose_contact(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    contact_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    contact_list.find_element(:id, "#{fullname}").click



#    cells = contact_list.find_elements(:class, "XCUIElementTypeCell")
#    cells.each do |cell|
#        name = cell.find_element(:accessibility_id, "#{fullname}")
#        if name == fullname
#    end
end

def wait_for_error(message)
    @driver.wait { @driver.find_element(:id, message).displayed? }
end

def navigate_to_contacts
    sleep 5
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:accessibility_id, "Contacts") }
    menu_element.click
    contacts_page_check

    #choose All events
    # button = @driver.find_element(:xpath, "//UIAApplication[1]/UIAWindow[2]/UIAButton[4]")
    # if button
    #     button.click
    #     all_choice = @driver.find_element(:id, "All events and occasions")
    #     if all_choice
    #         all_choice.click
    #     end
    # end

    #click_contacts_tab("All")
end


def select_all_events_on_contacts_page
    #@driver.wait { @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click }

    selection = Appium::TouchAction.new
    selection.press(x: 40, y:160).wait(500).release.perform


    @driver.wait { @driver.find_element(:accessibility_id, "All events and occasions").click }

    # if button
    #     button.click
    #     all_choice = @driver.find_element(:id, "All events and occasions")
    #     if all_choice
    #         all_choice.click
    #     end
    # else
    #     raise "Error: Could not select All Events on the Contacts Page."
    # end
end


def navigate_to_attendee_alerts
    click_menu_button
    menu = @driver.find_elements(:class, "UIATableView")[0]
    attendee_alerts = menu.find_element(:id, "Attendee Alerts")
    attendee_alerts.click
    attendee_alerts_page_check
end

def navigate_to_contactus
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:accessibility_id, "Contact Us") }
    menu_element.click
    contactus_page_check
end

def contactus_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:id, "hdr_contactus.png").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "atEvent").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "contact btn").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "email btn").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Get Directions").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "View Map").displayed? }
end
    

def navigate_to_about
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:accessibility_id, "About") }
    menu_element.click    
    verify_app_version
end

def about_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:id, "hdr_about.png").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "atEvent provides event specific solutions that have real-time lead retrieval and eNurturing functionality.").displayed? }
end

def verify_app_version
    @driver.wait { @driver.find_element(:accessibility_id, "Version #{$app_version}").displayed? }
end

def navigate_to_reportaproblem
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:id, "Report a Problem") }
    menu_element.click
end

def navigate_to_scan_and_checkin
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:accessibility_id, "Scan & Check-In") }
    menu_element.click
    #@driver.wait { @driver.find_element(:id, "Select an event or occasion").displayed? }
    #@driver.wait { @driver.find_element(:id, "btn orange card").displayed? }
    #@driver.wait { @driver.find_element(:id, "btn orange barcode").displayed? }
    #@driver.wait { @driver.find_element(:id, "btn orange qr").displayed? }
end

def navigate_to_select_event
    click_menu_button
    menu_element = @driver.wait { @driver.find_element(:accessibility_id, "Select an Event") }
    menu_element.click
end


def verify_app_menu_highlight(option)
    #menu = @driver.find_elements(:class, "UIATableView")[0]
    #highlight = menu.find_element(:id, option).selected?
    #if !highlight
    #    raise "Error: Expected #{option} to be highlighted."
    #end

    case option
    when "Select an Event"
        eyes_verify_screen(@app_name, 'Select an Event with Menu Highlight')
    when "Scan & Check-In"
        eyes_verify_screen(@app_name, 'Scan & Check-In with Menu Highlight')
    when "Contacts"
        eyes_verify_screen(@app_name, 'Contacts Screen with Menu Highlight')
    when "Activity Summary"
        eyes_verify_screen(@app_name, 'Activity Summary Screen with Menu Highlight')
    when "Contact Us"
        eyes_verify_screen(@app_name, 'Contact Us Screen with Menu Highlight')
    when "About"
        eyes_verify_screen(@app_name, 'About Screen with Menu Highlight')
    when "Report a Problem"
        eyes_verify_screen(@app_name, 'Report a Problem Screen with Menu Highlight')
    end
end

def click_contact(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)
    contact_element = @driver.wait { @driver.find_element(:id, fullname) }
    if contact_element
        contact_element.click
    else
        raise "Error: Could not click on the #{fullname} contact."
    end
end

def verify_contact_info(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    sleep 5
    @driver.wait { @driver.find_element(:accessibility_id, fullname).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["#{contact}_company"]).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["#{contact}_job_title"]).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["contact_phone"]).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["contact_mobile"]).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["#{contact}_email"]).displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, $test["contact_web"]).displayed? }
end

def alphabetic_contacts?(first, second, third)
    first_contact = @driver.wait { @driver.ele_index(:UIATableCell, 5) }
    second_contact = @driver.wait { @driver.ele_index(:UIATableCell, 6) }
    third_contact = @driver.wait { @driver.ele_index(:UIATableCell, 7) }

    if first_contact.name != first || second_contact.name != second || third_contact.name != third
        raise "Error: Contact List not in alphabetic order."
    end
end

def contact_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    #@driver.wait { @driver.find_element(:accessibility_id, "hdr_contacts.png").displayed? }
end

def back_to_contact_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Contact").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Comments").displayed? } 
    @driver.wait { @driver.find_element(:accessibility_id, "Topic(s)").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Follow-Up Action(s)").displayed? }
end


def attendee_alerts_page_check
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:id, "Attendee Alerts").displayed? }
    @driver.wait { @driver.find_element(:id, "Search").displayed? }
    @driver.wait { @driver.find_element(:id, "Filter").displayed? }
    @driver.wait { @driver.find_element(:id, "Sort").displayed? }
    @driver.wait { @driver.find_element(:id, "alertMessage.png").displayed? }
end

def search_for_contact(value)
    #search_element = @driver.wait { @driver.find_element(:id, "Search") }
    search_element = @driver.find_element(:name, "Search")
    search_element.click
    search_element.type value
    #search_element.send_keys value

#    return_element = @driver.wait { @driver.find_element(:id, "Return") }
#    return_element.click

    #keyboard = @driver.find_elements(:class, "UIAKeyboard")[0]
    keyboard = @driver.find_elements(:class, "XCUIElementTypeKeyboard")[0]
    return_element = keyboard.find_element(:id, "Return")
    return_element.click
end

def determine_contact(criteria)
    case criteria
    when $test["contact0_fname"], $test["contact0_lname"], $test["contact0_company"], $test["contact0_email"] # special chars
        contact = "contact0"
    when $test["contact1_fname"], $test["contact1_lname"], $test["contact1_company"], $test["contact1_email"] # Messi
        contact = "contact1"
    when $test["contact2_fname"], $test["contact2_lname"], $test["contact2_company"], $test["contact2_email"] # Ronaldo
        contact = "contact2"
    when $test["contact3_fname"], $test["contact3_lname"], $test["contact3_company"], $test["contact3_email"] # international chars
        contact = "contact3"
    when $test["contact4_fname"], $test["contact4_lname"], $test["contact4_company"], $test["contact4_email"] # Clarkson
        contact = "contact4"
    when $test["contact5_fname"], $test["contact5_lname"], $test["contact5_company"], $test["contact5_email"] # Hammond
        contact = "contact5"
    when $test["contact6_fname"], $test["contact6_lname"], $test["contact6_company"], $test["contact6_email"] # May
        contact = "contact6"
    when $test["contact7_fname"], $test["contact7_lname"], $test["contact7_company"], $test["contact7_email"] # Hawking
        contact = "contact7"
    when $test["contact8_fname"], $test["contact8_lname"], $test["contact8_company"], $test["contact8_email"] # Brown
        contact = "contact8"
    when $test["contact9_fname"], $test["contact9_lname"], $test["contact9_company"], $test["contact9_email"] # Abrams
        contact = "contact9"
    when $test["contact10_fname"], $test["contact10_lname"], $test["contact10_company"], $test["contact10_email"] # Baggins
        contact = "contact10"
    when $test["contact11_fname"], $test["contact11_lname"], $test["contact11_company"], $test["contact11_email"] # Hamilton
        contact = "contact11"
    when $test["contact12_fname"], $test["contact12_lname"], $test["contact12_company"], $test["contact12_email"] # Vettel
        contact = "contact12"
    when $test["contact13_fname"], $test["contact13_lname"], $test["contact13_company"], $test["contact13_email"] # Rosberg
        contact = "contact13"
    when $test["contact14_fname"], $test["contact14_lname"], $test["contact14_company"], $test["contact14_email"] # Manning
        contact = "contact14"
    when $test["contact15_fname"], $test["contact15_lname"], $test["contact15_company"], $test["contact15_email"] # Brady
        contact = "contact15"
    when $test["contact16_fname"], $test["contact16_lname"], $test["contact16_company"], $test["contact16_email"]
        contact = "contact16"
    when $test["contact17_fname"], $test["contact17_lname"], $test["contact17_company"], $test["contact17_email"] 
        contact = "contact17"
    when $test["contact18_fname"], $test["contact18_lname"], $test["contact18_company"], $test["contact18_email"] 
        contact = "contact18"

    end

    return contact, $test["#{contact}_fname"] + " " + $test["#{contact}_lname"], $test["#{contact}_company"], $test["#{contact}_topic1"], $test["#{contact}_topic2"], $test["#{contact}_fua1"], $test["#{contact}_fua2"]
end

def verify_search_contact_returned(criteria)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(criteria)

    #contact_box = @driver.find_elements(:class, "UIATableView")[1]
    #first_contact = contact_box.find_elements(:class, "UIATableCell")[0]

    contact_box = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    contacts = contact_box.find_elements(:class, "XCUIElementTypeCell")
    first_contact = contacts[0]
    first_contact_name = first_contact.find_elements(:class, "XCUIElementTypeStaticText")[0]

    if first_contact_name
        if first_contact_name.name != fullname
            raise "Error: Could not search for the given contact #{criteria}."
        end
    else
        raise "Error: The contact list is unexpectedly empty."
    end
end

def click_first_contact
    #sleep 1
    #contact_box = @driver.find_elements(:class, "UIATableView")[1]
    #first_contact = contact_box.find_elements(:class, "UIATableCell")[0]
    #first_contact = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]")
    first_contact = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
    if first_contact
        first_contact.click
    else
        raise "Error: The contact list is unexpectedly empty."
    end
end

def verify_no_contacts
    begin
        contact_exists = @driver.ele_index(:UIATableCell, 5).displayed?
        raise "Error: There should be no contacts." if contact_exists
    rescue
        return
    end
end

def edit_field(key, old_value, new_value)
    fields_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable")
    field_cells = fields_view.find_elements(:class, "XCUIElementTypeCell")
    field_cells.each do |cell|
        cell_name = cell.find_element(:class, "XCUIElementTypeTextField")

        #puts cell_name.value
        if cell_name.value == old_value
            cell_name.click
            cell_name.clear
            cell_name.send_keys new_value

            return_element = @driver.wait { @driver.find_element(:id, "Done") }
            #return_element = @driver.wait { @driver.find_element(:xpath, "(//XCUIElementTypeButton[@name=\"Done\"])[2]") }
            return_element.click
            sleep 5
            break
        end
    end
end

def replace_text(old_text, new_text)
    textfield = @driver.wait { @driver.find_element(:id, old_text) }
    textfield.click
    textfield.clear
    
    #textfield.type new_text

    textfield.send_keys new_text

    #@driver.wait { @driver.find_element(:accessibility_id, "Return").click }
    @driver.wait { @driver.find_element(:accessibility_id, "Done").click }

    #@driver.find_element(:xpath, "(//XCUIElementTypeButton[@name=\"Done\"])[2]").click

    #note_click_done   
end

def match_text(text)
    #@driver.wait { @driver.find_element(:id, text).displayed? }

    textview = @driver.find_elements(:class, "UIATextView")[0]
    raise "Error: The text does not match." unless textview.value == text

    note_click_done
end

def clear_note(text)
    note = @driver.wait { @driver.find_element(:id, text) }
    note.clear
    note_click_done
end

def note_click_done
    done_button = Appium::TouchAction.new
    done_button.press(:x => 182, :y => 360).move_to(:x => 182, :y => 360).release.perform
end

def sort_contacts_page_check
    @driver.wait { @driver.find_element(:id, "Sort").displayed? }
    @driver.wait { @driver.find_element(:id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:id, "Alphabetically (Ascending)").displayed? }
    @driver.wait { @driver.find_element(:id, "Alphabetically (Descending)").displayed? }
    @driver.wait { @driver.find_element(:id, "btn filterApply").displayed? }
    @driver.wait { @driver.find_element(:id, "btn filterclear").displayed? }
end

def filter_contacts_page_check
    #@driver.wait { @driver.find_element(:id, "Filter Contacts").displayed? }
    #@driver.wait { @driver.find_element(:id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:id, "Company").displayed? }
    @driver.wait { @driver.find_element(:id, "Title").displayed? }
    #@driver.wait { @driver.find_element(:id, "Event").displayed? }
    @driver.wait { @driver.find_element(:id, "Topics").displayed? }
    @driver.wait { @driver.find_element(:id, "Follow Up Actions").displayed? }
    @driver.wait { @driver.find_element(:id, "btn filterApply").displayed? }
    @driver.wait { @driver.find_element(:id, "btn filterclear").displayed? }
end

def sort_contacts(order)
    case order
    when "ascending"
        order_element = @driver.wait { @driver.find_element(:id, "Alphabetically (Ascending)") }
    when "descending"
        order_element = @driver.wait { @driver.find_element(:id, "Alphabetically (Descending)") }
    end

    order_element.click
    apply_element = @driver.wait { @driver.find_element(:id, "btn filterApply") }
    apply_element.click
end

# def verify_contacts_sorted(order)
#     first_contact = @driver.wait { @driver.ele_index(:UIATableCell, 5) }
#     second_contact = @driver.wait { @driver.ele_index(:UIATableCell, 6) }
#     third_contact = @driver.wait { @driver.ele_index(:UIATableCell, 7) }
#     fourth_contact = @driver.wait { @driver.ele_index(:UIATableCell, 8) }

#     case order
#     when "ascending"
#         first = $test["contact0_fname"] + " " + $test["contact0_lname"]
#         second = $test["contact1_fname"] + " " + $test["contact1_lname"]
#         third = $test["contact2_fname"] + " " + $test["contact2_lname"]
#         fourth = $test["contact3_fname"] + " " + $test["contact3_lname"]
#     when "descending"
#         fourth = $test["contact0_fname"] + " " + $test["contact0_lname"]
#         third = $test["contact1_fname"] + " " + $test["contact1_lname"]
#         second = $test["contact2_fname"] + " " + $test["contact2_lname"]
#         first = $test["contact3_fname"] + " " + $test["contact3_lname"]
#     end

#     if first_contact.name != first && second_contact.name != second && third_contact.name != third && fourth_contact.name != fourth
#         raise "Error: The contact list has not been sorted in #{order} alphabetical order."
#     end
# end

def verify_contacts_sorted(order)
    sleep 5

    #tableviews = @driver.find_elements(:class, "UIATableView")
    #contact_rows = tableviews[1].find_elements(:class, "UIATableCell")

    # app_list = Array.new
    # contact_rows.each do |person|
    #     lastname = person.name.split(' ')[1]
    #     app_list.push(lastname)
    # end

    table = @driver.find_elements(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")

    app_list = Array.new
    table.each do |row|
        person = row.find_elements(:class, "XCUIElementTypeStaticText")[0]
        lastname = person.name.split(' ')[1]
        app_list.push(lastname)
    end

    sorted_list = Array.new
    if order == "ascending"
        sorted_list = app_list.sort
    elsif order == "descending"
        sorted_list = app_list.sort.reverse
    end

    raise "Error: The contact list has not been sorted in #{order} alphabetical order." unless app_list == sorted_list 
end

def filter_contacts(filter, value)
    filter_element = @driver.wait { @driver.find_element(:id, filter) }
    filter_element.click

    value_element = @driver.wait { @driver.find_element(:id, value) }
    value_element.click

    done_element = @driver.wait { @driver.find_element(:id, "Done") }
    done_element.click
end

def apply_filter
    apply_element = @driver.wait { @driver.find_element(:id, "btn filterApply") }
    apply_element.click
end

def verify_filtered_number(number)
    @driver.wait { @driver.find_element(:id, "Filter (#{number})").displayed? }
end

def add_topic_fua(lname)
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

    topics_element = @driver.wait { @driver.find_element(:id, "Edit Topics") }
    topics_element.click

    if topic1 =~ /\w+/
        topic1_element = @driver.wait { @driver.find_element(:id, topic1) }
        topic1_element.click
    end

    if topic2 =~ /\w+/
        topic2_element = @driver.wait { @driver.find_element(:id, topic2) }
        topic2_element.click
    end

    next_element = @driver.wait { @driver.find_element(:id, "Next") }
    next_element.click

    if fua1 =~ /\w+/
        fua1_element = @driver.wait { @driver.find_element(:id, fua1) }
        fua1_element.click
    end

    if fua2 =~ /\w+/
        fua2_element = @driver.wait { @driver.find_element(:id, fua2) }
        fua2_element.click
    end

    done_element = @driver.wait { @driver.find_element(:id, "Done") }
    done_element.click
    #contacts_page_check
end

def verify_filter_highlight(option)
    highlight_element = @driver.wait { @driver.find_element(:accessibility_id, option) }
    if highlight_element.value != true
        raise "Error: The filter #{option} should be highlighted."
    end
end

def verify_no_filter_highlights(list)
    for option in list do
        element = @driver.wait { @driver.find_element(:id, option) }
        if element.value == 1
            raise "Error: The filter #{option} should not be highlighted."
        end
    end
end

def add_contacts(list)
    list.each do |person|
        contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
        click_button("btn orange addContact")
        add_new_contact(person)

        #if topic1 =~ /\w+/
        add_topic_fua(person)
        #else
        #    click_button("Done")
        #end
        #click_home_button
    end
end


def accept_notifications
    #allow_alert = @driver.wait { @driver.find_element(:id, "\"atEvent\" Would Like to Send You Notifications") }
    #ok = allow_alert.find_element(:id, "OK")
    begin
        button = @driver.find_element(:id, "Allow")
        if button
            button.click
        end
    rescue
        return
    end
end

def accept_location_data_dialog
    @driver.wait { @driver.find_element(:accessibility_id, "Allow").click }
end

def allow_location_data
    click_button("btn orange addContact")
    #allow_alert = @driver.wait { @driver.find_element(:accessibility_id, "Allow “atEvent” to access your location while you use the app?") }
    #allow = allow_alert.find_element(:accessibility_id, "Allow")
    #allow.click

    @driver.wait { @driver.find_element(:accessibility_id, "Allow").click }
    click_home_button
end

def verify_contacts(list)
    navigate_to_contacts
    list.each do |person|
      contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
      @driver.wait { @driver.find_element(:id, fullname).displayed? }
    end 
end

def verify_contact_with_card_image
    @driver.wait { @driver.find_element(:accessibility_id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "Contact").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "btn orange scan").displayed? }
    @driver.wait { @driver.find_element(:accessibility_id, "btn viewCard").displayed? }

    #images_array = @driver.wait { @driver.find_elements(:class, "UIAImage") }
    #images_array.each do |element|
    #    if element.name =~ /^(.*).jpg$/
    #        @driver.wait { @driver.find_element(:id, element.name).displayed? }
    #        $fields_hash["bcard_file"] = element.name
    #        raise "Error: The business card image is not visible." unless element.size.width == $fields_hash["bcard_small_width"] && element.size.height == $fields_hash["bcard_small_height"] && element.location.x == $fields_hash["bcard_small_x"] && element.location.y == $fields_hash["bcard_small_y"]
    #    end
    #end
end

def verify_edit_contact_with_card_image
    @driver.wait { @driver.find_element(:id, "btn home").displayed? }
    @driver.wait { @driver.find_element(:id, "hdr_editContact.png").displayed? }
    @driver.wait { @driver.find_element(:id, "btn orange scan").displayed? }
    @driver.wait { @driver.find_element(:id, "btn viewCard").displayed? }

    images_array = @driver.wait { @driver.find_elements(:class, "UIAImage") }
    images_array.each do |element|
        if element.name =~ /^(.*).jpg$/
            @driver.wait { @driver.find_element(:id, element.name).displayed? }
            $fields_hash["bcard_file"] = element.name
            raise "Error: The business card image is not visible." unless element.size.width == $fields_hash["bcard_small_width"] && element.size.height == $fields_hash["bcard_small_height"] && element.location.x == $fields_hash["bcard_small_x"] && element.location.y == $fields_hash["bcard_small_y"]
        end
    end
end

def click_magnify_button
    magnify_element = @driver.wait { @driver.find_element(:accessibility_id, "btn viewCard") }
    magnify_element.click
end

def verify_magnified_card
    @driver.wait { @driver.find_element(:accessibility_id, "capture close").displayed? }
    #@driver.wait { @driver.find_element(:id, $fields_hash["bcard_file"]).displayed? }
end

def multitouch_bcard(action, model)
    #@driver.pinch 75
    #@driver.zoom 200
  
    case model
    when "iPhone 6"
        top_startx = 177.5
        top_starty = 304.5
        bottom_startx = 177.5
        bottom_starty = 371.1
    when "iPhone 6 Plus"
        exit 1
    end

    case action
    when "pinches"
        top_offset = 40
        bottom_offset = -40
    when "zooms"
        top_offset = -40
        bottom_offset = 40 
    end

    top = Appium::TouchAction.new
    top.swipe start_x: top_startx, start_y: top_starty, end_x: 0, end_y: top_offset, duration: 1000

    bottom = Appium::TouchAction.new
    bottom.swipe start_x: bottom_startx, start_y: bottom_starty, end_x: 0, end_y: bottom_offset, duration: 1000

    zoom = Appium::MultiTouch.new
    zoom.add top
    zoom.add bottom
    zoom.perform

    sleep 10
end

def verify_fieldvalue(key, value)
    table_cells = @driver.wait { @driver.find_elements(:class, "UIATableCell") }
    table_cells.each do |cell|
        if cell.name == key
            @driver.wait { cell.find_element(:id, value).displayed? }
        end
    end
end

def verify_empty_fieldvalue(key)
    table_cells = @driver.wait { @driver.find_elements(:class, "UIATableCell") }
    table_cells.each do |cell|
        if cell.name == key
            text_fields = cell.find_elements(:class, "UIAStaticText")
            #if text_fields[1].name !~ /\0/
            if text_fields[1] != nil
                raise "Error: Expected \"#{key}\" to have been cleared."
            end
        end
    end
end

def select_topics(list)
    list.split(", ").each do |topic|
        # topic_element = @driver.wait { @driver.find_element(:id, topic) }
        # if topic_element.value != 1
        #     topic_element.click
        # else
        #     raise "Error: Expected #{topic_element.name} topic to be deselected but it is already selected."
        # end

        topic_selected = false
        #topic_view = @driver.find_elements(:class, "XCUIElementTypeTable")[0]
        topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
   
        topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
        topic_cells.each do |cell|
            if cell.name == "Topic Not Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == topic
                    cell.click
                    topic_selected = true
                    break
                end
            end
        end
        raise "Error: Could not select #{topic} topic." unless topic_selected
    end
end

def select_subtopics(list)
    list.split(", ").each do |topic|
        topic_selected = false
        topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
   
        topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
        topic_cells.each do |cell|
            if cell.name == "Sub Topic / FUA Not Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == topic
                    cell.click
                    topic_selected = true
                    break
                end
            end
        end
        raise "Error: Could not select #{topic} subtopic." unless topic_selected
    end
end

def deselect_topics(list)
    list.split(", ").each do |topic|
        # topic_element = @driver.wait { @driver.find_element(:id, topic) }
        # if topic_element.value == 1
        #     topic_element.click
        # else
        #     raise "Error: Expected #{topic_element.name} topic to be selected but it is already deselected."
        # end

        topic_deselected = false
        topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
        topic_cells.each do |cell|
            if cell.label == "Topic Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == topic
                    cell.click
                    topic_deselected = true
                    break
                end
            end
        end
        raise "Error: Could not deselect #{topic} topic." unless topic_deselected        
    end
end

def deselect_subtopics(list)
    list.split(", ").each do |topic|
        # topic_element = @driver.wait { @driver.find_element(:id, topic) }
        # if topic_element.value == 1
        #     topic_element.click
        # else
        #     raise "Error: Expected #{topic_element.name} topic to be selected but it is already deselected."
        # end

        topic_deselected = false
        topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
        topic_cells.each do |cell|
            if cell.label == "Sub Topic / FUA Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == topic
                    cell.click
                    topic_deselected = true
                    break
                end
            end
        end
        raise "Error: Could not deselect #{topic} subtopic." unless topic_deselected        
    end
end

def verify_topics_state(list, state)
    list.split(", ").each do |topic|

        # topic_element = @driver.wait { @driver.find_element(:id, topic) }
        # if state == "selected"
        #     raise "Error: The #{topic_element.name} topic is not #{state} as expected." unless topic_element.value == 1
        # elsif state == "deselected"
        #     raise "Error: The #{topic_element.name} topic is not #{state} as expected." unless topic_element.value != 1
        # end

        topics_array = Array.new
        if state == "selected"
            topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable")
            topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
            topic_cells.each do |cell|
                if cell.label == "Topic Selected" || cell.label == "FUA Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} topic is not selected as expected." unless topics_array.include? topic
        elsif state == "deselected"
            topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable")
            topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
            topic_cells.each do |cell|
                if cell.label == "Topic Not Selected" || cell.label == "FUA Not Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} topic is not deselected as expected." unless topics_array.include? topic
        end
    end
end

def verify_subtopics_state(list, state)
    list.split(", ").each do |topic|
        topics_array = Array.new
        topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")

        if state == "selected"
            #topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
            topic_cells.each do |cell|
                if cell.label == "Sub Topic / FUA Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} subtopic is not selected as expected." unless topics_array.include? topic
        elsif state == "deselected"
            #topic_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #topic_cells = topic_view.find_elements(:class, "XCUIElementTypeCell")
            topic_cells.each do |cell|
                if cell.label == "Sub Topic / FUA Not Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        topics_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{topic} subtopic is not deselected as expected." unless topics_array.include? topic
        end
    end
end

def verify_note_popup(title)
    #@driver.wait { @driver.find_element(:id, title).displayed? }
    @driver.wait { @driver.find_element(:id, "Done").displayed? }
end

def subtopics_page_check(list)
    @driver.wait { @driver.find_element(:id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:id, "Done").displayed? }
    list.split(", ").each do |subtopic|
        @driver.wait { @driver.find_element(:id, subtopic).displayed? }
    end
end

def fuas_page_check(list)
    @driver.wait { @driver.find_element(:id, "capture close").displayed? }
    @driver.wait { @driver.find_element(:id, "Done").displayed? }
    list.split(", ").each do |subtopic|
        @driver.wait { @driver.find_element(:id, subtopic).displayed? }
    end
end

def verify_topics_list_alpha_order(type, list)
    #edit_topics_page_check

    list_array = Array.new
    list.split(", ").each do |topic|
        list_array.push(topic)
    end
    list_array.sort
    
    visible_topics_array = Array.new

    if type == "topics"
        ui_topics_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    elsif type == "subtopics"
        ui_topics_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    end
        
    topics = ui_topics_list.find_elements(:class, "XCUIElementTypeCell")
    topics.each do |topic|
        topic_name = topic.find_element(:class, "XCUIElementTypeStaticText")
        if topic_name.displayed?
            visible_topics_array.push(topic_name.name)
        end
    end


    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_topics_array[i] == list_array[i]
    end
end

def verify_fua_list_alpha_order(type, list)
    list_array = Array.new
    list.split(", ").each do |fua|
        list_array.push(fua)
    end
    
    visible_topics_array = Array.new

    if type == "fua"
        list_array.sort
        ui_fua_list = @driver.find_element(:xpath, "//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable")
    elsif type == "subfua"
        ui_fua_list = @driver.find_element(:xpath,"//XCUIElementTypeApplication[@name=\"atEvent 3.0\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable")
    end
        
    fuas = ui_fua_list.find_elements(:class, "XCUIElementTypeCell")

    fuas.each do |fua|
        fua_name = fua.find_element(:class, "XCUIElementTypeStaticText")
        if fua_name.displayed?
            visible_topics_array.push(fua_name.name)
        end
    end

    for i in 0..list_array.size - 1
        raise "Error: #{list_array[i]} is not displayed in the list." unless visible_topics_array[i] == list_array[i]
    end
end

def select_fuas(list)
    # list.split(", ").each do |fua|
    #     fua_element = @driver.wait { @driver.find_element(:id, fua) }
    #     if fua_element.value != 1
    #         fua_element.click
    #     else
    #         raise "Error: Expected #{fua_element.name} follow-up action to be deselected but it is already selected."
    #     end
    # end

    list.split(", ").each do |fua|
        selected = false
        fua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        fua_cells = fua_view.find_elements(:class, "XCUIElementTypeCell")
        fua_cells.each do |cell|
            if cell.label == "FUA Not Selected" || cell.label == "FUA Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == fua
                    cell.click
                    selected = true
                    break
                end
            end
        end
        
        raise "Error: Could not select the #{fua} FUA." unless selected 
    end
end

def select_subfuas(list)
    list.split(", ").each do |subfua|
        selected = false
        subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
        subfua_cells.each do |cell|
            if cell.label == "Sub Topic / FUA Not Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == subfua
                    cell.click
                    selected = true
                    break
                end
            end
        end
        
        raise "Error: Could not select the #{subfua} sub-FUA." unless selected 
    end
end

def clear_all_subfuas
    subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
    subfua_cells.each do |cell|
        if cell.label == "Sub Topic / FUA Selected"
            cell.click
        end
    end
end

def deselect_subfuas(list)
    list.split(", ").each do |subfua|
        deselected = false
        subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
        subfua_cells.each do |cell|
            if cell.label == "Sub Topic / FUA Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == subfua
                    cell.click
                    deselected = true
                    break
                end
            end
        end
        
        raise "Error: Could not deselect the #{subfua} sub-FUA." unless deselected 
    end
end

def verify_subfuas_state(list, state)
    subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")

    list.split(", ").each do |subfua|
        subfuas_array = Array.new
        if state == "selected"
            #subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
            subfua_cells.each do |cell|
                if cell.label == "Sub Topic / FUA Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        subfuas_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{subfua} Sub Follow-Up Action is not selected as expected." unless subfuas_array.include? subfua
        elsif state == "deselected"
            #subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
            subfua_cells.each do |cell|
                if cell.label == "Sub Topic / FUA Not Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        subfuas_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{subfua} Sub Follow-Up Action is not deselected as expected." unless subfuas_array.include? subfua
        end
    end
end

def verify_all_subfuas_state(state)
    subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
    subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")

    if state == "selected"
        #subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        #subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
        subfua_cells.each do |cell|
            if cell.label == "Sub Topic / FUA Not Selected"
                raise "Error: All Sub Follow-Up Actions not selected."
            end
        end
    elsif state == "deselected"
        #subfua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        #subfua_cells = subfua_view.find_elements(:class, "XCUIElementTypeCell")
        subfua_cells.each do |cell|
            if cell.label == "Sub Topic / FUA Selected"
                raise "Error: All Sub Follow-Up Actions not deselected."
            end
        end
    end
end

def verify_fuas_state(list, state)
    list.split(", ").each do |fua|
        # fua_element = @driver.wait { @driver.find_element(:id, fua) }
        # if state == "selected"
        #     #raise "Error: The #{fua_element.name} follow-up action is not #{state} as expected." unless fua_element.value == 1
        #     raise "Error: The #{fua_element.name} follow-up action is not #{state} as expected." unless fua_element.selected?
        # elsif state == "deselected"
        #     #raise "Error: The #{fua_element.name} follow-up action is not #{state} as expected." unless fua_element.value != 1
        #     raise "Error: The #{fua_element.name} follow-up action is not #{state} as expected." unless !fua_element.selected?
        # end

        fuas_array = Array.new
        fua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        fua_cells = fua_view.find_elements(:class, "XCUIElementTypeCell")

        if state == "selected"
            #fua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #fua_cells = fua_view.find_elements(:class, "XCUIElementTypeCell")
            fua_cells.each do |cell|
                if cell.label == "FUA Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        fuas_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{fua} Follow-Up Action is not selected as expected." unless fuas_array.include? fua
        elsif state == "deselected"
            #fua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
            #fua_cells = fua_view.find_elements(:class, "XCUIElementTypeCell")
            fua_cells.each do |cell|
                if cell.label == "FUA Not Selected"
                    cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                    if cell_text
                        fuas_array.push(cell_text.name)
                    end
                end
            end
            raise "Error: The #{fua} Follow-Up Action is not deselected as expected." unless fuas_array.include? fua
        end
    end
end

def verify_all_fuas_state(state)
    visible_topics_array = Array.new
    ui_topics_list = @driver.find_elements(:class, "UIATableView")[1].find_elements(:class, "UIATableCell")
    ui_topics_list.each do |topic|
        if topic.displayed?
            visible_topics_array.push(topic.name)
        end
    end

    for i in 0..visible_topics_array.size - 1
        if state == "selected"
            raise "Error: #{visible_topics_array[i]} is not selected." unless visible_topics_array[i].value == 1
        elsif state == "deselected"
            raise "Error: #{visible_topics_array[i]} is selected." unless visible_topics_array[i].value != 1
        end    
    end
end


def deselect_fuas(list)
   # list.split(", ").each do |fua|
   #      fua_element = @driver.wait { @driver.find_element(:id, fua) }
   #      if fua_element.value == 1
   #          fua_element.click
   #      else
   #          raise "Error: Expected #{fua_element.name} follow-up action to be selected but it is already deselected."
   #      end
   #  end 

    list.split(", ").each do |fua|
        deselected = false
        fua_view = @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]")
        fua_cells = fua_view.find_elements(:class, "XCUIElementTypeCell")
        fua_cells.each do |cell|
            if cell.label == "FUA Selected"
                cell_text = cell.find_element(:class, "XCUIElementTypeStaticText")
                if cell_text.name == fua
                    cell.click
                    deselected = true
                    break
                end
            end
        end

        raise "Error: Could not deselect the #{fua} FUA." unless deselected 
    end
end

def verify_contact_tab_count(tab_name)
    count = get_contacts_count(tab_name).to_s
    text_display = "#{tab_name}(#{count})"
    @driver.wait { @driver.find_element(:id, text_display).displayed? }
end

# Get the number of contacts in the contacts list.
def get_contacts_count(tab_name)
    click_contacts_tab(tab_name)
    table_views = @driver.find_elements(:class, "UIATableView")
    contact_cells = table_views[1].find_elements(:class, "UIATableCell")
    contact_cells.size
end


def verify_contact_list_count(tab, expected)
    count = get_contacts_count(tab).to_s
    raise "Error: The actual number of contacts doesn't match the expected." unless count.to_i == expected.to_i
end


def verify_all_topics_deselected
    ui_topics_list = @driver.find_elements(:class, "UIATableView")[1].find_elements(:class, "UIATableCell")
    ui_topics_list.each do |topic|
        if topic.displayed?
            raise "Error: The #{topic.name} topic is not deselected as expected." unless topic.value != 1
        end
    end
end


def scroll_to_edit_contact_bottom
    #@driver.swipe(:start_x => 316, :start_y => 513, :end_x => 316, :end_y => 127, :touchCount => 1, :duration => 1000)
    #@driver.swipe(:start_x => 316, :start_y => 513, :end_x => 0, :end_y => -386, :touchCount => 1, :duration => 200)

    #@driver.swipe(:start_x => 359, :start_y => 513, :end_x => 0, :end_y => -392, :touchCount => 1, :duration => 500)

    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 359, :y => 513).move_to(:x => 0, :y => -392).release.perform
end


def scroll_field_to_top(field)
    scroll_el = Appium::TouchAction.new

    case field
    when "Web"
        scroll_el.press(:x => 57, :y => 454).move_to(:x => 0, :y => -110).release.perform
    when "Street"
        scroll_el.press(:x => 57, :y => 512).move_to(:x => 0, :y => -220).release.perform
    when "City"
        scroll_el.press(:x => 57, :y => 559).move_to(:x => 0, :y => -455).release.perform
    when "State"
        scroll_el.press(:x => 57, :y => 607).move_to(:x => 0, :y => -503).release.perform
    end
end


def click_keyboard_done
    keyboard = @driver.find_element(:class, "UIAKeyboard")
    done_key = keyboard.find_element(:id, "Done")
    done_key.click
end


def click_keyboard_next
    keyboard = @driver.find_element(:class, "UIAKeyboard")
    done_key = keyboard.find_element(:id, "Next")
    done_key.click
end


def verify_contact_tab_choice(choice)
    ui_buttons_list = @driver.find_elements(:class, "UIAButton")
    
    text_display = "^#{choice} Tab"
    ui_buttons_list.each do |button|
        if button.name =~ /#{text_display}/
            return
        end
    end
    raise "Error: Could not find the #{choice} tab displayed."
end


def verify_empty_contacts_tab(tabname)
    if tabname == "Today"
        display_image = "ZeroContactsToday"
    elsif tabname == "All"
        display_image = "ZeroContactsAll"
    elsif tabname == "All, specific event"
        display_image = "ZeroContactsEvent"
    elsif tabname == "Pending"
        display_image = "ZeroContactsPending"
    end

    @driver.wait { @driver.find_element(:id, display_image).displayed? }
end


def select_event_contacts_page(event)
    # event_element = Appium::TouchAction.new 
    # event_element.tap(:x => 336, :y => 159, :fingers => 1, :tapCount => 1, :duration => 1000).perform
    
    # choice_element = @driver.find_element(:id, event)
    # if choice_element
    #     choice_element.click
    # else
    #     raise "Error: Could not find #{event} in the Events dropdown."
    # end


    #selection = Appium::TouchAction.new
    #selection.tap( x: 336, y:159, count: 3).perform
    @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click

    @driver.wait { @driver.find_element(:accessibility_id, event).click }
end


def verify_attendee_alerts_event(match)
    buttons = @driver.find_elements(:class, "UIAButton")
    event = buttons[2]
    if event.name != "  #{match}"
        raise "Error: Expected the event to match #{event}."
    end
end


def eyes_verify_screen(app_name, screen_name)
    if $eyes_enabled == "true"
        @eyes.batch = @eyes_batch
        #@eyes.baseline_name = screen_name
        @eyes.test(app_name: app_name, test_name: screen_name, driver: @driver) do |driver|
            @eyes.check_window(screen_name)
        end
    end
end


def click_terms_link
    web_link = Appium::TouchAction.new 
    web_link.tap(:x => 134, :y => 421, :fingers => 1, :tapCount => 1, :duration => 1000).perform
end

def checkin_tab()
   checkInTabUp = @driver.find_element(:accessibility_id, "checkInTabUp")
   tapAction = Appium::TouchAction.new
   tapAction.tap(:element => checkInTabUp).perform
end

def is_attendee_list_present()
    @driver.wait { @driver.find_element(:accessibility_id, "checkInTabDown").displayed? }
end

def one_touch_check_in()
    checkin_button = @driver.find_element(:accessibility_id, "cicsOneTouchBG")
    tapAction = Appium::TouchAction.new
    tapAction.tap(:element => checkin_button).perform
end

def verify_one_touch_check_in()
    @driver.wait { @driver.find_element(:accessibility_id, "cicsOneTouchGreen").displayed? }
end

def one_touch_uncheck_in()
    onetouch_button = @driver.find_element(:accessibility_id, "cicsOneTouchGreen")
    tapAction = Appium::TouchAction.new
    tapAction.tap(:element => onetouch_button).perform
    yes_button = @driver.find_element(:accessibility_id, "Yes")
    yes_button.click
end

def verify_one_touch_uncheck_in()
    @driver.wait { @driver.find_element(:accessibility_id, "cicsOneTouchBG").displayed? }
end

def attendee_screen_checkin()
    tapAction = Appium::TouchAction.new
    checkin_button = @driver.find_element(:accessibility_id, "Check-In")
    tapAction.tap(:element => checkin_button).perform
end

# TODO have this take an attendee row
def attendee_info(row)
    attendees = @driver.find_element(:xpath, '//XCUIElementTypeTable/XCUIElementTypeCell[@name="CS Attendee cell checked in / Green check mark"]/parent::*').find_elements(:xpath, './/XCUIElementTypeCell')
    magnifying_glass = attendees[row-1].find_element(:accessibility_id, "cicsSwipeInfo")
    magnifying_glass.click
end 

def attendee_screen_uncheckin()
    uncheckin_button = @driver.find_element(:accessibility_id, "Undo Check-In")
    tapAction = Appium::TouchAction.new
    tapAction.tap(:element => uncheckin_button).perform
end

def verify_attendee_screen_uncheckin()
    @driver.wait { @driver.find_element(:accessibility_id, "Check-In").displayed? }
end

def verify_attendee_screen_checkin()
    @driver.wait { @driver.find_element(:accessibility_id, "Undo Check-In").displayed? }
end

def attendee_options(row)
    attendees = @driver.find_element(:xpath, '//XCUIElementTypeTable/XCUIElementTypeCell[@name="CS Attendee cell not checked in / Grey circle"]/parent::*').find_elements(:xpath, './/XCUIElementTypeCell')
        @driver.swipe(direction: 'left', element: attendees[row-1])
end

# TODO see todo on line 2045 for verify_alert_is_enabled
def verify_attendee_options()
    @driver.wait { @driver.find_element(:accessibility_id, "cicsSwipeInfo").displayed? }
end

# TODO can we have just 1 set_alert that takes an attendee row as an arguement
def set_alert(enabled)
    case enabled
    when "enabled"
        #attendees = @driver.find_element(:xpath, '//XCUIElementTypeTable/XCUIElementTypeCell[@name="CS Attendee cell checked in / Green check mark"]/parent::*').find_elements(:xpath, './/XCUIElementTypeCell')
        alert_button = attendees[0].find_element(:accessibility_id, "cicsSwipeAlert")
        alert_button.click
    when "disabled"
        alert_button = @driver.find_element(:accessibility_id, "cicsSwipeAlertGrey")
        alert_button.click
    else # default to "disabled if not "enabled"
        alert_button = attendees[0].find_element(:accessibility_id, "cicsSwipeAlertGrey")
        alert_button.click
  end
end

# TODO This is not sufficient, it could be finding an alert in another contact that you are not testing
# Probably make this take an arguement, which would be a contact row
def verify_alert_is_enabled(row)
    attendees = @driver.find_element(:xpath, '//XCUIElementTypeTable/XCUIElementTypeCell[@name="CS Attendee cell checked in / Green check mark"]/parent::*').find_elements(:xpath, './/XCUIElementTypeCell')
    @driver.wait { attendees[row-1].find_element(:accessibility_id, "cicsSwipeAlert").displayed? }
end

# TODO same as verify_alert_is_enabled
def verify_alert_is_disabled(row)
    attendees = @driver.find_element(:xpath, '//XCUIElementTypeTable/XCUIElementTypeCell[@name="CS Attendee cell checked in / Green check mark"]/parent::*').find_elements(:xpath, './/XCUIElementTypeCell')
    @driver.wait { attendees[row-1].find_element(:accessibility_id, "cicsSwipeAlertGrey").displayed? }
end

def verify_alert_popup()
    @driver.wait { @driver.find_element(:xpath, '//XCUIElementTypeAlert[@name="Attendee Alert"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther').displayed? }
end

def popup_options(option)
  yes_button = @driver.find_element(:accessibility_id, "Yes")
  cancel_button = @driver.find_element(:accessibility_id, "Cancel")
  case option
  when "Yes", "yes"
    yes_button.click
  when "Cancel"
    cancel_button.click
  else
    cancel_button.click
end
end

def verify_notification(name)
    @driver.wait { @driver.find_element(:accessibility_id, "ATEVENT, now, #{name} has been checked In.").displayed? }
end

def verify_no_notification()
    @driver.wait { @driver.find_element(:accessibility_id, "ATEVENT, now, #{name} has been checked In.").displayed? }
end

def attendee_screen_alert(name)
    case name
    when "Set Attendee Alert"
    tapAction = Appium::TouchAction.new
    checkin_button = @driver.find_element(:accessibility_id, "Set Attendee Alert")
    tapAction.tap(:element => checkin_button).perform
    else
    tapAction = Appium::TouchAction.new
    checkin_button = @driver.find_element(:accessibility_id, "Disable Attendee Alert")
    tapAction.tap(:element => checkin_button).perform
end
end

def verify_alert_action_screen(color)
    case color
    when "orange"
    @driver.wait { @driver.find_element(:accessibility_id, "Disable Attendee Alert").displayed? }
    else 
    @driver.wait { @driver.find_element(:accessibility_id, "Set Attendee Alert").displayed? }
end
end

def one_touch_alert(name)
    row = @driver.find_element(name: "#{name}")
    @driver.swipe(direction: 'right', element: row)
end

def enter_required_fields(field, value)
    field_element = @driver.wait { @driver.find_element(:name, field) }
    if field_element
        field_element.send_keys value
    else
        raise "Error: Could not find the #{field} field."
    end
end 

def select_dropdown(dropdown)
    dropdown = @driver.wait { @driver.find_element(:id, "#{dropdown} *") }
    if dropdown
        dropdown.click
    else
        raise "Error: Could not find the #{dropdown} dropdown."
    end
end 

def select_dropdown_value(dropvalue)
    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 0, :y => 523).move_to(:x => 0, :y => -170).release.perform
    @driver.wait { @driver.find_element(:xpath, '(//XCUIElementTypeButton[@name="Done"])[2]') }.click
    @driver.wait { @driver.find_element(:id, "#{dropvalue}").displayed? }
end 

def select_social_dropdown(sdropdown)
    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 0, :y => 600).move_to(:x => 0, :y => -800).release.perform
    sdropdown = @driver.wait { @driver.find_element(:id, sdropdown) }
    if sdropdown
        sdropdown.click
    else
        raise "Error: Could not find the #{sdropdown} dropdown."
    end
end 

def edit_dropdown(dropvalue)
    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 0, :y => 574).move_to(:x => 0, :y => 505).release.perform
    @driver.wait { @driver.find_element(:xpath, '(//XCUIElementTypeButton[@name="Done"])[2]') }.click
    @driver.wait { @driver.find_element(:id, "#{dropvalue}").displayed? }
end 

def scroll_to_edit_botton
    scroll_el = Appium::TouchAction.new
    scroll_el.press(:x => 359, :y => 303).move_to(:x => 0, :y => -950).release.perform
    scroll_el.press(:x => 359, :y => 600).move_to(:x => 0, :y => -950).release.perform
end