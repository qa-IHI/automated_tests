# encoding: UTF-8

@businesscards @qa_ready @22
Feature: QR code scan

@22.1
Scenario Outline: Verify that the user can scan a QR code with all mandatory fields

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the barcode scan button
	Then the code scanning functionality opens
	When the user scans a "<Type>"" QR code
	Then the user sees the scanned contact on the today tab

	| Type |
	| vCard |
	| meCard |

@22.2
Scenario: Verify that user is taken to attendee action screen when scanning a QR code with missing mandatory fields

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the barcode scan button
	Then the code scanning functionality opens
	When the user scans the QR code
	Then the attende information screen is displayed.

@22.3
Scenario Outline: Verify that the user can scan a vendor API QR code
	
	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the barcode scan button
	Then the code scanning functionality opens
	When the user scans a/n "<Vendor>" API QR code
	Then the user sees the scanned contact on the pending tab

	Examples:

	| Event | Vendor |
	|		| Experient |
	|		| Compusystems |
	|		|Xpress Leads Pro |
	|		| Expo Logic |
	|		| Custom |