# encoding: UTF-8

@checkin_feature_flow @qa_ready @19
Feature: Checkin Flow

@19.1
Scenario: Checkin an attendee doing a one-touch checkin

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user does a one touch checkin
	Then the user should see that the checkin icon is green.

@19.2
Scenario: Checkin an attendee via attendee action screen

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on the second attendee
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	And the user taps on "Check-In" button
	Then the user should see the attendee is checked-in.

@19.3
Scenario: Uncheckin an attendee by one-touch checkin

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user does a one touch uncheckin
	Then the user should see that the checkin icon is gray.

@19.4
Scenario: Verify that pop-up window for undoing checkin can be dismissed

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on the second attendee
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	When the user taps on "Undo Check-In" button
	And the user taps on "Cancel" on the pop-up window
	Then the user should see the attendee is checked-in.

@19.5
Scenario: Unceckin an attendee via attendee action screen

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on the second attendee
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	When the user taps on "Undo Check-In" button
	And the user taps on "Yes" on the pop-up window
	Then the user should see the attendee is unchecked-in.



