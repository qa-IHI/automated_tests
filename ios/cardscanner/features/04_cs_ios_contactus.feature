# encoding: UTF-8

@cs_feature_contact_us @qa_ready @4
Feature: Contact Us

Background:
  Given I am logged out on the app
  

@4.1
Scenario: 4.1 Home button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"


@4.2
Scenario: 4.2 Menu icon from Contact Us
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Menu Icon
  Then the app menu should open and "Contact Us" should be highlighted "eyes"


# Requires having to set up an email account on the iPhone (or simulator) first. May revisit later. 
@wip @4.3
Scenario: 4.3 Mail button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the tutorial is displayed "eyes"
  When the user clears the tutorial overlay
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks the Email button
  Then the default email client should open with recipient address set to info@at-event.com "eyes"


# Revisit after merging with Applitools update.
# Currently, Apple UIAutomation doesn't support switching to another app in iOS Simulator.
@wip @4.4
Scenario: 4.4 Get Directions button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks "Get Directions"
  Then the default map client should open with directions from the current location to the atEvent office "eyes"


# Revisit after merging with Applitools update.
# Currently, Apple UIAutomation doesn't support switching to another app in iOS Simulator.
@wip @4.5
Scenario: 4.5 View Map button from Contact Us
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the tutorial is displayed "eyes"
  When the user clears the tutorial overlay
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contact Us page "eyes"
  And the user clicks "View Map"
  Then the default map client should open with a map of the atEvent office "eyes"
  