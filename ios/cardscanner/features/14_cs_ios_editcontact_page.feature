# encoding: UTF-8

@cs_feature_edit_contact_page @qa_ready @14 @coretests
Feature: Edit Contact Page

Background:
  Given I am logged out on the app
  

@14.1
Scenario: 14.1 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  #When the user allows the app to use location data
  When the user adds the "Top Gear" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"


@14.2
Scenario: 14.2 Home button from Edit Contact page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "random" "eyes"
  When the user clicks the Home button
 	Then the Scan & Check-In page should open "noeyes"


@14.3
 Scenario: 14.3 Back button from Edit Contact page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "random" "eyes"
  When the user clicks the Back button
  Then the Contact page should appear "eyes"


@14.4
Scenario: 14.4 View a business card in full screen from the Edit Contact page
	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "komack" "eyes"
  When the user clicks the magnify button
  Then the enlarged business card should appear for "komack" "eyes"


# Pinch and zoom are currently not implemented by CS.
@wip @14.5
Scenario: 14.5 From the Edit Contact page, view a business card in full screen and zoom in


@14.6
Scenario: 14.6 Edit Topics from Edit Contact page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "komack" "eyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Topics*"
  Then the user should be on the Topics page "eyes"


@14.7
Scenario: 14.7 Edit Follow-Up Actions from Edit Contact page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "komack" "eyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Follow-Up Actions*"
  Then the user should be on the Edit Follow-Up Actions page "eyes"


@14.8
Scenario: 14.8 Press Done on the Edit Contact page
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "komack" "eyes"
  When the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the Contact page should appear "eyes"


@14.9
Scenario: 14.9 Press Cancel on the Edit Contact page
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "Uzair"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Card Scanner Demo" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "komack"
  And the user clicks the first contact
  Then the contact page should contain a business card image for "komack" "eyes"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "komack" "eyes"
  When the user scrolls to the "Done" field
  And the user clicks "Cancel"
  Then the Contact page should appear "eyes"


@14.10
Scenario: 14.10 Edit Contact without last name
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "clarkson" "noeyes"
  When the user changes the "Last Name *" from "Clarkson" to ""
	And the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Last Name can't be empty" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "clarkson" "noeyes"


@14.11
Scenario: 14.11 Edit Contact without company
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "clarkson" "noeyes"
  When the user changes the "Company *" from "Top Gear" to ""
  When the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Company can't be empty" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "clarkson" "noeyes"


@14.12
Scenario: 14.12 Edit Contact without email
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Email *" from "hammond@cars.com" to ""
	And the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Email can't be empty" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "Hammond" "noeyes"


@14.13
Scenario: 14.13 Edit Contact with invalid email
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page  
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Hammond"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Hammond"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Hammond" "noeyes"
  When the user changes the "Email *" from "hammond@cars.com" to "?????????!!!"
	And the user scrolls to the "Done" field
	And the user clicks "Done"
	Then the user will receive a "Invalid email address" message
  When the user clicks "OK"
  Then the Edit Contact page should open for "Hammond" "noeyes"


@14.14
Scenario: 14.14 Edit contact and press done
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  And the user changes the "Company *" from "Top Gear" to "Amazon.com"
  When the user scrolls to the "Done" field
	And the user clicks "Done"
	And the user clicks the Home button
	And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the "Company" field should contain "Amazon.com"
	

@14.15
Scenario: 14.15 Edit contact and press cancel
 	Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Company *" from "Amazon.com" to "Netflix"
  And the user scrolls to the "Done" field
	And the user clicks "Cancel"
	And the user clicks the Home button
	And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the "Company" field should contain "Amazon.com"


@wip @14.16
Scenario: 14.16 Edit contact field with more than 200 characters
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "First Name *" from "Jeremy" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZzzzzzzzzzzzzzz"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "First Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Last Name *" from "Clarkson" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Last Name has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Email *" from "jeremy@vroom.com" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Email has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Office Address" from "12345 Main St." to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Office Address has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "City" from "AnyCity" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "City has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user changes the "Zip Code" from "123 456" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Zip Code has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Website" field
  And the user changes the "State" from "Any State" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "State has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Website" field
  And the user changes the "Country" from "United Kingdom" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Country has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Website" field
  And the user changes the "Title" from "Presenter" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user scrolls to the "Done" field
  And the user clicks "Done"
  Then the user will receive a "Title has exceeded 200 maximum character limit." message
  When the user clicks "OK"
  And the user clicks the Home button
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user changes the "Website" from "www.website.com" to "AAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBAAAAABBBBBZ"
  And the user clicks "Done"
  Then the user will receive a "Website has exceeded 200 maximum character limit." message


# Can't tell if Topic/FUA is selected or not.
@wip @14.17
Scenario: 14.17 Edit Topics page clear button
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  And the "Android, Ruby" topics should be "selected" 
  When the user clicks "Clear"
  Then the "Android, Ruby" topics should be "deselected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Edit" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Done" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  And the "Android, Ruby" topics should be "deselected" 


# Can't tell if Topic/FUA is selected or not.
@wip @14.18
Scenario: 14.18 Edit Topics
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  And the "Android, Ruby" topics should be "deselected"
  When the user selects the "Android, Ruby" topics
  Then the "Android, Ruby" topics should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  And the "Android, Ruby" topics should be "selected" 


# Can't tell if Topic/FUA is selected or not.
@wip @14.19
Scenario: 14.19 Edit Follow-up Actions clear button
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Mars, Pluto" follow-up actions should be "selected"
  When the user clicks "Clear"
  Then the "Mars, Pluto" follow-up actions should be "deselected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks "Edit"
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Mars, Pluto" follow-up actions should be "deselected"


# Can't tell if Topic/FUA is selected or not.
@wip @14.20
Scenario: 14.20 Edit Follow-up Actions
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes" 
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Mars, Pluto" follow-up actions should be "deselected"
  When the user selects the "Mars, Pluto" follow-up actions
  Then the "Mars, Pluto" follow-up actions should be "selected"
  When the user clicks "Next"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  And the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Follow-Up Actions"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  And the "Mars, Pluto" follow-up actions should be "selected"


@14.21
Scenario: 14.21 Add Top Gear contacts
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 2" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user deletes existing contacts
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  #When the user allows the app to use location data
  When the user adds the "Top Gear" contacts
  And the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  Then the Contacts list should include "Top Gear" contacts "eyes"


# Currently doesn't work because there is not a way to disable autocorrect and keyboard location mapping is broken.
@wip @14.22
Scenario: 14.22 Edit Note/Free Text
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user successfully logs in as "QA"
  Then the user is on the Select an Event page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Select an Event page
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  And the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "default free text" to "I edited this free text"
  And the user clicks "Note"
  And changes the text from "Some random comments." to "I edited this note"
  And the user clicks "Done"
  And the user clicks the Home button
  Then the Scan & Check-In page should open "noeyes"
  When the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  And the user clicks the first contact
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Done" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "Edit Topics" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the text should match "I edited this free text"
  When the user clicks "Note"
  Then the text should match "I edited this note"


# Currently doesn't work because there is not a way to disable autocorrect and keyboard location mapping is broken.
@wip @14.23
Scenario: 14.23 Edit existing Note/Free Text
  Given the user is on the Login screen of the Card Scanner app "eyes"
  And the user successfully logs in as "QA"
  Then the user is logged into the app and the tutorial is displayed "noeyes"
  When the user clears the tutorial overlay
  Then the user is on the landing page "eyes"
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "eyes"
  When the user selects the "All" tab on the Contacts page "eyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  Then the contact with "Last Name" "Clarkson" should be returned by the search
  When the user clicks the first contact
  Then the Contact page should appear "eyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Comments" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "City" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "eyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "eyes"
  When the user clicks "Free Text"
  And changes the text from "I edited this free text" to "And on that bombshell"
  And the user clicks "Note"
  And changes the text from "I edited this note" to "Some say..."
  And the user clicks "Done"
  And the user clicks the Home button
  Then the user is on the landing page "noeyes"
  When the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user goes to the Contacts page
  Then the user should be on the Contacts page "noeyes"
  When the user selects the "All" tab on the Contacts page "noeyes"
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  Then the contact with "Last Name" "Clarkson" should be returned by the search
  When the user clicks the first contact
  Then the Contact page should appear "noeyes"
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Comments" field
  And the user clicks the Edit button
  Then the Edit Contact page should open for "Clarkson" "noeyes"
  When the user scrolls to the "City" field
  And the user clicks "Edit Topics"
  Then the user should be on the Topics page "noeyes"
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page "noeyes"
  When the user clicks "Free Text"
  Then the text should match "And on that bombshell"
  When the user clicks "Note"
  Then the text should match "Some say..."


# Currently, clearing a note with backspace doesn't work so this will fail.
@wip @14.24
Scenario: 14.24 Clear existing note
  Given the user is on the Login screen of the Card Scanner app
  And the user successfully logs in as "QA"
  Then the Scan a Card page should open
  When the user allows the app to use location data
  And the user chooses event "Mobile Automation Event 1" on the Scan a Card page
  And the user navigates to Contacts
  And the user selects the "All" tab
  And the user selects All Events
  And the user searches for the contact with "Last Name" "Clarkson"
  Then the contact with "Last Name" "Clarkson" should be returned by the search
  When the user clicks the first contact
  Then the Contact page should appear
  And the contact information for the contact should match "Clarkson"
  When the user scrolls to the "Comments" field
  And the user clicks the Edit button
  Then the user should be on the Edit Contact page for "Clarkson"
  And the user scrolls to the "City" field
  And the "Comments" field should contain "Some say..."
  When the user clicks "Edit Topics"
  Then the user should be on the Topics page
  When the user clicks "Next"
  Then the user should be on the Edit Follow-Up Actions page
  When the user clicks "Note"
  Then the text matches "Some say..."
  When the user clears the note "Some say..."
  And the user clicks "Done"
  Then the "Comments" field should be empty
