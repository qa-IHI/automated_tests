# encoding: UTF-8

@attendee_alerts @qa_ready @20
Feature: Attendee alerts and notifications

@20.1
Scenario: Verify that the user can subscribe to an attendee

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 1
	Then the attendee options appear
	When the user taps on the "disabled" alert icon
	Then the alert icon is orange for row 1

@20.2
Scenario: Verify that the user can unsubscribe to an attendee

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 1
	Then the attendee options appear
	When the user taps on the "enabled" alert icon
	And the user taps on "Yes" on the pop-up window
	Then the alert icon is grey for row 1

@20.3
Scenario: Verify that the user can subscribe to an attendee on attendee action screen

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 2
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	And the user taps on "Set Attendee Alert" button
	Then the user should see the button is "orange"

@20.4
Scenario: Verify that the user can unsubscribe to an attendee on attendee action screen

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 2
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	And the user taps on "Disable Attendee Alert" button
	Then the user should see the button is "grey"

@20.5
Scenario: Verify that the user can't subscribe to a checked-in attendee

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
   	When the user does a one touch checkin
	Then the user should see that the checkin icon is green.
	When the user swipes to the left on row 1
	Then the attendee options appear
	When the user taps on the "disabled" alert icon
	Then the alert warning popup is displayed

#Currently notifications are not shown on iOS Simulators
@wip @20.6
Scenario: Verify that the user receives notifications for a subscribed attendee

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 3
	Then the attendee options appear
	When the user taps on the "disabled" alert icon
	Then the alert icon is orange for row 3
	When the user swipes to the right and checks in "Alexis Young"
	Then the user receives a notification where "Alexis Young" is checked in

#Currently notifications are not shown on iOS Simulators
@wip @20.7
Scenario: Verify that the user doesn't receive notifications for an unsubscribed attendee

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user does a one touch checkin
	Then the user doesn't receive the checkin notification on the device

#Currently notifications are not shown on iOS Simulators
@wip @20.8:
Scenario: Verify that notifications trigger from attendee action screen

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 2
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	And the user taps on "Set Attendee Alert" button
	Then the user should see the button is "orange"
	And the user taps on "Check-In" button
	Then the user receives a notification where "Alexander Lester" is checked in

#Currently notifications are not shown on iOS Simulators
@wip @20.9
Scenario: Verify that notifications don't trigger from action screen checkin

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "noeyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Attendee Checkin list
   	Then the user should be on the Attendee Checkin list
	When the user swipes to the left on row 1
	Then the attendee options appear
	When the user taps on the magnifying glass icon
	And the user taps on "Check-In" button
	Then the user doesn't receive the checkin notification on the device
