# encoding: UTF-8

@cs_feature_adding_contacts @qa_ready @7 @coretests
Feature: Adding Contacts

Background:
  Given I am logged out on the app
  

@7.1
Scenario: 7.1 Home button from Add Contact page
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user clicks the Home button
   Then the Scan & Check-In page should open "noeyes"


@7.2
Scenario: 7.2 Back button from Add Contact
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user clicks the Back button
   Then the Scan & Check-In page should open "noeyes"


@7.3
Scenario: 7.3 User tries to add contact with no last name
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the user will receive a "Last Name can't be empty" message
  

@7.4
Scenario: 7.4 User tries to add contact with no company
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the user will receive a "Company can't be empty" message


@7.5
Scenario: 7.5 User tries to add contact with no email
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the user will receive a "Email can't be empty" message


@7.6
Scenario: 7.6 User tries to add contact with incorrectly formatted email
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters an incorrectly formatted email address
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the user will receive a "Invalid email address" message


@7.7
Scenario: 7.7 User has not entered last name and clicks Edit Topics
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Topics"
   Then the user will receive a "Last Name can't be empty" message


@7.8
Scenario: 7.8 User has not entered company and clicks Edit Topics
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Done" field
   And the user clicks "Edit Topics"
   Then the user will receive a "Company can't be empty" message


@7.9
Scenario: 7.9 User has not entered email and clicks Edit Topics
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Topics"
   Then the user will receive a "Email can't be empty" message


@7.10
Scenario: 7.10 User enters incorrectly formatted email and clicks Edit Topics
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters an incorrectly formatted email address
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Topics"
   Then the user will receive a "Invalid email address" message


@7.11
Scenario: 7.11 User has not entered last name and clicks Edit Follow-Up Actions
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Follow-Up Actions"
   Then the user will receive a "Last Name can't be empty" message


@7.12
Scenario: 7.12 User has not entered company and clicks Edit Follow-Up Actions
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user scrolls to the "Done" field
   And the user clicks "Edit Follow-Up Actions"
   Then the user will receive a "Company can't be empty" message


@7.13
Scenario: 7.13 User has not entered email and clicks Edit Follow-Up Actions
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Follow-Up Actions"
   Then the user will receive a "Email can't be empty" message


@7.14
Scenario: 7.14 User enters incorrectly formatted email and clicks Edit Follow-Up Actions
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters an incorrectly formatted email address
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Follow-Up Actions"
   Then the user will receive a "Invalid email address" message


@7.15
Scenario: 7.15 User has entered all required fields and clicks Edit Topics
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Topics"
   Then the user should be on the Topics page "eyes"


@7.16
Scenario: 7.16 User has entered all required fields and clicks Edit Follow-Up Actions
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Edit Follow-Up Actions"
   Then the user should be on the Edit Follow-Up Actions page "eyes"


@7.17
Scenario: 7.17 User has entered all required fields and clicks Done
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Done"
   Then the Scan & Check-In page should open "noeyes"


@7.18
Scenario: 7.18 User has entered all required fields and clicks Cancel
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user enters the first name
   And the user enters the last name
   And the user enters the email
   And the user enters the company name
   And the user scrolls to the "Done" field
   And the user clicks "Cancel"
   Then the Scan & Check-In page should open "noeyes"


@7.19
Scenario: 7.19 User correctly enters all required fields for new contacts and clicks Done
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user navigates to Contacts
   And the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   And the user deletes existing contacts
   And the user clicks the Home button
   Then the Scan & Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user manually enters new contact ")(*&^%$$##@!><?:{"
   And the user clicks "Done"
   #And the user clicks the Home button
   Then the Scan & Check-In page should open "noeyes"
   When the user goes to the Contacts page
   Then the user should be on the Contacts page "eyes"
   When the user selects the "All" tab on the Contacts page "eyes"
   And the user selects All Events
   Then the new contact ")(*&^%$$##@!><?:{" should be in the Contacts list "eyes"
   And the user navigates to the Select an Event page
   And the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user manually enters new contact "¿¡ÀÈÌÒÙÇÃÑÕ"
   And the user adds a Topic and Follow-up Action for "¿¡ÀÈÌÒÙÇÃÑÕ"
   And the user navigates to Contacts
   Then the new contact "¿¡ÀÈÌÒÙÇÃÑÕ" should be in the Contacts list "eyes"
   When the user navigates to the Select an Event page
   And the user chooses event "Mobile Automation Event 2" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user manually enters new contact "Ronaldo"
   And the user adds a Topic and Follow-up Action for "Ronaldo"
   And the user navigates to Contacts
   Then the new contact "Ronaldo" should be in the Contacts list "eyes"


@7.20
Scenario: 7.20 Manually added new contact should be at the top and have a "New" badge until clicked
   Given the user is on the Login screen of the Card Scanner app "noeyes"
   When the user successfully logs in as "QA"
   Then the user is on the Select an Event page "noeyes"
   When the user chooses event "Mobile Automation Event 1" on the Select an Event page
   Then the Scan & Check-In page should open "noeyes"
   #When the user allows the app to use location data
   When the user clicks the '+' button
   Then the user should be on the Add Contact page "eyes"
   When the user manually enters new contact "Messi"
   And the user adds a Topic and Follow-up Action for "Messi"
   And the user navigates to Contacts
   And the user selects the "All" tab on the Contacts page "eyes"
   #And the user selects All Events
   Then the new contact "Messi" should be at the top of the Contact list "eyes"
   When the user clicks the contact "Messi"
   Then the Contact page should appear "eyes"
   And the contact information for the contact should match "Messi"
   When the user clicks the Back button
   Then the new contact "Messi" should be in the Contact list in alphabetic order "eyes"

