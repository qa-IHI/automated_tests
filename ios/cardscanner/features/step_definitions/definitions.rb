Given(/^I am logged out on the app$/) do
  login_button = @driver.find_element(:accessibility_id, "Log In").displayed?

  if !login_button
    logout
  end
end

# Check if we are on the Login screen
# Check that the elements on the Login screen are visible
Given(/^the user is on the Login screen of the Card Scanner app "(.*?)"$/) do |visual_validate|
  #sleep 10
  accept_notifications
  all_elements_visible?(["logoLogin.png", "Forgot Password?", "Keep me logged in", "Log In"])

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Initial Login Screen')
  end
end

When(/^the user accepts notifications$/) do
  accept_notifications
end


# Log into the app with a valid username/password
When(/^the user enters valid email and password$/) do
  enter_valid_email($test["qa_username"])
  enter_valid_password($test["qa_password"])
  click_keyboard_done
end

Then(/^the user is on the Select an Event page "(.*?)"$/) do |visual_validate|
  sleep 15
  select_event_page_check

  if visual_validate == "eyes"
    sleep 3
    eyes_verify_screen(@app_name, 'Select an Event Screen')
  end
end


# After logging in, dismiss the tutorial overlay
Then(/^the user is logged into the app and clears the tutorial overlay$/) do
  clear_tutorial_overlay
end

# Once we have logged in, we should not see the "Keep Me Logged in" checkbox
# We should see the other elements on the landing screen
Then(/^the user is on the landing page "(.*?)"$/) do |visual_validate|
  scan_card_page_check

  if visual_validate == "eyes"
    sleep 3
    eyes_verify_screen(@app_name, 'Landing Screen')
  end
end

# Check that we are on the Password Reset screen
Then(/^the Password Reset window should appear "(.*?)"$/) do |visual_validate|
  password_reset_page_check
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Password Reset Screen')
  end
end


Then(/^the Password Reset window with keyboard should appear "(.*?)"$/) do |visual_validate|
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Password Reset Screen with keyboard')
  end
end


# Make sure Keep Me Logged in is enabled by default
When(/^Keep Me Logged in is enabled by default$/) do
  keep_me_logged_in_checked?
end

# Background the Card Scanner app for a time
When(/^the user backgrounds the Card Scanner app and returns$/) do
  @driver.background_app(DEFAULT_BACKGROUND_TIME)
end


When(/^the user kills and relaunches the app$/) do
  @driver.close_app
  @driver.launch_app
end

# Uncheck the Keep Me Logged in checkbox
When(/^the user unchecks Keep Me Logged in$/) do
  keep_me_logged_in(false)
end


# Check the Keep Me Logged in checkbox
When(/^the user checks Keep Me Logged in$/) do
  keep_me_logged_in(true)
end

# Enter nothing for the login email address.
When(/^the user doesn't enter an email address$/) do
  enter_empty_email
end

# Find the error message
Then(/^the user will receive an* "([^"]*)" message$/) do |message|
  wait_for_error(message)
end

# Click a button
When(/^the user clicks "([^"]*)"$/) do |name|
  click_button(name)
end

# Enter an invalid email address format
When(/^the user enters an invalid email$/) do
  enter_invalid_email
end

# Enter a valid email address.
When(/^the user enters a valid email$/) do
  enter_valid_email($test["qa_username"])
end

# Don't enter a password
When(/^does not enter a password$/) do
  enter_empty_password
  #click_keyboard_done
end

When(/^the user enters an invalid email and invalid password$/) do
  enter_invalid_email
  enter_invalid_password
end

When(/^the user enters an invalid email format$/) do
  enter_invalid_email_format
end

When(/^the user enters an invalid password$/) do
  enter_invalid_password
  #click_keyboard_done
end

When(/^enters a valid email account for reset$/) do
  enter_valid_reset_email($test["qa_reset_username"])
end

# Given(/^the user successfully logs in$/) do
#   enter_valid_email($test["qa_username"])
#   enter_valid_password($test["qa_password"])
#   click_button("Log In")
#   clear_tutorial_overlay
#   scan_card_page_check
# end

Given(/^the user successfully logs in as "(.*?)"$/) do |user|
  case user
  when "Uzair"
    username = $test["uzair_username"]
    password = $test["uzair_password"]
  when "QA"
    username = $test["qa_username"]
    password = $test["qa_password"]
  when "AutoQA"
    username = $test["auto_username"]
    password = $test["auto_password"]
  end

  enter_valid_email(username)
  enter_valid_password(password)
  #click_keyboard_done
  click_button("Log In")
  #clear_tutorial_overlay
  #scan_card_page_check
end

When(/^the user chooses event "(.*?)" on the Select an Event page$/) do |event|
  choose_event(event)
end

When(/^the user navigates to other pages from Scan & Check-In$/) do
  navigate_to_contacts
  navigate_to_contactus
  navigate_to_about
  #navigate_to_reportaproblem
end

When(/^navigates back to the Scan & Check-In page$/) do
  navigate_to_scan_and_checkin
  eyes_verify_screen(@app_name, 'Scan & Check-In Screen')
end


When(/^the user navigates to the Select an Event page$/) do
  navigate_to_select_event
  eyes_verify_screen(@app_name, 'Select an Event Screen')
end


Then(/^the chosen Event should be "(.*?)"$/) do |event|
  @driver.wait { @driver.find_element(:accessibility_id, event).displayed? }
end

When(/^the user clicks the tutorial question mark on the Scan & Check-In page$/) do
  click_button("hintBlackCircle")
end

Then(/^the tutorial overlay should be opened "(.*?)"$/) do |visual_validate|
  find_tutorial_overlay

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Tutorial Overlay Screen')
  end
end

Then(/^clicking the tutorial overlay should close it$/) do
  clear_tutorial_overlay
end

When(/^the user goes to the Scan & Check-In page$/) do
  navigate_to_scan_and_checkin
  eyes_verify_screen(@app_name, 'Scan & Check-In Screen')
end

When(/^the user goes to the Contacts page$/) do
  navigate_to_contacts
end

When(/^the user goes to the Attendee Alerts page$/) do
  navigate_to_attendee_alerts
end

Then(/^the user should be on the Attendee Alerts page "(.*?)"$/) do |visual_validate|
  attendee_alerts_page_check
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Attendee Alerts Screen')
  end
end

When(/^the user clicks the Menu Icon$/) do
  click_menu_button
end

Then(/^the app menu should open and "([^"]*)" should be highlighted "(.*?)"$/) do |option, visual_validate|
  verify_app_menu_highlight(option)

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "App Menu with #{option} Highlighted Screen")
  end
end

When(/^the user clicks the '\+' button$/) do
  click_button("btn orange addContact")
end

Then(/^the Add Contact page should open "(.*?)"$/) do |visual_validate|
  add_contact_page_check
  
  #@eyes.force_fullpage_screenshot = true
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Manual Add Contact Screen')
  end
end

When(/^the user goes to the Contact Us page "(.*?)"$/) do |visual_validate|
  navigate_to_contactus
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Contact Us Screen')
  end
end

When(/^the user clicks the Home button$/) do
  click_home_button
end

Then(/^the Scan & Check-In page should open "([^"]*)"$/) do |visual_validate|
  sleep 5
  scan_and_checkin_page_check

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Scan & Check-In Screen')
  end
end

When(/^the user clicks the Email button$/) do
  click_button("email btn")
end

When(/^the user clicks the X button$/) do
  click_button("ci btnCloseX")
end

Then(/^the default email client should open with recipient address set to info@at\-event\.com$/) do
  sleep 10
  @driver.wait { @driver.find_element(:id, "New Message").displayed? }
  @driver.wait { @driver.find_element(:id, "info@at-event.com").displayed? }
end

Then(/^the default map client should open with directions from the current location to the atEvent office$/) do
  #@driver.wait { @driver.find_element(:id, "Allow \"Maps\" to Access Your Location While You Use the App?").displayed? }
  @driver.wait { @driver.find_element(:id, "Allow").click }
end

Then(/^the default map client should open with a map of the atEvent office$/) do
  pending
end

When(/^the user goes to the About page$/) do
  navigate_to_about
  eyes_verify_screen(@app_name, 'About Screen')
end

Then(/^the correct app version is displayed "(.*?)"$/) do |visual_validate|
  verify_app_version
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'About Screen')
  end
end

When(/^the user clicks the Terms and Conditions link$/) do
  click_terms_link
end

Then(/^default web browser should open and display the Terms and Conditions page$/) do
  sleep 5
  eyes_verify_screen(@app_name, 'Terms and Conditions Screen')
end

When(/^the user goes to Report a Problem$/) do
  navigate_to_reportaproblem
  eyes_verify_screen(@app_name, 'Report a Problem Screen')
end

Then(/^default email client should open$/) do
  pending # express the regexp above with the code you wish you had
end

Then(/^the To field should be "(.*?)"$/) do |address|
  @driver.wait { @driver.find_element(:id, address).displayed? }
end

Then(/^the default text should be "(.*?)" "(.*?)"$/) do |text, visual_validate|
  @driver.wait { @driver.find_element(:id, text).displayed? }
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Report a Problem Screen')
  end
end

Then(/^the email contains an attached report$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^the user changes the To field to personal email "(.*?)"$/) do |email|
  email_element = @driver.wait { @driver.find_element(:id, "support@at-event.com") }
  #email_element.type email
  email_element.send_keys email
  click_keyboard_done
end

When(/^clicks "(.*?)"$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

Then(/^the email should be sent$/) do
  @driver.wait { @driver.find_element(:id, "Report successfully sent.").displayed? }
end

Then(/^the user should be on the Add Contact page "(.*?)"$/) do |visual_validate|
  add_contact_page_check
  #@eyes.force_fullpage_screenshot = true

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Manual Add Contact Screen')
  end
end

Then(/^the user should be on the scrolled Add Contact page$/) do
  eyes_verify_screen(@app_name, 'Manual Add Contact Scrolled Screen')
end


When(/^the user clicks the Back button$/) do
  click_add_contact_back_button
end

Then(/^the user should be on the Topics page "(.*?)"$/) do |visual_validate|
  edit_topics_page_check

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Edit Topics Screen')
  end
end

Then(/^the user should be on the Edit Follow-Up Actions page "(.*?)"$/) do |visual_validate|
  edit_followup_actions_page_check

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Edit Follow-Up Actions Screen')
  end
end

Then(/^the user should be on the Contacts page "(.*?)"$/) do |visual_validate|
  contacts_page_check

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Contacts Screen')
  end
end

When(/^the user enters an incorrectly formatted email address$/) do
  enter_new_contact_invalid_email
end

Then(/^the new contact "(.*?)" should be in the Contacts list "(.*?)"$/) do |name, visual_validate|
  check_new_contact_added(name)
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Contacts Screen with #{name}")
  end
end

When(/^the user deletes existing contacts$/) do
  delete_all_contacts
end
   
Then(/^the Contacts list should be empty "(.*?)"$/) do |visual_validate|
  #verify_all_contact_deletion
  verify_contacts_deleted($test["contact_list_size"])

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Empty Contacts Screen')
  end
end

When(/^the user navigates to Contacts$/) do
  sleep 5
  navigate_to_contacts
  eyes_verify_screen(@app_name, 'Contacts Screen')
end

When(/^the user waits "(.*?)" seconds$/) do |secs|
  sleep secs.to_i
end

Then(/^the new contact "(.*?)" should be at the top of the Contact list "(.*?)"$/) do |lname, visual_validate|
  # if lname == $test["contact0_lname"]
  #   top_of_contact_list?($test["contact0_fname"] + " " + $test["contact0_lname"])
  # elsif lname == $test["contact2_lname"]
  #   top_of_contact_list?($test["contact2_fname"] + " " + $test["contact2_lname"])
  # elsif lname == $test["contact1_lname"]
  #   top_of_contact_list?($test["contact1_fname"] + " " + $test["contact1_lname"])
  # elsif lname == $test["contact3_lname"]
  #   top_of_contact_list?($test["contact3_fname"] + " " + $test["contact3_lname"])
  # end

  click_contacts_tab("All")
  select_all_events_on_contacts_page
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "#{lname} Top of Contacts List Screen")
  end
end

Then(/^the new contact should have a "(.*?)" badge$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

When(/^the user manually enters new contact "(.*?)"$/) do |name|
  add_new_contact(name)
end

When(/^the user clicks the contact "(.*?)"$/) do |lname|
  click_contact(lname)
end

Then(/^the contact information for the contact should match "(.*?)"$/) do |lname|
  verify_contact_info(lname)
end

Then(/^the new contact "(.*?)" should be in the Contact list in alphabetic order "(.*?)"$/) do |lname, visual_validate|
  # name1 = $test["contact0_fname"] + " " + $test["contact0_lname"]
  # name2 = $test["contact2_fname"] + " " + $test["contact2_lname"]
  # name3 = $test["contact1_fname"] + " " + $test["contact1_lname"]
  # alphabetic_contacts?(name1, name2, name3)

  click_contacts_tab("All")
  select_all_events_on_contacts_page
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "#{lname} in Alphabetical Order Contacts Screen")
  end
end

Then(/^the Contact page should appear "(.*?)"$/) do |visual_validate|
  if visual_validate == "eyes"
    contact_page_check
  end
end

When(/^the user enters the first name$/) do
  enter_contact_field("First Name *", "Phil")
end

When(/^the user enters a first name$/) do
  enter_requesttrial_field("First Name *", "QA")
end

When(/^the user enters the last name$/) do
    enter_contact_field("Last Name *", "Collins")
end

When(/^the user enters a last name$/) do
    enter_requesttrial_field("Last Name *", "Tester")
end

When(/^the user enters the company name$/) do
  enter_contact_field("Company *", "Spotify")
end

When(/^the user enters a company name$/) do
  enter_requesttrial_field("Company *", "Mobile Automation")
end

When(/^the user enters the email$/) do
  enter_contact_field("Email *", "phil@spotify.com")
end

When(/^the user enters an email$/) do
  enter_requesttrial_field("Email *", "qa@tester.com")
end

When(/^the user enters a phone number$/) do
  enter_requesttrial_field("Phone", "12345678900")
end

When(/^the user scrolls to the "(.*?)" field$/) do |field|
  scroll_to(field)
  #@driver.scroll_to(field)
end

When(/^the user scrolls down on Add Contact$/) do
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 188, :y => 533).move_to(:x => 0, :y => -362).release.perform
  sleep 5
end

When(/^the user scrolls to the bottom of the Contacts page$/) do
  scroll_to_contacts_bottom
end

When(/^the user scrolls to the bottom of the FUA list$/) do
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 182, :y => 464).move_to(:x => 0, :y => -360).release.perform
end

When(/^the user scrolls to the bottom of the Topics list$/) do
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 182, :y => 464).move_to(:x => 0, :y => -360).release.perform
end

When(/^the user scrolls to the bottom of the Sub\-Topics list$/) do
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 200, :y => 426).move_to(:x => 0, :y => -392).release.perform
end

When(/^the user scrolls to the bottom of the sub\-FUA list$/) do
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 178, :y => 425).move_to(:x => 0, :y => -318).release.perform
end

Then(/^the user is on the Request a Trial page$/) do
  request_trial_page_check
end

Then(/^the user is back on the Request a Trial page$/) do
  error_request_trial_page_check
end

Then(/^the "(.*?)" checkbox is unchecked$/) do |boxname|
  agree_terms_of_service_unchecked?
end

When(/^the user enters an invalid phone number$/) do
  enter_requesttrial_field("Phone", "abc-def-ghij")
end

When(/^the user enters invalid email$/) do
  enter_requesttrial_field("Email", "!!!!!!!$$}}}}}}}")
end

When(/^user accepts the Terms of Service$/) do
  accept_terms_of_service(true)
end

When(/^the user searches for the contact with "(.*?)" "(.*?)"$/) do |key, value|
  search_for_contact(value)
end

Then(/^the contact with "(.*?)" "(.*?)" should be returned by the search$/) do |key, value|
  verify_search_contact_returned(value)
end

When(/^the user clicks the first contact$/) do
  click_first_contact
end

Then(/^the contact with "(.*?)" "(.*?)" should not be returned by the search$/) do |key, value|
  verify_no_contacts
end

Then(/^the user should be on the Edit Contact page for "(.*?)"$/) do |lname|
  edit_contact_page_check(lname)
end

Then(/^the Edit Contact page should open for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
  edit_contact_page_check2

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "#{attendee} Edit Contact Screen")
  end
end

When(/^the user changes the "(.*?)" from "(.*?)" to "(.*?)"$/) do |key, old_value, new_value|
  edit_field(key, old_value, new_value)
end

Then(/^the contact information for "(.*?)" will have "(.*?)" "(.*?)"$/) do |lname, key, value|
  contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(lname)

  @driver.wait { @driver.find_element(:id, fullname).displayed? }
  @driver.wait { @driver.find_element(:id, value).displayed? }
  @driver.wait { @driver.find_element(:id, $test["#{contact}_job_title"]).displayed? }
  @driver.wait { @driver.find_element(:id, $test["contact_phone"]).displayed? }
  @driver.wait { @driver.find_element(:id, $test["contact_mobile"]).displayed? }
  @driver.wait { @driver.find_element(:id, $test["#{contact}_email"]).displayed? }
  @driver.wait { @driver.find_element(:id, $test["contact_web"]).displayed? }
end

Then(/^the Sort Contacts page should appear "(.*?)"$/) do |visual_validate|
  sort_contacts_page_check
  if visual_validate
    eyes_verify_screen(@app_name, 'Sort Contacts Screen')
  end
end

Then(/^the Filter Contacts page should appear "(.*?)"$/) do |visual_validate|
  filter_contacts_page_check
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, 'Filter Contacts Screen')
  end 
end

When(/^the user selects to sort the contacts into "(.*?)" alphabetical order$/) do |order|
  sort_contacts(order)
end

Then(/^the Contacts list should be in "(.*?)" alphabetical order by last name "(.*?)"$/) do |order, visual_validate|
  verify_contacts_sorted(order)
  if visual_validate
    eyes_verify_screen(@app_name, "Sort Contacts #{order} Alphabetical Order Screen")
  end
end

Then(/^only the "(.*?)" option should be selected "(.*?)"$/) do |order, visual_validate|
  ascend_element = @driver.find_element(:accessibility_id, "Alphabetically (Ascending)")
  descend_element = @driver.find_element(:accessibility_id, "Alphabetically (Descending)")

  case order
  when "ascending"
    raise "Error: Only ascending option should be selected." unless ascend_element.value.to_i == 1
  when "descending"
    raise "Error: Only descending option should be selected." unless descend_element.value.to_i == 1
  end

  if visual_validate
    eyes_verify_screen(@app_name, "#{order} Sort Option Screen")
  end
end

When(/^the user clears the Sort option$/) do
  clear_element = @driver.wait { @driver.find_element(:id, "btn filterclear") }
  clear_element.click
end

Then(/^no Sort options should be selected "(.*?)"$/) do |visual_validate|
  ascend_element = @driver.wait { @driver.find_element(:id, "Alphabetically (Ascending)") }
  descend_element = @driver.wait { @driver.find_element(:id, "Alphabetically (Descending)") }

  if ascend_element.value == 1 || descend_element.value == 1
    raise "Error: No sorting option should be selected."
  end

  if visual_validate
    eyes_verify_screen(@app_name, 'No Sort Options Screen')
  end
end

When(/^the user filters by the "(.*?)" named "(.*?)"$/) do |filter, value|
  filter_contacts(filter, value)
end

When(/^the user applies the filter$/) do
  apply_filter
end

Then(/^the Filter button will show "(.*?)" filter applied$/) do |number|
  verify_filtered_number(number)
end

Then(/^the Contacts list should contain contact\(s\) "(.*?)" based on "(.*?)"$/) do |contact_list, filter|
  index = 3
  contact_list.split(", ").each do |person|
    contact, fullname, company, topic1, topic2, fua1, fua2 = determine_contact(person)
    @driver.wait { @driver.find_element(:id, fullname).displayed? }
    index = index + 1
  end

  begin
    contact_exists = @driver.ele_index(:UIATableCell, index).displayed?
    raise "Error: Filter return too many contacts." if contact_exists
  rescue
    #return
  end
end

When(/^the user adds a Topic and Follow\-up Action for "(.*?)"$/) do |lname|
  add_topic_fua(lname)
end

Then(/^"(.*?)" should be highlighted$/) do |option|
  verify_filter_highlight(option)
end

When(/^the user clears all filters$/) do
  clear_element = @driver.wait { @driver.find_element(:id, "btn filterclear") }
  clear_element.click
end

Then(/^no filters should be highlighted$/) do
  verify_no_filter_highlights(["Company", "Event", "Follow Up Actions", "Job Title", "Topics"])
end

Then(/^the Filter button will show no filters applied$/) do
  @driver.wait { @driver.find_element(:id, "Filter").displayed? }
end

When(/^the user deletes the first "(.*?)" contacts$/) do |number|
  delete_contacts(number)
end

Then(/^the first "(.*?)" contacts will not be on the Contacts List$/) do |number|
  verify_contacts_deleted(number)
end

When(/^the user logs out of the Card Scanner app$/) do
  logout
end

When(/^the user adds the "(.*?)" contacts$/) do |group|
  if group == "Top Gear"
    add_contacts ["Clarkson", "Hammond", "May"]
  elsif group == "F1"
    add_contacts ["Hamilton", "Vettel", "Rosberg"]
  elsif group == "NFL"
    add_contacts ["Brady", "Manning"]
  elsif group == "Other"
    add_contacts ["Brown", "Baggins", "Abrams"]
  elsif group == "Starfleet"
      add_contacts ["Kirk", "McCoy"]
  end

  sleep 5
end

Then(/^the Contacts list should include "(.*?)" contacts "(.*?)"$/) do |group, visual_validate|
  if group == "Top Gear"
    verify_contacts ["Clarkson", "Hammond", "May"]
    if visual_validate == "eyes"
      eyes_verify_screen(@app_name, 'Contacts with Top Gear Screen')
    end
  elsif group == "F1"
    verify_contacts ["Hamilton", "Vettel", "Rosberg"]
  elsif group == "NFL"
    verify_contacts ["Brady", "Manning"]
  elsif group == "Other"
    verify_contacts ["Brown", "Baggins", "Abrams"]
    if visual_validate == "eyes"
      eyes_verify_screen(@app_name, 'Contacts with Others Screen')
    end
  end
end

Then(/^the contact page should contain a business card image for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
  verify_contact_with_card_image

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Contact #{attendee} with Business Card Screen")
  end
end

Then(/^the edit contact page should contain a business card image$/) do
  verify_edit_contact_with_card_image
end

When(/^the user clicks the magnify button$/) do
  click_magnify_button
end

Then(/^the enlarged business card should appear for "(.*?)" "(.*?)"$/) do |attendee, visual_validate|
  verify_magnified_card

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Enlarged #{attendee} Business Card Screen")
  end
end

When(/^the user "(.*?)" the business card$/) do |action|
  #multiaction = Appium::MultiTouch.new
  #action1 = Appium::TouchAction.new
  #action2 = Appium::TouchAction.new
 
  if action == "zooms"
    #action1.press(:x => 193, :y => 259).move_to(:x => 0, :y => -95).release
    #action2.press(:x => 193, :y => 400).move_to(:x => 0, :y => 100).release
    #multiaction.add(action1).add(action2).perform
    @driver.pinch(:scale => 5, :velocity => 1)
  elsif action == "pinches"
    # TBD
  end
end

Then(/^the business card should zoom in$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^the user clicks the X icon$/) do
  #@driver.wait { @driver.find_element(:accessibility_id, "capture close").click }
  #@driver.wait { @driver.find_element(:xpath, "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click }

  x = Appium::TouchAction.new
  x.press(x: 339, y:54).wait(1000).move_to(x: 0, y:0).release.perform
end

Then(/^the "(.*?)" field should contain "(.*?)"$/) do |key, value|
  verify_fieldvalue(key, value)
end

Then(/^the topics for "(.*?)" should be "(.*?)"$/) do |lname, state|
  pending
end


When(/^changes the text from "(.*?)" to "(.*?)"$/) do |old_text, new_text|
  replace_text(old_text, new_text)
end

When(/^changes the text from "(.*?)" to more than 500 characters$/) do |old_text|
  new_text = ""
  base = "abcdefghijk"

  for i in 1..50 do
    new_text = new_text + base
  end

  replace_text(old_text, new_text)
end

Then(/^the text should match "(.*?)"$/) do |text|
  match_text(text)
end

When(/^the user clears the note "(.*?)"$/) do |text|
  clear_note(text)
end

Then(/^the "(.*?)" field should be empty$/) do |key|
  verify_empty_fieldvalue(key)
end

Then(/^the text matches "(.*?)"$/) do |text|
  @driver.wait { @driver.find_element(:id, text).displayed? }
end

Then(/^the user should go back to the Edit Contact page$/) do
  back_to_edit_contact_page_check
end

When(/^the user selects the "(.*?)" topics$/) do |list|
  select_topics(list)
end

When(/^the user selects the "(.*?)" subtopics$/) do |list|
  select_subtopics(list)
end

When(/^the user deselects the "(.*?)" topics$/) do |list|
  deselect_topics(list)
end

When(/^the user deselects the "(.*?)" subtopics$/) do |list|
  deselect_subtopics(list)
end

Then(/^the "(.*?)" topics should be "(.*?)"$/) do |list, state|
  verify_topics_state(list, state)
end

Then(/^the "(.*?)" subtopics should be "(.*?)"$/) do |list, state|
  verify_subtopics_state(list, state)
end

Then(/^the sub\-topics should open with "(.*?)" sub\-topics$/) do |list|
  subtopics_page_check(list)
end

Then(/^the sub follow-up actions should open with "(.*?)"$/) do |list|
  fuas_page_check(list)
end

Then(/^the sub-topics pop up should appear "(.*?)"$/) do |visual_validate|
  @driver.wait { @driver.find_element(:id, "capture close").displayed? }
  @driver.wait { @driver.find_element(:id, "Done").displayed? }

  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Sub-topics Screen")
  end
end

Then(/^the sub follow\-up actions pop up should appear$/) do
  #@driver.wait { @driver.find_element(:id, "capture close").displayed? }
  @driver.wait { @driver.find_element(:accessibility_id, "Done").displayed? }
end

Then(/^the Topics page should display "(.*?)" topics in alpha order$/) do |topics_list|
  verify_topics_list_alpha_order("topics", topics_list)
end

Then(/^the FUAs page should display "(.*?)" FUAs in alpha order$/) do |fua_list|
  #verify_topics_list_alpha_order(fua_list)
  verify_fua_list_alpha_order("fua", fua_list)
end

Then(/^the sub\-topics page should display "(.*?)" sub\-topics in alpha order$/) do |subtopics_list|
  verify_topics_list_alpha_order("subtopics", subtopics_list)
end

Then(/^the sub follow\-up actions page should display "(.*?)" sub follow\-up actions in alpha order$/) do |subfua_list|
  #verify_topics_list_alpha_order(subfua_list)
  verify_fua_list_alpha_order("subfua", subfua_list)
end

When(/^the user allows the app to use location data$/) do
  sleep 4
  allow_location_data
end

When(/^the user accepts the use of location data$/) do
  accept_location_data_dialog
end

When(/^the user selects the "(.*?)" tab on the Contacts page "(.*?)"$/) do |tab_name, visual_validate|
  click_contacts_tab(tab_name)

  if visual_validate == "eyes"
    screen_name = "Contacts Screen - #{tab_name} Tab"
    eyes_verify_screen(@app_name, screen_name)
  end
end

When(/^the user presses the Edit button$/) do
  button = Appium::TouchAction.new
  button.press(x: 185, y:618).wait(1000).move_to(x: 185, y:618).release.perform
end

When(/^the "(.*?)" tab will indicate the correct number of contacts$/) do |tab_name|
  verify_contact_tab_count(tab_name)
end

Then(/^all topics should be de\-selected$/) do
  verify_all_topics_deselected
end

When(/^the user selects the "(.*?)" follow\-up actions$/) do |list|
  select_fuas(list)
end

Then(/^the "(.*?)" follow\-up actions should be "(.*?)"$/) do |list, state|
  verify_fuas_state(list, state)
end

When(/^the user deselects the "(.*?)" follow\-up actions$/) do |list|
  deselect_fuas(list)
end

When(/^the user selects the "(.*?)" sub follow\-up actions$/) do |list|
  select_subfuas(list)
end

Then(/^the "(.*?)" sub follow\-up actions should be "(.*?)"$/) do |list, state|
  verify_subfuas_state(list, state)
end

When(/^the user clears all sub follow\-up actions$/) do
  clear_all_subfuas
end

Then(/^all follow\-up actions should be "(.*?)"$/) do |state|
  verify_all_fuas_state(state)
end

Then(/^all sub follow\-up actions should be "(.*?)"$/) do |state|
  verify_all_subfuas_state(state)
end

Then(/^the Note pop\-up should appear with title "(.*?)" "(.*?)"$/) do |title, visual_validate|
  verify_note_popup(title)
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Note Screen - A test note")
  end
end

Then(/^the Free Text pop-up should appear with title "(.*?)"$/) do |title|
  verify_note_popup(title)
end

Then(/^the user will go back to the Contact info page$/) do
  back_to_contact_page_check
end

When(/^the user scrolls to the bottom of the Edit Contact page$/) do
  scroll_to_edit_contact_bottom
end

When(/^the user scrolls the "(.*?)" field to the top of the window$/) do |field|
  scroll_field_to_top(field)
end

Then(/^the event dropdown should have "(.*?)" selected by default "(.*?)"$/) do |event, visual_validate|
  @driver.wait { @driver.find_element(:accessibility_id, "  #{event}").displayed? }
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Contacts Screen - #{event} Event Selected")
  end
end


Then(/^the user should be on the "(.*?)" tab "(.*?)"$/) do |choice, visual_validate|
  verify_contact_tab_choice(choice)
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Contacts Screen - #{choice} Tab")
  end
end


Then(/^the no contacts image should be displayed on the "(.*?)" tab "(.*?)"$/) do |tabname, visual_validate|
  verify_empty_contacts_tab(tabname)
  if visual_validate == "eyes"
    eyes_verify_screen(@app_name, "Contacts Screen - #{tabname} Tab No Contacts Image")
  end
end

When(/^the user scrolls to the final buttons$/) do
  scroll_to_edit_botton
end


When(/^the user selects the "(.*?)" event on the Contact page$/) do |event|
  select_event_contacts_page(event)
end


Then(/^the "(.*?)" tab should contain "(.*?)" contacts$/) do |tab, count|
  verify_contact_list_count(tab, count)
end


Then(/^the "(.*?)" filter option will be displayed$/) do |choice|
  raise "Error: The Event filter should be displayed." unless @driver.wait { @driver.find_element(:id, choice).displayed? }
end


Then(/^the "(.*?)" filter option will not be displayed$/) do |choice|
  #if @driver.wait { @driver.find_element(:id, choice).displayed? }
    raise "Error: The Event filter option should not be displayed." if element_exists_by_name?(choice)
  #end
end


When(/^the user enters an invalid password reset email$/) do
  enter_invalid_reset_email
end


Then(/^the default Attendee Alert event dropdown should match "(.*?)"$/) do |event|
  verify_attendee_alerts_event(event)
end


Then(/^the user is logged into the app and the tutorial is displayed "(.*?)"$/) do |visual_validate|
  if visual_validate == "eyes"
    sleep 5
    eyes_verify_screen(@app_name, 'Tutorial Overlay Screen')
  end
end

When(/^the user clears the tutorial overlay$/) do
  clear_tutorial_overlay
end

When(/^the user selects All Events$/) do
  select_all_events_on_contacts_page
end

Then(/^the tutorial is displayed "(.*?)"$/) do |visual_validate|
  find_tutorial_overlay
  if visual_validate == "eyes"
    sleep 5
    eyes_verify_screen(@app_name, 'Tutorial Overlay Screen')
  end
end


When(/^the user scrolls to the bottom of the screen$/) do
  #@driver.execute_script("mobile: scroll", {"direction": "down"})

  # press/wait/moveTo/release
  scroll = Appium::TouchAction.new
  scroll.press(:x => 177, :y => 592).wait(50).move_to(:x => 0, :y => -285).release.perform
end


When(/^the user clicks the Edit button$/) do
  #@driver.execute_script('mobile: tap', :x => 118.0, :y => 603.0)
  @driver.find_element(:accessibility_id, "Edit").click
end


When(/^the user chooses contact "(.*?)"$/) do |lname|
  choose_contact(lname)
end

When (/^the user goes to the Attendee Checkin list$/) do
  checkin_tab()
end

Then (/^the user should be on the Attendee Checkin list$/) do
  is_attendee_list_present()
end

When(/^the user does a one touch checkin$/) do
  one_touch_check_in()
end

Then(/^the user should see that the checkin icon is green\.$/) do
  verify_one_touch_check_in()
end

When(/^the user does a one touch uncheckin$/) do
  one_touch_uncheck_in()
end

Then(/^the user should see that the checkin icon is gray\.$/) do
  verify_one_touch_uncheck_in()
end

And(/^the user taps on "Check-In" button$/) do
  attendee_screen_checkin()
end

Then(/^the user should see the attendee is checked\-in\.$/) do 
  verify_attendee_screen_checkin()
end

When(/^the user taps on "Undo Check-In" button$/) do 
  attendee_screen_uncheckin()
end

Then(/^the user should see the attendee is unchecked\-in\.$/) do 
  verify_attendee_screen_uncheckin()
end

Then(/^the attendee options appear$/) do
  verify_attendee_options()
end

When(/^the user taps on the "(.*?)" alert icon$/) do |enabled|
  set_alert(enabled)
end

Transform /^(-?\d+)$/ do |number|
  number.to_i
end

Then(/^the alert icon is orange for row (\d+)$/) do |row|
  verify_alert_is_enabled(row)
end

When(/^the user taps on "(.*?)" on the pop-up window$/) do |options|
  popup_options(options)
end

Then(/^the alert icon is grey for row (\d+)$/) do |row|
  verify_alert_is_disabled(row)
end

Then(/^the user should receive the checkin notification on the device$/) do
  verify_notification()
end

When(/^the user swipes to the left on row (\d+)$/) do |row|
  attendee_options(row)
end

When(/^the user taps on the magnifying glass icon for row (\d+)$/) do |row|
  attendee_info(row)
end

Then(/^the user doesn't receive the checkin notification on the device$/) do
  verify_no_notification()
end

When(/^the user taps on "(.*?)" button$/) do |name|
  attendee_screen_alert(name)
end

Then(/^the alert warning popup is displayed$/) do 
  verify_alert_popup()
end

Then(/^the user should see the button is "(.*?)"$/) do |color|
  verify_alert_action_screen(color)
end

When(/^the user swipes to the right and checks in "(.*?)"$/) do |name|
  one_touch_alert(name)
end

When(/^the user enters the required fields$/) do 
  enter_required_fields("First Name *", "Lucy")
  enter_required_fields("Last Name *", "Nightstar")
  enter_required_fields("Email *", "lucy@themermaid.com")
  scroll_el = Appium::TouchAction.new
  scroll_el.press(:x => 17, :y => 304).move_to(:x => 17, :y => -170).release.perform
  enter_required_fields("Company *", "The Mermaid Inc.")
end

When(/^the user selects the dropdown "(.*?)"$/) do |dropdown|
  select_dropdown(dropdown)
end

When(/^the user selects the dropdown value "(.*?)"$/) do |dropvalue|
  select_dropdown_value(dropvalue)
end

When(/^the user selects the social dropdown "(.*?)"$/) do |sdropdown|
  select_social_dropdown(sdropdown)
end

Then(/^the user modifies the dropdown value to "(.*?)"$/) do |dropvalue|
  edit_dropdown(dropvalue)
end


