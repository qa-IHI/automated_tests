# encoding: UTF-8

@businesscards @qa_ready @21
Feature: Business card scan

@21.1
Scenario: Verify that the user can scan a business card

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the card scan button
	Then the camera functionality opens
	When the user presses the take photo button
	Then the photo is taken
	When the user presses the 'Next' button
	And the user presses the 'Submit' button
	Then the user is taken to the contacts tab
	And the card is on pending status

@21.2
Scenario: Verify that the user can cancel a business card scan

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the card scan button
	Then the camera functionality opens
	When the user presses the cross at the top right corner
	Then the user is taken back to the homepage

@21.3
Scenario: Verify that the user can retake a business card photo

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the card scan button
	Then the camera functionality opens
	When the user presses the take photo button
	Then the photo is taken
	When the user presses the 'Retake' button
	Then the user is taken back to the camera

@21.4
Scenario: Verify that topics and follow ups appear after scanning

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the card scan button
	Then the camera functionality opens
	When the user presses the take photo button
	Then the photo is taken
	When the user presses the 'Next' button
	Then the topics for the event appear
	When the user select the first topic
	And the user presses the 'Next' button
	Then the follow ups for the event appear
	When the user selects the first follow up
	And the user presses the 'Submit' button
	Then the user is taken to the contacts tab
	And the card is on pending status

@21.5 
Scenario: Verify that time stamp is shown on pending cards
	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "AutoQA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "An Event" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user taps on the card scan button
	Then the camera functionality opens
	When the user presses the take photo button
	Then the photo is taken
	When the user presses the 'Next' button
	And the user presses the 'Submit' button
	Then the user is taken to the contacts tab
	And the card is on pending status
	And the cards have the proper timestamp
