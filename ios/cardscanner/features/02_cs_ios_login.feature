# encoding: UTF-8

@cs_feature_login @qa_ready @2
Feature: Login

Background:
  Given I am logged out on the app

@2.1
Scenario: 2.1 User logs into app with valid credentials
  Given the user is on the Login screen of the Card Scanner app "noeyes"
  When the user enters valid email and password
  And the user clicks "Log In"
  Then the user is on the Select an Event page "noeyes"


@2.2
Scenario: 2.2 Forgot password request
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user clicks "Forgot Password?"
  Then the Password Reset window should appear "eyes"
  When the user clicks "Cancel"
  Then the user is on the Login screen of the Card Scanner app "noeyes"
  When the user clicks "Forgot Password?"
  Then the Password Reset window should appear "noeyes"


@2.3
Scenario: 2.3 Keep me logged in and background app
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user checks Keep Me Logged in
  And the user enters valid email and password
  And the user clicks "Log In"
  Then the user is on the Select an Event page "noeyes"
  When the user backgrounds the Card Scanner app and returns
  Then the user is on the Select an Event page "noeyes"


@2.4
Scenario: 2.4 Keep me logged in and kill app
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user checks Keep Me Logged in
  And the user enters valid email and password
  And the user clicks "Log In"
  Then the user is on the Select an Event page "noeyes"
  When the user kills and relaunches the app
  Then the user is on the Select an Event page "noeyes"


@2.5
Scenario: 2.5 Don't keep me logged in and kill app
  Given the user is on the Login screen of the Card Scanner app "eyes"
  When the user unchecks Keep Me Logged in
  And the user enters valid email and password
  And the user clicks "Log In"
  Then the user is on the Select an Event page "noeyes"
  When the user kills and relaunches the app
  Then the user is on the Login screen of the Card Scanner app "noeyes"


@2.6
Scenario: 2.6 No email error
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user doesn't enter an email address
   And the user clicks "Log In"
   Then the user will receive an "Please enter your password" message
   When the user clicks "OK"
   Then the user is on the Login screen of the Card Scanner app "noeyes"

@2.7
Scenario: 2.7 Incorrect email format
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user enters an invalid email format
   And the user clicks "Log In"
   Then the user will receive an "Invalid email address!" message
   When the user clicks "OK"
   Then the user is on the Login screen of the Card Scanner app "noeyes"

@2.8
Scenario: 2.8 No password error
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user enters a valid email
   And does not enter a password
   And the user clicks "Log In"
   Then the user will receive an "Please enter your password" message
   When the user clicks "OK"
   Then the user is on the Login screen of the Card Scanner app "noeyes"

@2.9
Scenario: 2.9 Invalid login credentials
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user enters an invalid email
   And the user enters an invalid password
   And the user clicks "Log In"
   Then the user will receive an "Invalid email address!" message
   When the user clicks "OK"
   Then the user is on the Login screen of the Card Scanner app "noeyes"

@2.10
Scenario: 2.10 Reset password for invalid account
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user clicks "Forgot Password?"
   Then the Password Reset window should appear "eyes"
   When the user enters an invalid password reset email
   And the user clicks "Send"
   Then the user will receive a "No account found associated with this email address." message
   When the user clicks "OK"
   Then the Password Reset window with keyboard should appear "eyes"

@2.11
Scenario: 2.11 Reset password for valid account
   Given the user is on the Login screen of the Card Scanner app "eyes"
   When the user clicks "Forgot Password?"
   And enters a valid email account for reset
   And the user clicks "Send"
   Then the user will receive a "Success" message
   And the user clicks "OK"
   