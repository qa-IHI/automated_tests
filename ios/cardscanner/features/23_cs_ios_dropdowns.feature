# encoding: UTF-8

@businesscards @qa_ready @23
Feature: Dropdown values

	
@23.1
Scenario: Verify that user cannot add a contact without required dropdowns

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "QA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "3.0 Automation" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	And the user clicks the '+' button
	When the user enters the required fields
	When the user scrolls to the final buttons
	And the user clicks "Done"
	Then the user will receive a "Company size can't be empty" message

@23.2
Scenario: Verify that user can select contact and social values in dropdowns

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "QA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "3.0 Automation" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	#When the user allows the app to use location data
	And the user clicks the '+' button
	#Then the user should be on the Add Contact page "noeyes"
	When the user enters the required fields
	And the user selects the dropdown "Company size"
	And the user selects the dropdown value "50-100"
	And the user selects the social dropdown "Has Facebook"
	And the user selects the dropdown value "No"
	And the user clicks "Done"
	#Then the new contact "Lucy" should be in the Contacts list "noeyes"
	Then the Scan & Check-In page should open "noeyes"

@23.3
Scenario: Verify that user can edit contact and social values in dropdowns

	Given the user is on the Login screen of the Card Scanner app "noeyes"
 	And the user successfully logs in as "QA"
 	Then the user is on the Select an Event page "noeyes"
    When the user chooses event "3.0 Automation" on the Select an Event page
    Then the tutorial is displayed "eyes"
    When the user clears the tutorial overlay
    Then the Scan & Check-In page should open "noeyes"
	When the user goes to the Contacts page
	Then the user should be on the Contacts page "noeyes"
	When the user selects the "All" tab on the Contacts page "noeyes"
	And the user selects All Events
	And the user searches for the contact with "Last Name" "Nightstar"
	And the user clicks the first contact
	#Then the Edit Contact page should open for "Nightstar" "noeyes"
	Then the Contact page should appear "noeyes"
	When the user scrolls to the final buttons
	And the user clicks the Edit button	
	And the user selects the dropdown "Company size"
	And the user modifies the dropdown value to "+500"
	And the user selects the social dropdown "Has Facebook"
	And the user modifies the dropdown value to "Yes, as a user"
  	And the user clicks "Done"
  	Then the Contact page should appear "noeyes"
